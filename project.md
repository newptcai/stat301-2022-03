---
title: Final Project Arrangement
subtitle: STATS 301 Statistics
colorlinks: true
urlcolor: Maroon
marings: 1.5in
...

As part of the evaluation process of this course,
you will do a statistics project 
and present your result in a **written** report.


# Data and Statistics

You should choose a real-world data set for this project.
Some potential sources of data include

1. [DASL](https://dasl.datadescription.com/)
2. [OzDASL]()
3. [Our World in Data](https://ourworldindata.org/)
4. [Statista](https://www.statista.com/)
5. [EIA](https://www.eia.gov/)
6. https://data.gov/
7. [Reddit](http://www.reddit.com/r/datasets/)
8. Any scientific paper which comes with access to supporting data.
9. Any other publicly available data.

# Statistics

Once you have decided on your data set,
you should carry out some statistics on the data to answer questions such as --

1. What kind of distribution do the observations have?
2. What are the parameters of these distributions?
3. What are the confidence interval of the parameters?
4. If the observations can be divided into subgroups, how do they compare with each
   other.

The computations and plots must be done with Julia.

# Presentation

For this time, we will **skip** the planned oral presentation of your project.
You **only** need to submit your data set, Julia code, 
and a report (in the form of a set of slides) to GradeScope, by end of Week 7.

\pagebreak{}

# Rubrics

The grades you get for this project will depend on which of the following category
best fit your project.

## Excellent (5pt)

1. The questions answered in the project give some insight into the real world.
2. A clear summary of the data has been given.
3. At least five different types statistics inferences have been applied. 
4. At least one of the five statistic inferences have **not** been covered in the course.
5. The code has no computation mistakes.
6. At least five clear plots have been generated and used in the report.
7. The report is written with care and has no typos.

## Good (4pt)

1. A summary of the data has been given.
2. At least five different types statistics inferences have been applied. 
3. The code has no mistakes.
4. At least five plots have been generated and used in the report.
5. The report is written with care but has a few typos.

## Fine (3pt)

1. A summary of the data has been given.
2. Some statistics inferences have been applied. 
3. The code has a few mistakes.
4. At least five plots have been generated and used in the report.
5. The report has many typos or some other issues.

## Pass (2pt)

1. Some statistics inferences have been applied. 
2. The code has many mistakes.
4. Less than five plots have been generated and used in the report.
3. The report has many typos.

## Complete (1pt)

1. The project has been submitted on time.

## Incomplete (0pt)

1. The project has not been submitted on time.
