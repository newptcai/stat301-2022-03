#!/bin/bash

find \
    slides \
    -type f \
    \( -name "*.tex" ! -name "meta.tex" \) \
    -printf '%p' \
    -execdir \
    latexmk \
        -silent \
        -synctex=1 \
        -interaction=nonstopmode \
        -shell-escape \
        -pdflua \
        {} \;
