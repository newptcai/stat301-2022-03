using Distributions, Random

## How to compute \hat{θ}
hat(data) = 4/(2+sum(data))

## The prior
prior = Gamma(1, 1/2)

## How to get a sample
function sample()
    θ = rand(prior)
    xs = rand(Exponential(1/θ), 3)
    return abs(θ-hat(xs)) < 0.1
end

## Simulation
N = 10^6
prob = sum([sample() for _ in 1:N])/N
println("The probability is approximately $prob")