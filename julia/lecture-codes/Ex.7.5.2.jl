using Roots, ForwardDiff, Plots

## The function we want to maximize
f(θ) = θ^3*exp(-6.6*θ)

## The derivative of f
df = θ -> ForwardDiff.derivative(f, float(θ))

## Draw a plot
plt=plot(f, xlims=(0, 4), label="Likelihood function")

## Find the maximum
roots = find_zeros(df, 0, 2)

## Maximum point
pt = round.((roots[2], f(roots[2])), sigdigits=2)
scatter!(plt, pt, label="Maximum")
txtpt = pt .- (-0.5, 0.0002)
annotate!(plt, [(txtpt[1], txtpt[2], (string(pt), 9))])

## Save the plot for slides
Plots.savefig(plt, "/tmp/5.2.7.png")