using Distributions

function thredsholds(n, μ0, α0) 
    c = quantile(Normal(), 1-α0/2)*sqrt(26)/sqrt(n)
    return (μ0-c, μ0+c)
end

# From the book
thredsholds(30, 140, 0.05)

# In-classe
thredsholds(100, 140, 0.1)