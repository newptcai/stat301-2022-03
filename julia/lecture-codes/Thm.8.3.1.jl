using Random, Distributions, Plots

## Give me a sample
function sample(dist::UnivariateDistribution, n)
    data = rand(dist, n)
    ch1 = sum([x^2 for x in data])
    μ0 = mean(data)
    ch2 = sum([(x-μ0)^2 for x in data])
    return [ch1 ch2]
end

## Sample a lot
function sample(N::Integer, n::Integer)
    ## The distributions
    dist = Normal(0, 1)
    data = vcat([sample(dist, n) for _ in 1:N]...)
    return data
end


## Simulation
N = 10^5
n = 3
distch1 = Chisq(n)
distch2 = Chisq(n-1)
data = sample(N, n)

## Plot
plt = plot(x->pdf(distch1, x), xlims=(0, 8), label="Chisq($n)", title="n = $n")
plot!(x->pdf(distch2, x), xlims=(0, 8), label="Chisq($(n-1))")
histogram!(data[:, 1], normalize=true, opacity=0.7, label="μ=0")
histogram!(data[:, 2], normalize=true, opacity=0.7, label="μ unknown")

## Save plot
savefig(plt, "/tmp/thm.8.3.1.png")

## Simulation
N = 10^5
n = 10
distch1 = Chisq(n)
distch2 = Chisq(n-1)
data = sample(N, n)

## Plot
plt = plot(x->pdf(distch1, x), xlims=(0, 2*n), label="Chisq($n)", title="n = $n")
plot!(x->pdf(distch2, x), label="Chisq($(n-1))")
histogram!(data[:, 1], normalize=true, opacity=0.7, label="μ=0")
histogram!(data[:, 2], normalize=true, opacity=0.7, label="μ unknown")

## Save plot
savefig(plt, "/tmp/thm.8.3.1-1.png")