using Distributions, Random, Plots

## Compute the quantile for the following experiment
γ = 0.9
n = 26
dist = TDist(n-1)
c = quantile(dist, (1+γ)/2)
c1 = round(c/sqrt(n), digits=4)

## What does this tell us?
println("With probability $γ, μ and sample mean is at most $c1 σ' away")

## Give us a sample
function getsample(dist, n, c1)
    data = rand(dist, n)
    x0 = mean(data)
    σ0 = sum((data .- x0).^2)/(n-1) |> sqrt
    h = c1*σ0
    (x0 - h, x0 + h, x0, σ0)
end

## Just one sample
Random.seed!(1000)
μ = 0
σ = 1
dist1 = Normal(μ, σ)
int1 = getsample(dist1, n, c1)

## Simulation
Random.seed!(1000)
N = 100
samples = [getsample(dist1, n, c1) for _ in 1:N]
prob = sum([a < 0 < b for (a, b, c, d) in samples])/N

## Plot
bars = OHLC[(c, b, a, c) for (a, b, c, d) in samples]
bars1 = OHLC[(a < 0 < b) ? (NaN, NaN, NaN, NaN) : (c, b, a, c) for (a, b, c, d) in samples]
default(linewidth = 2)
plt = ohlc(bars, label="Good", title="μ=$μ, σ=$σ, n=$n, N=$N")
ohlc!(bars1, label="Bad", c=:red)
plot!([-0.5,100.5], [0, 0], c=:orange, linewidth=1, label="μ")

## Save the plot
savefig(plt, "/tmp/Ex.8.5.2.png")