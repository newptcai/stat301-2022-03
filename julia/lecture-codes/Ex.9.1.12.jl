using Distributions

## Compute the p-value
# n = 10
# p0 = 0.3
# y = 6
n = 12
p0 = 0.5
y = 7
dist = Binomial(n, p0)
sum([pdf(dist, i) for i in y:n])