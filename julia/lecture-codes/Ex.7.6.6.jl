using Plots, ForwardDiff, Roots, SpecialFunctions; gr()

## Straing point
α0 = 3.679

## We need to find the roots of
f(x) = digamma(x) - 1.220

## Draw a plot
plt = plot(f, xlims=(3.6, 3.9), legend=:topleft, label="f(x)")

## Newton's method
function newton(f, α0)
    ## Take the direvative
    df =  x -> ForwardDiff.derivative(f, float(x))
    α1 = α0 - f(α0)/df(α0)
    return α1
end

## Staring point
αs = [α0] 

## Better and better approximate solution
for i in 1:5
    α = αs[end]
    pt = (α, f(α))
    @show pt
    scatter!(plt, pt, label="α$(i-1)")
    push!(αs, newton(f, α))
end
plt

## Compare the result with Roots
roots = find_zeros(f, α0, 4)
roots[1] ≈ αs[end]

## Save the plot for slides
savefig(plt, "/tmp/e.x.7.6.6.png")