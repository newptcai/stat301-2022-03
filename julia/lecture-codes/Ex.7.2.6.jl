# # Example 7.2.6

# This code is inspired by 
# [How to Use Turing](https://storopoli.io/Bayesian-Julia/pages/4_Turing/)
# The alogrithm used to find the posterior is described
# [here](https://storopoli.io/Bayesian-Julia/pages/5_MCMC/#metropolis_algorithm)

using Turing, Plots, StatsPlots, LaTeXStrings

##
@model function phone_lifetime(data, prior)
    #Our prior     
    θ ~ prior
    #Each lifetime of phones
    data ~ filldist(Exponential(1/θ), length(data))
end;

## 
function posterior(α, β, data)
    prior = Gamma(α, 1/β)
    model = phone_lifetime(data, prior);
    chain = sample(model, NUTS(), 3_000);
    (summaries, quantiles) = describe(chain);
    plt = density(chain.value[:, 1], label="posterior")
    plot!(plt, 
        title=L"\alpha=%$α, \beta=%$β",
        x->pdf(prior, x), 
        xlims=(0,xlims(plt)[2]),
        label="prior")
    return plt
end

## Five observations from the book
data = [2911, 3403, 3237, 3509, 3118];

##
plt1=posterior(4, 20_000, data)
plt2=posterior(1, 1_000, data)
plot(plt1, plt2, layout=(2,1))