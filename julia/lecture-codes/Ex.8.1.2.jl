using Distributions, Random, Plots

## Define some distributions
dist1 = Normal(1, 1)
dist2 = Normal(1, 1/10)

## Plot PDF
plt = plot(x->pdf(dist1, x), xlims=(-2, 4), label="PDF of N(1,1)")
plot!(x->pdf(dist2, x), xlims=(-2, 4), label="PDF of N(1, 1/10)")

## Save the plot
savefig(plt, "/tmp/ex.8.1.2.png")