using Distributions, Plots

## Unknown parameter(s)
μ = 3.8
σ = 1.1
dist = Normal(μ,σ)

## Get some data
N = 10^5
data = rand(dist, N)

## Find MLE
dist_mle = fit_mle(Normal, data)

## Is this what we expected?
isapprox(dist_mle.μ, mean(data), atol=10^-4)
isapprox((dist_mle.σ)^2, var(data), atol=10^-4)

## We use PlotLy
plotly()

# Make a plot
L(μ,σ) = -(1/2)*N*log(2) -(1/2)*N*log(π) -N*log(σ) -sum([(data[i]-μ)^2 for i in 1:N])/(2*σ^2)
μs = 3:0.1:4
σs = 0.5:0.1:1.5
surface(μs, σs, L, xlabel="μ", ylabel="σ", zlabel="L(μ,σ)")
scatter!((dist_mle.μ, dist_mle.σ, L(dist_mle.μ, dist_mle.σ)))