using SpecialFunctions

## Example 7.2.8

## We use Julia as a calculator in this problem

# This is wrong
2.633 * 10^36 * gamma(10)

# because this overflows
10^36

# The correct way
c = 2.633 * BigInt(10)^36 * gamma(10)

# Round it
round(c, sigdigits=4)