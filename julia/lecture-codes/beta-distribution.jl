## Draw a plot of the PDF of beta distribution
using Distributions, Plots
gr()

## Plot a PDF
dist = Beta(30, 30)
plt = plot(x->pdf(dist, x), 
    xlims=(0,1), 
    label="PDF of beta(1,1)",
    size=(800, 600),
    thickness_scaling=2,
    )

## Save the plot
savefig(plt, "/tmp/beta-30-30.pdf")