## Example 7.6.5
using Distributions, Random, Plots, ForwardDiff, Roots

## Unknown parameter(s)
θ = 3
dist = Cauchy(θ, 1)

## Plot the PDF
plt = plot(x->pdf(dist,x),
    xlims=(-1,7),
    label="PDF of Cauchy(3,1)", 
    legend=:bottomleft,
    thickness_scaling=1.5
    )
savefig(plt, "/tmp/cauchy-3-1.png")

## Get some data
Random.seed!(300)
N = 5
data = rand(dist, N)

## The likelihood function
f(θ) = 1/(π^N*prod([1+(xi-θ)^2 for xi in data]))
plt = plot(f, xlims=(-10,10), label="Likelihood function")

## The derivative of f
df = θ -> ForwardDiff.derivative(f, float(θ))

## Find the maximum
roots = find_zeros(df, 0, 5)

## Maximum point
pt = round.((roots[1], f(roots[1])), sigdigits=2)
scatter!(plt, pt, label="Maximum")
txtpt = pt .+ (2.5, 0.0)
annotate!(plt, [(txtpt[1], txtpt[2], (string(pt), 9))])