using Distributions, QuadGK, Plots

## The distribution of $(X_1+X_2+X_3)$
dist(θ) = Gamma(3, 1/θ)

## How to compute expecation
expect(pdf,support) = quadgk((x) -> 3/x*pdf(x),support...)[1]

## Compute the expecations
θs = 1:0.1:5
hatθs = Float64[]
support = (0, 10^4)
for θ in θs
    d = dist(θ)
    f(x) = pdf(d, x)
    hatθ = expect(f, support)
    push!(hatθs, hatθ)
end

## Plot
plt = plot(θs, hatθs, label="Numeric Expecation", linestyle=:dot, legend=:topleft)
plot!(θ->3*θ/2, θs, label="3*θ/2", linestyle=:dash)