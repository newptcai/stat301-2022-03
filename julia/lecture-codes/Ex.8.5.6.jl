using Distributions, Random, Plots

## Compute the quantile for the following experiment
γ = 0.9
n = 26
dist = TDist(n-1)
c = quantile(dist, γ)
c1 = round(c/sqrt(n), digits=4)

## Give us a sample
function getsample(dist, n, c1)
    data = rand(dist, n)
    x0 = mean(data)
    σ0 = sum((data .- x0).^2)/(n-1) |> sqrt
    h = c1*σ0
    (x0 - h, x0 + h, x0, σ0)
end

## Just one sample
Random.seed!(1000)
μ = 0
σ = 1
dist1 = Normal(μ, σ)
int1 = getsample(dist1, n, c1)

## Simulation
Random.seed!(1000)
N = 100
samples = [getsample(dist1, n, c1) for _ in 1:N]
prob1 = sum([a < μ  for (a, b, c, d) in samples])/N
prob2 = sum([μ < b  for (a, b, c, d) in samples])/N

## Plot lower
apt = [a for (a, b, c, d) in samples]
apt1 = [(a < μ) ? NaN : a for (a, b, c, d) in samples]
default(linewidth = 2)
plt = scatter(apt, label="Good A", title="μ=$μ, σ=$σ, n=$n, N=$N", ylims=(-0.75, 0.75))
scatter!(apt1, label="Bad A")
plot!([-0.5,100.5], [0, 0], linewidth=1, label="μ")
savefig(plt, "/tmp/Ex.8.5.6-a.png")
plt

## Plot lower
bpt = [b for (a, b, c, d) in samples]
bpt1 = [(b > μ) ? NaN : b for (a, b, c, d) in samples]
plt = scatter(bpt, label="Good B", title="μ=$μ, σ=$σ, n=$n, N=$N", ylims=(-0.75, 0.75), c=4)
scatter!(bpt1, label="Bad B", c=5)
plot!([-0.5,100.5], [0, 0], linewidth=1, label="μ", c=3)
savefig(plt, "/tmp/Ex.8.5.6-b.png")
plt