using Distributions, Random, Plots, StatsPlots

## Prior
prior = Gamma(1, 1/2)
θ = round(rand(prior), digits=4)

## Function for simulation
function mle(dist, n)
    xs = rand(dist, n)
    return n/sum(xs)
end

## Simulate
n = 10^2
dist = Exponential(1/θ)
N = 10^5
data = [mle(dist, n) for _ in 1:N]

## Approximate Normal
dist1 = Normal(θ, θ/sqrt(n))

## Plot
plt = histogram(data, label="MLE", normalize=:pdf, title="θ=$θ, n=$n")
plot!(x->pdf(dist1, x), xlims=(θ-0.2, θ+0.2), label="PDF of N(θ, θ^2/n)")

## Save plot
savefig(plt, "/tmp/ex.7.6.11.png")

## The Bayes case
n = 10^2
tn = n/get_sample(dist, n)
α = 1
β = 3

## The distributions
dist1 = Gamma(α+n, 1/(β+tn))
dist2 = Normal((α+n)/(β+tn), sqrt(α+n)/(β+tn))

## plot
plt = plot(x->pdf(dist1, x), xlims=(θ-0.2, θ+0.2), label="PDF of Gamma",
    title="θ=$θ, n=$n, α=$α, β=$β, t(n)=$(round(tn))")
plot!(plt, x->pdf(dist2, x), xlims=(θ-0.2, θ+0.2), label="PDF of Normal")

## Save plot
savefig(plt, "/tmp/ex.7.6.11-1.png")