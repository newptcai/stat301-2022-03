using Distributions

## True value of θ
θ = 15.5
dist = Uniform(0, θ)

## Get some data
N = 10^5
data = rand(dist, N)

## Find MLE
dist_mle = fit_mle(Uniform, data)

## Is this what we expected?
dist_mle.b == maximum(data)

## Is this what we expected?
dist_mle.a == minimum(data)

## Consistency
ns = 1:10:N
estimates = [maximum(data[1:i]) for i in ns]
plt = plot(ns, estimates, label="Mean of data", xlabel="n")