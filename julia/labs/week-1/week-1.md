---
title: Week 1 Self-Study Guide
subtitle: STATS 301 Statistics
author: Xing Shi Cai
numbersections: true
colorlinks: true
urlcolor: Maroon
marings: 1.5in
---

In this week, you will learn some basics about the programming language 
[Julia](https://julialang.org/) and try using it on your own computer.

Once you setup your development environment,
read and run *all* the code from Chapter 1 of the textbook,
and finish the assignment on [GradeScope](https://gradescope.com/).

Note that in the first lab, you will be given exercise based on the examples from
chapter 1. So you make sure you understand and can run *all* of them.

The rest of this document details how to set things up.

# The textbook

For the practical part of this course,
we will use the free draft version of [Statistics with Julia](https://statisticswithjulia.org/).
You can find a copy through this [post](https://edstem.org/us/courses/20171/discussion/1059125).

# Install Julia

Follow this [YouTube introductory video](https://youtu.be/oTUmW8dWZws?t=242) until 21:07 
and install the [latest version of Julia](https://julialang.org/downloads/).
Get yourself familiar with the [Julia REPL](https://docs.julialang.org/en/v1/stdlib/REPL/).

## Windows User

If you use Windows, 
note that in [this step of installation](https://julialang.org/downloads/platform/#adding_julia_to_path_on_windows_10),
the path you add should be something like
```
C:\Users\MyUserName\AppData\Local\Programs\Julia-1.7.2\bin
```
Replace `MyUserName` by your Windows User Name and
note that it should be dash in `Julia-1.7.2` instead of `Julia 1.7.2` as stated in the
document.

# Install VS code (Optional)

As mentioned in the video, 
there are many ways to write Julia code.
We recommend using [Visual Studio Code](https://code.visualstudio.com/)
together with its [Julia Extension](https://code.visualstudio.com/docs/languages/julia).
The [video linked above](https://youtu.be/oTUmW8dWZws?t=1267) 
shows a bit of how to use VS code with Julia.

# Install R

The codes from the textbook occasionally calls R code from Julia.
(See, e.g., section 1.6 of the book.)
To make this work, download and install R from its [website](https://cran.r-project.org/mirrors.html) 
\emph{before} the next step.


# Setup an environment for the textbook

The code for the textbook can be downloaded from
[GitHub](https://github.com/h-Klok/StatsWithJuliaBook).
To run these code, we need to install some packages as follows ---

Step 1. Start a terminal (command prompt in Windows).

Step 2. Change directory by 
```bash
cd path-to-where-you-down-load-the-code
```

Step 3. Check what files are in the current directory by `ls`  (`dir` on Windows).
Make sure you see `Project.toml` and `Manifest.toml` in the current directory.

Step 4. Install and update the required packages by 
```
julia init.jl
```
This will take quite some time. Have a tea and relax.

If you do not see any error messages, then everything should be good.

# Run code from the textbook

## REPL

To run the codes from the textbook in an REPL, follow these steps --

Step 1. Start a terminal (command prompt in Windows).

Step 2. Change directory by 
```bash
cd path-to-where-you-down-load-the-code
```

Step 3. Start a Julia REPL from your terminal by 
```
julia --project=.
```

Step 4. Type 

```julia
include("1_chapter/helloWorld.jl")
```
or
```julia
include("1_chapter\\helloWorld.jl")
```
if you use Windows,
where `"1_chapter/hellowWorld.jl"` is the path to the file which you want to run.

*Tip:* Hit `<tab>` while typing a file path and the REPL will try some complete the
path for you.

Hit `<enter>` and you should be able to see the output like this

```julia
julia> include("1_chapter/helloWorld.jl")
There is more than one way to say hello:
	Hello World!
	G'day World!
	Shalom World!

These squares are just perfect:
  0  1  4  9  16  25  36  49  64  81  10011-element Vector{Float64}:
  0.0
  1.0
  2.0
  3.0
  4.0
  5.0
  6.0
  7.0
  8.0
  9.0
 10.0
```

## VS code

You can also run the codes from VS code.
Use the menu item `File->Open Folder` to open the source code
folder (where the `Manifest.toml` and `Project.toml` are).
Then you can use the file browser on the left-panel to open a source file which you
want to edit or run.

To run the whole file, simply hit `<Alt-Shift-Enter>`. 
A console panel will open automatically and show you the output. 
See more [here](https://www.julia-vscode.org/) for working with Julia in VS code.
