module Week1

using Random, Roots

export squares, area

"""
    squares(n)

Return a list of perfect squares which are at most `n`

# Examples
```julia-repl
julia> squares(3)
[1]

julia> squares(10)
[1, 4, 9]
```
"""
function squares(n)
    # Finish this function
end

"""
    area(f)

Let `f` be a function with `f(0) == 0` and `f(1) == 1` with `0 <= f(x) <=1`.
Then `area(f)` Estimate the area between `f(x)` and the x-axis.

# Examples
```julia-repl
julia> area(x->x^2)
0.33272

julia> area(x->x)
0.50065
```
"""
function area(f)
end

"""
    stablepts(a)

Let `a` be the list of coefficients in a polynomial. 
For example `a = [1, 0, 2]` represents the polynomial `1 + 0*x + 2^x^2`.
Then `stablepts(a)` find where the polynomials has derivative of 0 between -10 and 10.

# Examples
```julia-repl
julia> stablepts([1, 1, 0])
Float64[]

julia> stablepts([0, 0, 1])
1-element Vector{Float64}:
 0.0

julia> stablepts([0, 3, 1, 2, 1])
1-element Vector{Float64}:
 -1.5
```
"""
function stablepts(a)
end

"""
    rec_seq(a, b, n)

This function computes the n-th term of the sequence \$f_n\$ for which 
\$f_1 = a\$, \$f_2 = b\$ and \$f_{n} = f_{n-1}+f_{n-2}\$ if \$n > 2\$.

Hint:

1. Try test your function with `a=1`, `b=1` and `n=100`.
2. Check out this [post](https://stackoverflow.com/a/52084004/134852)
"""
function rec_seq(a, b, n)
end

"""
    myplot(f)

Let `f(x)` be a real-valued function.
`myplot(f)` returns a plot with two sub-plots.

1. The plot of `f(x)` from `x=0` to `x=100`.
2. The histogram of `[f(1), f(2), ..., f(100)]`

Note: This one will be graded manually.
"""
function myplot(f)
end

end
