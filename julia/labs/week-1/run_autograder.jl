module AutoGrader
    using Gradescope

    include("week-1.jl")
    import .Week1
    export test

    module Solution
        using Random, Roots

        export squares

        function squares(n)
            sqrtn = floor(Int, sqrt(n))
            return [i^2 for i in 1:sqrtn]
        end

        function area(f)
            Random.seed!()
            N = 10^6
            data = [[rand(),rand()] for _ in 1:N]
            indata = filter((x)-> (x[2] <= f(x[1])), data)
            approxarea = length(indata)/N
            return approxarea
        end

        function stablepts(a)
            n = length(a)-1
            poly = function(x)
                return sum([a[i+1]*i*x^(i-1) for i in 1:n])
            end

            return find_zeros(poly, -10, 10)
        end

        function rec_seq(a, b, n)
            d = Dict{BigInt, BigInt}()
            return rec_seq!(a, b, n, d)
        end
        
        function rec_seq!(a, b, n, d)
            get!(d, n) do
                if n == 1
                    a
                elseif n == 2
                    b
                else
                    rec_seq!(a, b, n-2, d) + rec_seq!(a, b, n-1, d) 
                end
            end
        end
    end

    function compare(ret::Real, expect::Real)
        return round(ret-expect, digits=2) == 0.0
    end

    function compare((ret, expect))
        return compare(ret, expect)
    end

    function compare(ret::Vector{<:Real}, expect::Vector{<:Real})
        return all(compare.(zip(ret, expect)))
    end

    function compare(ret, expect)
        return ret == expect
    end

    function test(expr, ret, expect, name)
        score = 0
        output = """Failed test:
        `$expr`.

        Expected result:
        $expect

        Actual result:
        $ret
        """
        if compare(ret, expect)
            score = 1
            output = "Pass"
        end
        t = TestCase(name=name,
                        max_score=1, 
                        score = score,
                        output = output,
                        extra_data = Dict("ret"=>ret, "expect"=>expect))
        return t
    end

    function test1()
        n = rand(1:1000)
        expr = "squares($n)"
        ret = Week1.squares(n) 
        expect = Solution.squares(n)
        return test(expr, ret, expect, "squares")
    end

    function test2()
        a = rand(1:10)
        b = rand(1:10)
        f = x->x^a*exp(b*(x-1))
        expr = "area(x->x^$a*exp($b*(x-1))"
        ret = Week1.area(f) 
        expect = Solution.area(f)
        return test(expr, ret, expect, "area")
    end

    function test3()
        m = rand(3:5)
        a = rand(-3:3, m)
        expr = "stablepts($a)"
        ret = Week1.stablepts(a)
        expect = Solution.stablepts(a)
        return test(expr, ret, expect, "stablepts")
    end

    function test4()
        n = rand(100:150)
        a = rand(-3:3)
        b = rand(-3:3)
        expr = "rec_seq($a, $b, $n)"
        ret = Week1.rec_seq(a, b, n)
        expect = Solution.rec_seq(a, b, n)
        return test(expr, ret, expect, "rec_seq")
    end

    function result()
        tests = TestCase[test1(), test2(), test3(), test4()]

        return Results(tests = tests)
    end

    Gradescope.output(result())
end

AutoGrader.result()
