module Week3

using Random, QuadGK, Distributions, SpecialFunctions, Plots, ForwardDiff
"""
    expect(g, a, b)

Compute the expectation of \$g(X)\$ where \$X\$ is Gamma(a,b)

# Examples
```julia-repl
julia> Week3.expect(x->1/x, 3, 4)
1.9999999999999951

julia> Week3.expect(x->x, 3, 4)
0.7500000000000001

julia> Week3.expect(x->3/x, 3, 4)
5.999999999999985
```
"""
function expect(g, a, b)
    # Finish this function

    dist = Gamma(a, 1/b)
    f(x) = pdf(dist, x)

    quadgk(x-> g(x)*f(x), 0, Inf)[1]
end

"""
    inv_digamma(y)

Implement Newton's method to find `α` such that `digmma(α) = y`.

# Examples
```julia-repl
julia> α = Week3.inv_digamma(0)
1.4616168773114884

julia> digamma(α)
-1.4774191018957113e-5

julia> α = Week3.inv_digamma(3)
20.575150223196832

julia> digamma(α)
2.9995860863092774
```
# Hint 

You will need SpecialFunctions and ForwardDiff
"""
function inv_digamma(y)
    ## Newton's method
    function newton(f, α0)
        ## Take the derivative
        df =  x -> ForwardDiff.derivative(f, float(x))
        α1 = α0 - f(α0)/df(α0)
        return α1
    end

    ## Staring point
    α = 1.0

    ## The function we need to solve
    f(α) = digamma(α)-y

    ## Better and better approximate solution
    while abs(f(α)) > 0.001
        α = newton(f, α)
    end
    return α
end

"""
    conf_int(xs, λ)

As `xs` are iid rvs of normal distribution with unknown mean \$μ\$
and variance \$σ^2\$.

Compute \$A\$ such that the probability of \$μ > A\$ is exactly λ.

# Examples
#
```julia-repl
julia> using Random; Random.seed!(10); # Make sure we get the same xs

julia> xs = rand(Normal(0.7, 1.2), 30);

julia> A = Week3.conf_int(xs, 0.9)
0.25605366012528186
```
"""
function conf_int(xs, λ)
    n = length(xs)

    dist = TDist(n-1)

    m = mean(xs)
    sp = std(xs, corrected=true)

    return m - quantile(dist, λ)*sp/sqrt(n)
end


"""
    poisson()

Verify that a Passion distribution with parameter 
θ has mean θ and variance θ
by return a plot which contains
the sample mean and sample variance
of `N = 10^3` Poisson(θ) random variables
for θ in `0.1:0.1:3`.

Note: This will be graded manually.
"""
function poisson()
    # Finish this function
end

end
