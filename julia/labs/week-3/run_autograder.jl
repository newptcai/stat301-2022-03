module AutoGrader
    using Gradescope, Random, Distributions, SpecialFunctions

    include("week-3.jl")
    import .Week3
    export test

    module Solution
        using QuadGK, Distributions, SpecialFunctions, Plots, ForwardDiff

        function expect(g, a, b)
            # Finish this function

            dist = Gamma(a, 1/b)
            f(x) = pdf(dist, x)

            quadgk(x-> g(x)*f(x), 0, Inf)[1]
        end

        function inv_digamma(y)
            ## Newton's method
            function newton(f, α0)
                ## Take the derivative
                df =  x -> ForwardDiff.derivative(f, float(x))
                α1 = α0 - f(α0)/df(α0)
                return α1
            end

            ## Staring point
            α = 1.0

            ## The function we need to solve
            f(α) = digamma(α)-y

            ## Better and better approximate solution
            while abs(f(α)) > 0.001
                α = newton(f, α)
            end
            return α
        end

        function conf_int(xs, λ)
            n = length(xs)

            dist = TDist(n-1)

            m = mean(xs)
            sp = std(xs, corrected=true)

            return m - quantile(dist, λ)*sp/sqrt(n)
        end
    end

    function compare(ret::Real, expect::Real)
        if expect ≈ 0
            d = round(ret-expect, digits=1) 
        else
            d = round(ret/expect-1, digits=1) 
        end
        @show ret, expect, d
        return d == 0.0
    end

    function compare((ret, expect))
        return compare(ret, expect)
    end

    function compare(ret::Vector{<:Real}, expect::Vector{<:Real})
        return all(compare.(zip(ret, expect)))
    end

    function compare(ret, expect)
        return ret == expect
    end

    function test(expr, ret, expect, name, score)
        output = """Failed test:
        `$expr`.

        Expected result:
        $expect

        Actual result:
        $ret
        """
        s = 0
        if compare(ret, expect)
            s = score
            output = "Pass"
        end
        t = TestCase(name=name,
                        max_score=score, 
                        score = s,
                        output = output,
                        extra_data = Dict("ret"=>ret, "expect"=>expect))
        return t
    end

    function test1()
        β = rand(1:10)
        α = rand(1:10)
        s = rand(-1:5)
        t = rand(-1:5)
        g(x) = s*x^t
        expr = "expect(x->$s*x^$t, $α, $β)"
        expect = Solution.expect(g, α, β) 
        ret = nothing
        try
            ret = Week3.expect(g, α, β) 
        catch e
            ret = e
        end
        return test(expr, ret, expect, "expect", 0.25)
    end

    function test2()
        α = rand(Uniform(0.5, 5))
        @show α
        y = digamma(α)
        expr = "inv_digamma($y)"
        expect = Solution.inv_digamma(y)
        ret = nothing
        try
            ret = Week3.inv_digamma(y)
        catch e
            ret = e
        end
        return test(expr, ret, expect, "inv_digamma", 0.25)
    end

    function test3()
        μ = rand(Uniform())
        σ = rand(Uniform())
        n = rand(3:5)
        λ = rand(Uniform(0.8, 0.99))
        xs = rand(Normal(μ, σ), n)
        expect = Solution.conf_int(xs, λ)
        expr = "conf_int($xs, $λ)"
        ret = nothing
        try
            ret = Week3.conf_int(xs, λ)
        catch e
            ret = e
        end
        return test(expr, ret, expect, "conf_int", 0.25)
    end

    function result()
        t1 = [test1() for _ in 1:4]
        t2 = [test2() for _ in 1:4]
        t3 = [test3() for _ in 1:4]

        return Results(tests = [t1; t2; t3])
    end

    ret = result()
    Gradescope.output(ret)

    function show()
        for t in ret.tests
            println(t.output)
        end
    end
end
