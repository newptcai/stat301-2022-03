module AutoGrader
    using Gradescope

    include("week-2.jl")
    import .Week2
    export test

    module Solution
        using Random

        function pwdmatch(pwd, passLength, numMatchesForLog)
            passLength = min(passLength, length(pwd))
            if passLength < numMatchesForLog
                return 0.0
            end
            possibleChars = ['a':'z' ; 'A':'Z' ; '0':'9']
            correctPassword = pwd
            numMatch(loginPassword) =
            sum([loginPassword[i] == correctPassword[i] for i in 1:passLength])
            N = 10^7
            passwords = [String(rand(possibleChars,passLength)) for _ in 1:N]
            numLogs = sum([numMatch(p) >= numMatchesForLog for p in passwords])
            return numLogs/N
        end

        function couponcollector(numCoupon, minCoupon)
            function sim()
                couponCount = zeros(Int, numCoupon)
                couponBought = 0
                while minimum(couponCount) < minCoupon
                    coupon = rand(1:numCoupon)
                    couponCount[coupon] += 1
                    couponBought += 1
                end
                return couponBought
            end

            N = 10^5
            totalCoupon = sum([sim() for _ in 1:N])
            return totalCoupon/N
        end

        function cupsacuer(n)
            saucers = vcat([fill(i, n) for i in 1:3]...)
            cupNum = 3*n

            function nopair()
                cups = Random.shuffle(saucers)
                for i in 1:cupNum
                    if saucers[i] == cups[i]
                        return false
                    end
                end
                return true
            end

            N = 10^6
            totalnopair = sum([nopair() for _ in 1:N])

            return  totalnopair/N
        end
    end

    function compare(ret::Real, expect::Real)
        if expect ≈ 0
            d = round(ret-expect, digits=1) 
        else
            d = round(ret/expect-1, digits=1) 
        end
        @show ret, expect, d
        return d == 0.0
    end

    function compare((ret, expect))
        return compare(ret, expect)
    end

    function compare(ret::Vector{<:Real}, expect::Vector{<:Real})
        return all(compare.(zip(ret, expect)))
    end

    function compare(ret, expect)
        return ret == expect
    end

    function test(expr, ret, expect, name, score)
        output = """Failed test:
        `$expr`.

        Expected result:
        $expect

        Actual result:
        $ret
        """
        s = 0
        if compare(ret, expect)
            s = score
            output = "Pass"
        end
        t = TestCase(name=name,
                        max_score=score, 
                        score = s,
                        output = output,
                        extra_data = Dict("ret"=>ret, "expect"=>expect))
        return t
    end

    function test1()
        n = rand(3:5)
        m = rand(1:n-1)
        passLength = rand(1:10)
        possibleChars = ['a':'z' ; 'A':'Z' ; '0':'9']
        pwd = String(rand(possibleChars,passLength))
        expr = "pwdmatch(\"$pwd\", $n, $m)"
        ret = Week2.pwdmatch(pwd, n, m) 
        expect = Solution.pwdmatch(pwd, n, m) 
        return test(expr, ret, expect, "pwdmatch", 0.5)
    end

    function test2()
        n = rand(10:30)
        m = rand(1:3)
        expr = "couponcollector($n, $m)"
        ret = Week2.couponcollector(n, m) 
        expect = Solution.couponcollector(n, m) 
        return test(expr, ret, expect, "couponcollector", 1.5)
    end

    function test3()
        ts = TestCase[]
        for n in 1:4
            expr = "cupsacuer($n)"
            ret = Week2.cupsacuer(n) 
            expect = Solution.cupsacuer(n)
            t = test(expr, ret, expect, "cupsacuer", 0.25)
            push!(ts, t)
        end
        return ts
    end

    function result()
        t1 = [test1() for _ in 1:2]
        t2 = [test2() for _ in 1:2]
        t3 = test3()

        return Results(tests = [t1; t2; t3;])
    end

    ret = result()
    Gradescope.output(ret)
end
