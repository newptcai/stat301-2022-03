---
title: Week 2
subtitle: STATS 301 Statistics
author: Xing Shi Cai
numbersections: true
colorlinks: true
urlcolor: Maroon
marings: 1.5in
---

# Lab Time

In week 2, we will have four lectures. 
The lab will be moved to the recitation time Thursday night 20:10-21:10 (Beijing Time).

The reason for this change is:

1. There are too much to cover in the textbook. 😩
2. We have a student in Duke. I would like to have him participate the in the lab. 🌎 

# Self Study

In this week, please read and run *every* listing in Chapter 2 of Statistics with Julia.
Then go to [GradeScope](https://www.gradescope.com/) to 
finish the assignment "Week-2 Self Study".

In lab-2, your task will be solving some problems which are 
very similar to these in Chapter 2. 
But this time you will need to finish in limited time (1hour 15 minutes). 
So be prepared. 😉

# Quizzes

For each lecture on Thursdays from week 2 to week 7, 
we will spend the last few minutes on quizzes.

The problems in quizzes will be selected from the assignment of the previous week,
with some **modifications**.
The assignment problems are given at the end of slides for each lecture.

💣 The quizzes are **open** book.
But you are only allowed to use the **two textbooks** and **Julia**.
No other aid is allowed.

## How to take the quiz:

1. When the quiz starts, go to [GradeScope](https://www.gradescope.com/courses/365483), download the quiz PDF file.
2. If you have an iPad/Tablet, answer the questions directly on the PDF.
3. Otherwise, write on a piece of paper and scan it with your phone. (Ideally
   with a scanning app.)
4. You have in total 20 minutes to download, answer and upload.
5. Email me a copy of your answers if you have trouble uploading.

# Expectation

A brief clarification of what is expected from you --

1. Read and understand parts of Probability and Statistics which cover the concepts, examples, theorems and proofs mentioned in the slides.
2. Read, run and understand the code in Chapter 1-7, Statistics with Julia. (One
   chapter each week.)
3. Be able to solve the problems similar to these in the assignments.
4. Be able to finish these programming tasks given in the labs.
5. Use what you have learned to do some statistics on real-world data with Julia.

and what is **not** expected from you --

1. Read the textbooks in their entirety.
2. Reproduce proofs of theorems.

