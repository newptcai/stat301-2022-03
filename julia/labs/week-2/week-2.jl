module Week2

export pwdmatch, couponcollector, cupsacuer

using Random

"""
    pwdmatch(pwd, passLength, numMatchesForLog)

Let `pwd` be a password which consists of 
letters 'a'-'z', 'A'-'Z', and `0-9`.
Modify listing 2.2 to compute the probability that a uniform random 
password of length `passLength` matches at least `numMatchesForLog` letter in `pwd`.

# Examples
```julia-repl
julia> pwdmatch("3xyZu4vN", 8, 1)
0.1222424

julia> pwdmatch("x", 3, 1)
0.0161381

julia> pwdmatch("xyz", 8, 4)
0.0
```
"""
function pwdmatch(pwd, passLength, numMatchesForLog)
    # Finish this function
end

"""
    couponcollector(numCoupon, minCoupon)

Suppose each time you buy a bottle of soda, 
you get one coupon uniformly at random from `numCoupon` types of coupons.
Compute the average number of bottles you need to buy
so that you have at least `minCoupon` coupons of each type.

This is called the [Coupon-collector Problem](https://en.wikipedia.org/wiki/Coupon_collector%27s_problem).

# Examples
```julia-repl
julia> couponcollector(100, 1)
518.69087

julia> couponcollector(50, 2)
324.91004

julia> couponcollector(20, 3)
141.135803
```
"""
function couponcollector(numCoupon, minCoupon)
    # Finish this function
end

"""
    cupsacuer(n)

You are given `3n` pairs of cups and saucers --- `n` black, `n` red, and `n` blue.
If the cups are put on saucers uniformly at random,
compute via simulation the probability that no cup is put on a saucer of the same colour.

# Hint 

Use a vector `[1, ... 1, 2, ... 2, 3 ... 3]` to represent the position of saucers.
Shuffle this vector by [`Random.shuffle`](https://docs.julialang.org/en/v1/stdlib/Random/#Random.shuffle)
to get the position of cups.
Check if any cup sits on top of a saucer of the same colour.

# Example

```julia-repl
julia> cupsacuer(1)
0.333696

julia> cupsacuer(2)
0.1113
```
"""
function cupsacuer(n)
    # finish this function
end

end
