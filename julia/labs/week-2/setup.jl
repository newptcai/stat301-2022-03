using Gradescope
using ZipFile

# -------------------------------------------------------
# Create zip file for auto-grader
# -------------------------------------------------------
gradescope_repo = "https://github.com/newptcai/Gradescope.jl"
dir = "autograder"
grader = "run_autograder.jl"

# create run_autograder.jl
mkpath(dir)
cp(grader, joinpath(dir, grader), force=true)

# Create setup.sh and run_autograder
cd(dir)
Gradescope.create_setup(packages=["Random", gradescope_repo])
Gradescope.create_run_autograder()

# Create the zip file for auto-grader
w = ZipFile.Writer("autograder.zip")
Gradescope.addfile!(w, ["run_autograder",
                        "run_autograder.jl",
                        "setup.sh"])
close(w)

# Return to parent directory
cd("..")
