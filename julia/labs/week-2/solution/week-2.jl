module Week2

export pwdmatch, couponcollector

using Random

"""
    pwdmatch(pwd, passLength, numMatchesForLog)

Let `pwd` be a password which consists of 
letters 'a'-'z', 'A'-'Z', and `0-9`.
Modify listing 2.2 to compute the probability that a uniform random 
password of length `passLength` matches at least `numMatchesForLog` letter in `pwd`.

# Examples
```julia-repl
julia> pwdmatch("3xyZu4vN", 8, 1)
0.1222424

julia> pwdmatch("x", 3, 1)
0.0161381

julia> pwdmatch("xyz", 8, 4)
0.0
```
"""
function pwdmatch(pwd, passLength, numMatchesForLog)
    passLength = min(passLength, length(pwd))
    if passLength < numMatchesForLog
        return 0.0
    end
    possibleChars = ['a':'z' ; 'A':'Z' ; '0':'9']
    correctPassword = pwd
    numMatch(loginPassword) =
        sum([loginPassword[i] == correctPassword[i] for i in 1:passLength])
    N = 10^7
    passwords = [String(rand(possibleChars,passLength)) for _ in 1:N]
    numLogs = sum([numMatch(p) >= numMatchesForLog for p in passwords])
    return numLogs/N
end

"""
    couponcollector(numCoupon, minCoupon)

Suppose each time you buy a bottle of soda, 
you get one coupon uniformly at random from `numCoupon` types of coupons.
Compute the average number of bottles you need to buy
so that you have at least `minCoupon` coupons of each type.

This is called the [Coupon-collector Problem](https://en.wikipedia.org/wiki/Coupon_collector%27s_problem).

# Examples
```julia-repl
julia> couponcollector(100, 1)
518.69087

julia> couponcollector(50, 2)
324.91004

julia> couponcollector(20, 3)
141.135803
```
"""
function couponcollector(numCoupon, minCoupon)
    function sim()
        couponCount = zeros(Int, numCoupon)
        couponBought = 0
        while minimum(couponCount) < minCoupon
            coupon = rand(1:numCoupon)
            couponCount[coupon] += 1
            couponBought += 1
        end
        return couponBought
    end

    N = 10^5
    totalCoupon = sum([sim() for _ in 1:N])
    return totalCoupon/N
end

"""
    cupsacuer(n)

You are given `3n` pairs of cups and saucers --- `n` black, `n` red, and `n` blue.
If the cups are put on saucers uniformly at random,
compute via simulation the probability that no cup is put on a saucer of the same colour.

# Hint 

Use a vector `[1, ... 1, 2, ... 2, 3 ... 3]` to represent the position of saucers.
Shuffle this vector by [`Random.shuffle`](https://docs.julialang.org/en/v1/stdlib/Random/#Random.shuffle)
to get the position of cups.
Check if any cup sits on top of a saucer of the same colour.

# Example

```julia-repl
julia> cupsacuer(1)
0.333696

julia> cupsacuer(2)
0.1113
```
"""
function cupsacuer(n)
    saucers = vcat([fill(i, n) for i in 1:3]...)
    cupNum = 3*n

    function nopair()
        cups = Random.shuffle(saucers)
        for i in 1:cupNum
            if saucers[i] == cups[i]
                return false
            end
        end
        return true
    end

    N = 10^6
    totalnopair = sum([nopair() for _ in 1:N])

    return  totalnopair/N
end

end
