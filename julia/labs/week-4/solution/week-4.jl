module Week4

export loaddata, summarize

using CSV, DataFrames, CategoricalArrays, Statistics, StatsBase, Plots, StatsPlots

"""
    loaddata()

A simpler helper function to load Salmon data.

It should

1. Read data from `farmed-salmon.txt`
2. Keep only the columns `:Location` and `:Mirex`
3. Remove any row which contains missing data
4. Turn the column `:Location` into categorical data.
5. Make sure the column `:Mirex` is of type `Float64`
6. Return the resulted `DataFrame`

"""
function loaddata()
    data = CSV.read("./farmed-salmon.txt", DataFrame, missingstring="NA")
    select!(data,[:Location,:Mirex])
    dropmissing!(data)
    data.Location = categorical(data.Location)
    return data
end

"""
    summarize(data)

Take a `DataFrame` returned by `loaddata`, 
return a new `DataFrame` with each row corresponding to one location 
and with the following columns
```
[:Location, :Mean, :Var, :Min, :Max, :IQR]
```

# Hint

Consult listing `dataframeOperations.jl` in this [repository](https://github.com/newptcai/StatsWithJuliaBook)
"""
function summarize(data)
    combine(groupby(data, :Location), :Mirex 
            => (x -> [mean(x) var(x) minimum(x) maximum(x) iqr(x)]) 
            => [:Mean, :Var, :Min, :Max, :IQR])
end

"""
    pdfplot(data)

Take a `DataFrame` returned by `loaddata`,
return a plot which compares the smoothed PDFs of mirex pollution at different locations.
"""
function pdfplot(data)
end


"""
    normalplot(data)

Take a `DataFrame` returned by `loaddata`,
return a plot which compares the Normal Probability Plot
mirex pollution of each location.
"""
function normalplot(data)
end

"""
    qqplot(data)

Take a `DataFrame` returned by `loaddata`,
return a plot grid which  contains the quantile-quantile plot
of each pair of locations, like Figure 4.11 in the book.
"""
function qqplot(data)
end

"""
    boxplot(data)

Take a `DataFrame` returned by `loaddata`,
return a box plot in which boxes represent different locations,
like Fig 4.9 in the book.
"""
function boxplot(data)
end

end
