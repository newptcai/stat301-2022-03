module Week4

export logu

using Plots, StatsPlots, Distributions

function logu()
    dist = Uniform()
    N = 10^7
    data = .-(log.(rand(dist, N)))

    density(data, xlims=(0,10), linestyle=:dot)

    dist1 = Exponential(1)
    plot!(x->pdf(dist1, x), linestyle=:dot)
end

end
