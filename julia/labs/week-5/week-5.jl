module Week5
using CSV, DataFrames, Distributions, Plots

"""
    ttest(α0, μ0)

The function 

1. Load the data file "./SpiderLong.dat".
2. Extract ``X_1, \\ldots, X_n``, the anxiety level of arachnophobes 
   when they are exposed to pictures of spiders.
3. Run a t-test for the hypothesis ``H_0:`` μ > μ0,
   assuming that ``X_1, \\ldots, X_n`` are iid normal random 
   variables with mean ``μ`` and ``σ^2``,
   with significance level `α0`.
4. Compute the ``p``-value of the test
5. Print out the results
"""
function ttest(α0, μ0)
    data = CSV.read("./SpiderLong.dat", DataFrame)

    # You will need to change the value of these three according to the test result.
    reject = false
    p = 0.0
    u = 0.0

    # Finish the function

    # ......

    # Print the result
    decision = reject ? "rejects" : "fails to reject"
    criteria = reject ? "U ≥ $u" : "U ≤ $u"

    println("The t-test $decision the hypothesis μ ≤ $μ0 with signifcance level $α0 because $criteria.")
    println("The p-value is $p.")
end

"""
    ttest(α0)

The function 

1. Load the data file "./SpiderLong.dat".
2. Extract ``X_1, \\ldots, X_n`` and ``Y_1, \\ldots, Y_n``,
   the anxiety level of arachnophobes when they are 
   exposed to pictures of spiders or real spiders
3. Run a t-test for the hypothesis ``H_0: μ_1 > μ_2``, assuming that 
   ``X_1, \\ldots, X_n`` are iid normal random variables with mean ``μ_1`` and ``σ^2``,
   ``Y_1, \\ldots, Y_n`` are iid normal random variables with mean ``μ_2`` and ``σ^2``,
   with significance level `α0`.
4. Compute the ``p``-value
5. Print out the results in the fashion similar to `ttest(α0, μ0)`
"""
function ttest(α0)
    data = CSV.read("./SpiderLong.dat", DataFrame)

    # Finish the function
end

"""
    paired_ttest(α0)

The function 

1. Load the data file "./SpiderWide.dat".
2. Extract ``X_1, \\ldots, X_n`` and ``Y_1, \\ldots, Y_n``,
   the anxiety level of the same arachnophobes when they are 
   exposed to pictures of spiders or real spiders
3. Use the paired t-test to answer the question --
   "Are arachnophobes more scared of pictures of spiders or real spiders?"
4. Print out the results in the fashion similar to `ttest(α0, μ0)`
"""
function paired_ttest(α0)
    data = CSV.read("./SpiderWide.dat", DataFrame)
    # Finish the function
end

"""
    bayesestimate()

1. Load the data file "./SpiderLong.dat".
2. Extract ``X_1, \\ldots, X_n``, the anxiety level of arachnophobes 
   when they are exposed to pictures of spiders.
3. Assume ``X_1, \\ldots, X_n`` are iid normal distribution with
   mean `θ` and variance `9.29`, 
   where `θ` has a prior normal distribution with mean `25` and variance `9`.
4. Compute the pdf for the posterior and estimate `θ`, 
   using both the brute force method given in listing 5.18,
   and the conjugate families method given in listing 5.19.
5. Print your estimates.
6. Return a plot of the pdf of prior and the pdf of the posterior 
   (computed with the different methods).

Hint: See DeGroot Theorem 7.3.3
"""
function bayesestimate()
    # Finish the function
    data = CSV.read("./SpiderLong.dat", DataFrame)
    # Finish the function
end

end
