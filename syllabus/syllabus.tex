%! TEX program = xelatex
\documentclass[11pt]{article}

% Misc packages
\usepackage{ifthen}
\usepackage{xspace}
\usepackage{ifpdf}
\usepackage{framed}
\usepackage{amsmath}
\usepackage{amssymb}

% Colours
\PassOptionsToPackage{dvipsnames,svgnames,x11names,table}{xcolor}
\usepackage{xcolor}
\definecolor{dku-blue}{HTML}{003A81}
\definecolor{dku-green}{HTML}{006F3F}

% Fonts
\usepackage[no-math]{fontspec}
\usepackage{xunicode}
\usepackage{xltxtra}
\defaultfontfeatures{Mapping=tex-text,Numbers=Lining}
% \newfontfamily\cnfont{Hiragino Sans GB}
\AtBeginDocument{%
    \setmainfont{Carlito}%
    \setsansfont[
        BoldFont={Carlito}]{Carlito}%
    \raggedbottom%
}
% \usepackage[sf]{noto}

% Multi-language support
\usepackage{polyglossia}
\setmainlanguage{english}

% Links
\usepackage{url}
\renewcommand{\UrlBreaks}{\do\.\do\@\do\\\do\/\do\!\do\_\do\|\do\;\do\>\do\]%
    \do\)\do\,\do\?\do\'\do+\do\=\do\#\do\-}
\renewcommand{\UrlFont}{\normalfont}
\usepackage{hyperref} % Required for adding links	and customizing them
\hypersetup{colorlinks, breaklinks, urlcolor=Maroon, linkcolor=Maroon,
    citecolor=Green} % Set link colors
\hypersetup{%
    pdfauthor=Xing Shi Cai,%
    pdftitle=STATS 301 Statistics Syllabus}

% Geometry
\usepackage[a4paper,
    includehead,tmargin=2cm,nohead,
    hmargin=2cm,
    includefoot,foot=1.5cm,bmargin=2cm]{geometry}

% Tabular
\usepackage{tabularx}
\usepackage{longtable}
\usepackage{booktabs}
\usepackage{array}
\newcommand\gvrule{\color{lightgray}\vrule width 0.5pt}

% From pandoc
\renewcommand{\tabularxcolumn}[1]{m{#1}}
\usepackage{calc} % for calculating minipage widths
% Correct order of tables after \paragraph or \subparagraph
\usepackage{etoolbox}
\makeatletter
\patchcmd\longtable{\par}{\if@noskipsec\mbox{}\fi\par}{}{}
\makeatother
% Allow footnotes in longtable head/foot
\IfFileExists{footnotehyper.sty}{\usepackage{footnotehyper}}{\usepackage{footnote}}
\makesavenoteenv{longtable}
\usepackage{graphicx}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
% Set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
    \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering
\ifLuaTeX
    \usepackage{selnolig}  % disable illegal ligatures
\fi

% For including pictures from current directory
\graphicspath{.}

% Misc
\usepackage{enumitem}
\usepackage{parskip}
\usepackage{hanging}

\newcommand{\push}{\hangpara{2em}{1}}

% Section titles
\usepackage{titlesec}
\titleformat{\section}{\raggedright\large\bfseries\color{dku-blue}}{}{0em}{}[{\titlerule[\heavyrulewidth]}]
\titleformat{\subsection}{\raggedright\itshape\color{dku-blue}}{}{0em}{}

% Emoji
\usepackage{xelatexemoji}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{tabularx}{\textwidth}{Xm{6cm}}
    \arrayrulecolor{dku-blue}\toprule\vspace{6pt}
    {\bfseries\color{dku-blue}
        {\Large STATS 301} \newline\newline
        {\huge Statistics} \newline\newline
        {\Large Spring 2022, Session 4}}
     & \includegraphics[width=6cm]{dku-logo.pdf} \\
    \bottomrule
\end{tabularx}

\vspace{1em}

\textcolor{dku-blue}{\rule{\textwidth}{1pt}}

{
    \setlength{\parskip}{0.2em}
    \textbf{Class meeting time:}
    See DKU Hub.

    \textbf{Academic credit:} 4 DKU credits.

    \textbf{Course format:}
    Lectures.

    \textbf{Asynchronous study:}
    For students who cannot attend the lectures in person or remotely, slides and video
    recordings will be provided.

    \textbf{Office hour:}
    Two one-hour online sessions per week.
    In-person meetings can be arranged upon request.

    \textbf{Last update:} \today{}
}

\vspace{-1em}\textcolor{dku-blue}{\rule{\textwidth}{1pt}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Instructor's information}

Dr.\ \href{https://newptcai.gitlab.io/}{Xing Shi Cai}, Assistant Professor of
Mathematics, Duke Kunshan University

Email: \href{mailto:xingshi.cai@dukekunshan.edu.cn}{xingshi.cai@dukekunshan.edu.cn}

Dr.\ Cai received his PhD in computer science at McGill University in Canada.
After that he worked as postdoc researcher at Uppsala University in Sweden.
His main research interest is in applying probability theory in the analysis of
algorithms and complex networks.

\section*{What is this course about?}

This course, STATS 301, introduces some basic concepts, methods and
applications in inference theory from theoretical, empirical, and
practical points of view. By the end of the course, students will be
able to choose appropriate inference methods to solve concrete problems
such as comparing the popularity of two flavours of ice creams or
predicting the age-gap between a married couple.

This course covers topics similar to that of MATH 205 (Probability and
Statistics), but goes deeper in the subject matter and includes an extra
practical component. Students who have taken STATS 101 (Introduction to
Applied Statistical Methods) may find STATS 301 (as well as MATH 205)
offers a mathematical formulation for the statistics methods they have
learned. For those who intend to study probability courses such as MATH
301 (Advanced Introduction to Probability) and MATH 404 (Stochastic
Modeling \& Computing), the concrete intuition of probabilities built
through computer experiments in STATS 301 will be useful.

The practical and experimental part of the course will be done using
\href{https://julialang.org/}{Julia}, a modern,
high-performance, dynamic, and open-source programming language designed
for scientific computations.

\section*{What background knowledge do I need before taking this course?}

Prerequisite: MATH 201 Multivariable Calculus and MATH 205 Probability
and Statistics.

You should be able to type reasonably fast on a standard English
keyboard.

Previous experience with any programming language is preferred but not
required.

\section*{What will I learn in this course?}

By the end of this course, you will be able to:

\begin{itemize}
\item
  Explain basic concepts and methods in inference theory;
\item
  Present statistical analysis in spoken or written form;
\item
  Use computers (Julia) to solve concrete statistics problems.
\end{itemize}

The topics covered in this course include:

\begin{itemize}
\item
  Prior and Posterior Distributions, Conjugate Prior Distributions,
  Bayes Estimators
\item
  Maximum Likelihood Estimators
\item
  The Sampling Distribution of a Statistic, The Chi-Square
  Distributions, The t-distributions, Confidence Intervals, Fisher
  Information
\item
  Testing Simple Hypotheses, Uniformly Most Powerful Tests, Uniformly
  Most Powerful Tests, The t Test, Comparing Two Normal Distributions
\item
  Goodness-of-Fit, Contingency Tables, Tests of Homogeneity
\item
  Linear Regression
\end{itemize}

\section*{What will I do in this course?}

\subsection*{Theory part}

\subsubsection*{Lectures}

You will attend three lectures each week which cover the theoretical
part of the course.

\subsection*{Assignment}
There are going to be one assignment set for each week together with the solutions
given to you.
You should try to solve the problems and compare your answers with the standard
solution by yourself.

\subsection*{Quiz}
After a assignment set has been given for a week,
there will be an in-class quiz consisting of problems from the assignment.

\subsubsection*{Exam}

There are going to be two exams ---
one midterm exam in week 4,
and one final exam in week 8.

\subsection*{Practical part}

\subsubsection*{Self-study}

At the beginning of each week, 
you will be given text to read 
and computer experiments to run,
which should be finished before the week's laboratory. 

\subsubsection*{Laboratories}
You will attend a laboratory once per week and 
to solve problems similar to what you have encountered in self-study.
The result of your work will be collected by the end of the lab and graded.

\subsubsection*{Project}
Students will be randomly divided into groups of 2-3 and work on a project. 

\subsection*{Experiential activity}

We will invite a statistician who works in industries to give a talk
about real-world applications of statistics. Attendance will be
optional.

\section*{How can I prepare for the class sessions to be successful?}

To succeed, you should be prepared to devote several hours to this
course on a daily basis. You are strongly encouraged to use the tutoring
resources of ARC, to work with classmates, to seek information online,
and to contact instructors in a timely manner for additional help as
needed.

\section*{What required texts, materials, and equipment will I need?}

We will use two textbooks:

\begin{itemize}
\item
  DeGroot and Shervish, Probability and Statistics, 4th edition. (The
  same as MATH 205.)
\item
  Statistics with Julia: Fundamentals for Data Science, Machine Learning and Artificial
  Intelligence by Yoni Nazarathy and Hayden Klok. (We will use the
  free draft version.)
\end{itemize}

Library textbook contact: Xue QIU,
\href{mailto:xue.qiu@dukekunshan.edu.cn}{\nolinkurl{xue.qiu@dukekunshan.edu.cn}} , Office: AB 2112

\section*{How will my grade be determined?}

Course grades will be assigned according to a standard 10-pt scale:

\begin{longtable}[h]{@{}llll@{}}
\toprule
Grades & Percentage & C+ & [77\%, 80\%) \\
\midrule
\endhead
A+ & [98\%, 100\%] & C & [73\%, 77\%) \\
A & [93\%, 98\%) & C- & [70\%, 73\%) \\
A- & [90\%, 93\%) & D+ & [67\%, 70\%) \\
B+ & [87\%, 90\%) & D & [63\%, 67\%) \\
B & [83\%, 87\%) & D- & [60\%, 63\%) \\
B- & [80\%, 83\%) & F & [0\%, 60\%) \\
\bottomrule
\end{longtable}

Points are rounded downwards, e.g., 92.99 will become to 92 (A-).
Petitions for increasing grades will \emph{not} get replies.
Regrade requests will be \emph{rejected} unless there is a misreading of the answers.

Answers for the quizzes and exams will not only be judged according to mathematical
correctness, but also \emph{correct} usage of English language.

The course grade will be based on:

\begin{itemize}
    \item Theory: 65%

        \begin{itemize}
            \item Assignment-Quiz: 20\%
            \item Midterm exam: 20\%
            \item Final exam: 25\%
        \end{itemize}
    \item Practice: 35\%

        \begin{itemize}
            \item Self-study: 5\%
            \item Laboratory: 20\%
            \item Project: 10\%
        \end{itemize}
\end{itemize}

A maximum of 2\% bonus points will be given to students who actively participate in
online and in-class discussions.

\section*{What are the course policies?}

\subsection*{Q \& A}

Questions \emph{must} be posted on
\href{https://edstem.org}{Ed-Discussion}.
Emails will \emph{not} get replied.

\subsection*{Remote learning}

If you are not able to come to classes or exams in-person, you will need
to send the instructor
\begin{itemize}
    \item the permission from the Dean of Undergraduate Studies,
    \item or proof of COVID-related restrictions for travelling inside China,
    \item or proof of other special circumstances.
\end{itemize}
Failing to do so will result in falling the class.

\subsection*{Classroom management}

Using laptops are \emph{not} allowed during lectures.
If you want to take notes,
use a paper notebook or a tablet computer.

Sit next to another student. We will have group activities in class.

Turn on your camera if you attend remotely.
Otherwise you will get kicked out of the meeting.

\subsection*{Quizzes and the final exam}

All quizzes and the exams will be closed book.
Books, notes, collaborations, calculators, the Internet, 
and other aids are \emph{not} allowed during exams.
Cheating for the first time will result in getting zero points.
Repeated infractions will cause failing the course.

We do not give make-up quizzes or exams.
An unexcused absence from a quiz or exam will be counted as a zero.
Excuses may be accepted, at the discretion of the instructor.
If an excuse is approved, then your grade of the final exam
will be counted as the grade of the quiz.

The final exam must be taken during the assigned time.
Make-up final exam (if approved) will take place at a certain time in the following
semester/session arranged by Dean's office and the instructor.

\subsection*{Academic Integrity}
As a student, you should abide by the academic honesty standard of the Duke Kunshan
University.
Its Community Standard states: ``Duke Kunshan University is a community comprised of
individuals from diverse cultures and backgrounds.
We are dedicated to scholarship, leadership, and service and to the principles of
honesty, fairness, respect, and accountability.
Members of this community commit to reflecting upon and upholding these principles
in all academic and non-academic endeavors, and to protecting and promoting a
culture of integrity and trust.''
For all graded work, students should pledge that they have neither given nor
received any unacknowledged aid.

\subsection*{Academic Policy \& Procedures}
You are responsible for knowing and adhering to academic policy and procedures as
published in University Bulletin and Student Handbook.
Please note, an incident of behavioral infraction or academic dishonesty (cheating
on a test, plagiarizing, etc.) will result in immediate action from me, in
consultation with university administration (e.g., Dean of Undergraduate Studies,
Student Conduct, Academic Advising).
Please visit the Undergraduate Studies website for additional guidance related to
academic policy and procedures.
Academic integrity is everyone's responsibility.

Please refer to \href{https://undergrad.dukekunshan.edu.cn/en/undergraduate-bulletin}{Undergraduate Bulletin} and visit the
\href{https://dukekunshan.edu.cn/en/academics/advising}{Office of Undergraduate
Advising website}.

\subsection*{Academic Disruptive Behavior and Community Standard}
Please avoid all forms of disruptive behavior, including but not limited to: verbal
or physical threats, repeated obscenities, unreasonable interference with class
discussion, making/receiving personal phone calls, text messages or pages during
class, excessive tardiness, leaving and entering class frequently without notice of
illness or other extenuating circumstances, and persisting in disruptive personal
conversations with other class members.
Please turn off phones, pagers, etc., during class unless instructed otherwise.
Laptop computers may be used for class activities allowed by the instructor during
synchronous sessions.
If you choose not to adhere to these standards, I will take action in consultation
with university administration (e.g., Dean of Undergraduate Studies, Student
Conduct, Academic Advising).

\subsection*{Academic Accommodations}
If you need to request accommodation for a disability, you need a signed
accommodation plan from Campus Health Services, and you need to provide a copy of
that plan to me.
Visit the Office of Student Affairs website for additional information and
instruction related to accommodations.

\section*{What campus resources can help me during this course?}

\subsection*{Academic Advising and Student Support}
Please consult with me about appropriate course preparation and
readiness strategies, as needed. Consult your academic advisors on
course performance (i.e., poor grades) and academic decisions (e.g.,
course changes, incompletes, withdrawals) to ensure you stay on track
with degree and graduation requirements. In addition to advisors, staff
in the Academic Resource Center can provide recommendations on academic
success strategies (e.g., tutoring, coaching, student learning
preferences). All ARC services will continue to be provided online.
Please visit the
\href{https://dukekunshan.edu.cn/en/academics/advising}{Office
of Undergraduate Advising website} for additional information related
to academic advising and student support services.

\subsection*{Writing and Language Studio}
For additional help with academic writing---and more generally with
language learning---you are welcome to make an appointment with the
Writing and Language Studio (WLS). To accommodate students who are
learning remotely as well as those who are on campus, writing and
language coaching appointments are available in person and online. You
can register for an account, make an appointment, and learn more about
WLS services, policies, and events on the
\href{https://dukekunshan.edu.cn/en/academics/language-and-culture-center/writing-and-language-studio}{WLS
website}. You can also find writing and language learning resources on
the \href{https://sakai.duke.edu/x/mQ6xqG}{Writing \&
Language Studio Sakai site}.

\subsection*{Online resources}

For statistics related questions, the follows websites may be helpful:
\begin{itemize}
\item
  \href{https://math.stackexchange.com/}{MSE (Math Stack
  Exchange)} --- Ask questions about mathematics and get answers from
  other users.
\item
  \href{https://stats.stackexchange.com/}{Cross Validated}
  --- Like MSE but for statistics.
\item
  \href{https://mathworld.wolfram.com/}{Wolfram Mathworld}
  --- A good place to look up mathematical definitions.
\end{itemize}
For Julia related questions, check these websites:
\begin{itemize}
\item
  \href{https://julialang.org/}{Official Julia Website} ---
  Documentations and other learning material
\item
  \href{https://discourse.julialang.org/}{Discourse} --- A
  forum for discussing Julia.
\item
  \href{https://julialang.org/slack/}{Julia Slack Channel}
  --- Casual conversations on Julia. A good place for getting help
  quickly.
\end{itemize}

\section*{What is the expected course schedule?}

💣️ Exact topics covered in each week may subject to change.

%\begin{longtable}{| p{.20\textwidth} | p{.80\textwidth} |}
%\hline
%foo & bar \\ \hline
%foo & bar \\ \hline
%foo & bar \\ \hline
%foo & bar \\ \hline
%foo & bar \\ \hline
%foo & bar \\ \hline
%foo & bar \\ \hline
%foo & bar \\ \hline
%foo & bar \\ \hline
%foo & bar \\ \hline
%foo & bar \\ \hline
%\caption{Your caption here} % needs to go inside longtable environment
%\end{longtable}

\begin{longtable}[]{@{}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.20}}
  >{\raggedright\arraybackslash}p{(\columnwidth - 2\tabcolsep) * \real{0.80}}@{}}
\toprule
\endhead
Week 1 &

Lectures: Statistical Inference, Prior and Posterior Distributions,
Conjugate Prior Distributions, Bayes Estimators [PS (Probability and
Statistics), 7.1-7.4]

Lab: Introduction to Julia [SJ (Statistics with Julia) 1] \\
\midrule
Week 2 &

Lectures: Maximum likelihood estimation (MLE), The Sampling Distribution
of a Statistic, The Chi-Square Distributions [PS 7.5-7.6, 8.1-8.2]

Lab: Basic Probability [SJ 2]
\\
\midrule
Week 3 &

Lectures: Sample Mean and Sample Variance, The t Distributions,
Confidence Intervals, Samples from a Normal Distribution [PS
8.3-8.6]

Lab: Processing and Summarizing Data [SJ 3]\\
\midrule
Week 4 &

Lectures: Unbiased Estimators, Fisher Information, Testing simple
hypotheses, NP paradigm/lemma [PS 8.7-8.8, 9.1-9.2]

Lab: Statistical Inference Concepts [SJ 4]
\\
\midrule
Week 5 &

Lectures: Uniformly Most Powerful Tests, Two-Sided Alternatives, The t
Test, Comparing the Means of Two Normal Distributions [PS 9.3-9.6]

Lab: Confidence Intervals [SJ 5]

💣️ Midterm exam\\
\midrule
Week 6 &

Lectures: Tests of Goodness-of-Fit, Goodness-of-Fit for Composite
Hypotheses, Contingency Tables, Tests of Homogeneity [PS 10.1-10.4]

Lab: Hypothesis Testing [SJ 6]
\\
\midrule
Week 7 &

Lectures: The Method of Least Squares, Regression, Statistical Inference
in Simple Linear Regression, Bayesian Inference in Simple Linear
Regression [PS 11.1-11.4]

Lab: Linear Regression [SJ 7]
\\
\midrule
Week 8 & 💣️ Final exam

💣️ Project presentation \\
\bottomrule
\end{longtable}

\end{document}
