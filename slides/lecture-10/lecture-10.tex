\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 10}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{8.6 Bayesian Analysis of Samples from a Normal Distribution}


\subsection{The Precision of a Normal Distribution}

\begin{frame}
    \frametitle{Definition 8.6.1}
    
    The \alert{precision} $τ$ of a normal distribution is 
    defined as the reciprocal of the variance; that is, $τ = 1/σ^2$.

    The \acs{rv} $X \sim \scN(\mu, 1/\tau)$ has a \acs{pdf}
    \begin{equation*}
        f(x|\mu, \tau)
        =
        \frac{\sqrt{\tau } }{\sqrt{2 \pi }}e^{-\frac{1}{2} \tau  (x-\mu )^2}
    \end{equation*}

    If $X_1,\ldots, X_n$ are \acs{iid} $\scN(\mu, 1/\tau)$, then the joint \acs{pdf}
    is
    \begin{equation*}
        f(x|\mu, \tau)
        =
        \left(
        \frac{\sqrt{\tau } }{\sqrt{2 \pi }}
        \right)^{n}
        e^{-\sum_{i=1}^{n} \frac{1}{2} \tau  (x_{i}-\mu )^2}
    \end{equation*}
\end{frame}

\subsection{A Conjugate Family of Prior Distributions}

\begin{frame}
    \frametitle{Theorem 8.6.1}
    
    Let $X_1,\ldots, X_n$ be \acs{iid} $\scN(\mu, 1/\tau)$.

    Assume that 
    \begin{equation*}
    \mu \sim \scN(\mu_{0}, 1/(\lambda_{0} \tau)),
    \qquad
    \tau \sim \Be(\alpha_{0}, \beta_{0}).
    \end{equation*}

    Given that $\bfX = \bfx$, we have
    \begin{equation*}
    \mu \sim \scN(\mu_{1}, 1/(\lambda_{1} \tau)),
    \qquad
    \tau \sim \Be(\alpha_{1}, \beta_{1}),
    \end{equation*}
    \pause{}
    where
    \begin{equation*}
        \mu_{1} =
        \frac{\lambda_{0} \mu_{0} + n \overline{x}_n}{\lambda_0 +n}
        ,
        \qquad
        \lambda_{1} = \lambda_{0} + n
        ,
    \end{equation*}
    and
    \begin{equation*}
        \alpha_{1} = \alpha_{0} + \frac{n}{2}
        ,
        \qquad
        \beta_{1} =
        \beta_{0} 
        + 
        \frac{1}{2}s_{n}^{2}
        +
        \frac
        {n \lambda_{0} (\overline{x}_n-\mu_{0})^{2}}
        {2(\lambda_{0} +n)}
        .
    \end{equation*}
\end{frame}

\begin{frame}[c]
    \frametitle{Definition 8.6.2}
    
    Assume that 
    \begin{equation*}
    \mu \sim \scN(\mu, 1/(\lambda \tau)),
    \qquad
    \tau \sim \Be(\alpha, \beta).
    \end{equation*}
    Then we say that the distribution of $(\mu, \tau)$ is
    the \alert{normal-gamma} distribution with hyperparameters
    $(\mu, \lambda, \alpha, \beta)$.
\end{frame}

\begin{frame}
    \frametitle{Example 8.6.2}
    

    Let $X_1,\ldots, X_n$ be the amount of acid in $n=10$ \emoji{cheese}. 

    Assume that $X_1,\ldots, X_n$ are \acs{iid} $\scN(\mu_{0}, 1/(\tau))$.

    Assume also that $(\mu, \tau)$ has the \alert{normal-gamma} distribution with hyperparameters
    \begin{equation*}
    (\mu_{0}, \lambda_{0}, \alpha_{0}, \beta_{0})
    =
    (1, 1, 0.5, 0.5).
    \end{equation*}

    If we have observed $\overline{x}_n = 1.345$  and $s_{n}^{2}=0.9633$, what is
    \begin{equation*}
        \p{\sigma> 0.3 | \bfx}
    \end{equation*}
\end{frame}

\subsection{The Marginal Distribution of the Mean}

\begin{frame}
    \frametitle{A useful lemma}

    If $X$ has \acs{pdf} $f(x)$, then $Y = a X + b$ has the \acs{pdf}
    \begin{equation*}
        g(y) = \frac{1}{a} f\left(\frac{y-b}{a}\right).
    \end{equation*}

    \cake{}
    If $X \sim \Gam(\alpha, \beta)$, then $X$ has the \acs{pdf}
    \begin{equation*}
        f(x)=
        \begin{cases}
            \frac{\left(\frac{1}{\beta }\right)^{-\alpha } x^{\alpha -1} e^{\beta  (-x)}}{\Gamma (\alpha )} 
            & 
            \text{for } x > 0, \\
            0
            & \text{otherwise}.
        \end{cases}
    \end{equation*}
    What is the \acs{pdf} of $Y = 2 \beta X$?
    What is the distribution of $Y$?
\end{frame}

\begin{frame}
    \frametitle{Theorem 8.6.2}
    
    Assume that $(\mu, \tau)$ has the \alert{normal-gamma} 
    distribution with hyperparameters
    $(\mu_{0}, \lambda_{0}, \alpha_{0}, \beta_{0})$.

    Then
    \begin{equation*}
        \left(\frac{\lambda_0 \alpha_0}{\beta_0}\right)^{1/2}
        (\mu-\mu_{0})
        \sim
        t(2 \alpha_0)
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Theorem 8.6.3}
    
    Assume that $(\mu, \tau)$ has the \alert{normal-gamma} 
    distribution with hyperparameters
    $(\mu_{0}, \lambda_{0}, \alpha_{0}, \beta_{0})$ with

    If $\alpha_{0} > 1/2$, then
    \begin{equation*}
        \E{\mu} = \mu_{0}.
    \end{equation*}

    If $\alpha_{0} > 1$, then
    \begin{equation*}
        \V{\mu} = 
        \frac{\beta_{0}}{\lambda_{0}(\alpha_0-1)}
        .
    \end{equation*}
\end{frame}

\subsection{A Numerical Example}

\begin{frame}
    \frametitle{Example 8.6.3 -- Prior}

    Assume that $X_1,\ldots, X_n$ are \acs{iid} $\scN(\mu_{0}, 1/(\tau))$ with $n =
    18$.

    Assume also that $(\mu, \tau)$ has the \alert{normal-gamma} distribution with hyperparameters
    \begin{equation*}
    (\mu_{0}, \lambda_{0}, \alpha_{0}, \beta_{0})
    =
    (200, 2, 2, 6300).
    \end{equation*}

    Then
    \begin{equation*}
        \begin{aligned}
            &
            \p{89 < μ < 311}
            \\
            &
            =
            \p{−2.776 < 0.025(μ − 200) < 2.776} = 0.95.
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 8.6.3 -- Posterior}

    If we observe
    \begin{equation*}
        \begin{aligned}
            \bfx=& 128,281,291,238,155,148,154,232,316,\\
                & 96,146,151,100,213,208,157,48,217.
        \end{aligned}
    \end{equation*}

    Then by computing the posterior of $\mu$, we have
    \begin{equation*}
        \begin{aligned}
        &
        \p{152.38 < μ < 215.52|\bfx}
        \\
        &
        =
        \p{−2.074 <  (0.0657)(μ − 183.95) < 2.074|\bfx}
        \\
        &
        = 0.95.
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 8.6.3 -- Comparison}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{8.7.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 8.6.3 -- Confidence Intervals}
    
    If we compute the confidence interval of $\mu$, then we get
    \begin{equation*}
        \p{A < \mu < B} = 0.95
    \end{equation*}
    where
    \begin{equation*}
        \begin{aligned}
        & A = \overline{X}_n − 2.110 \frac{\sigma'}{n^{1/2}} = 146.25, \\
        & B = \overline{X}_n + 2.110 \frac{\sigma'}{n^{1/2}} = 218.09.
        \end{aligned}
    \end{equation*}

    \emoji{smile} In many problems involving the normal distribution, 
    the method of confidence intervals and the method of using posterior
    probabilities yield similar results.
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-10.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 8.5, Exercise 1, 3, 5, 7, 9, 11, 13,
                    15.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
