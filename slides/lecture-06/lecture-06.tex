\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 06}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{7.6 Properties of Maximum Likelihood Estimators}

\subsection{\Acp{mle} and Bayes Estimators}

\begin{frame}
    \frametitle{Example 7.6.11 --- \acs{mle}}
    
    Let $X_1,\ldots, X_n$ be \ac{iid} $\Exp(\theta)$.
    Let $T_n = \sum_{i=1}^{n} X_{i}$.

    The \acs{mlee} of $\theta$ is $\hat{\theta}_{n} = n / T_{n}$ is approximately
    $\scN(\theta, \theta^{2}/n)$. (See Example 6.3.7.)

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{ex.7.6.11.png}
    \end{figure}

\end{frame}

\begin{frame}
    \frametitle{Example 7.6.11 --- Bayes estimator}
    
    Let $X_1,\ldots, X_n$ be \ac{iid} $\Exp(\theta)$.
    Let $T_n = \sum_{i=1}^{n} X_{i}$.

    Assuming prior of $\Gam(\alpha,\beta)$, the posterior is 
    \begin{equation*}
        \Gam(\alpha+n, \beta+t_{n})
        \approx
        \scN
        \left(
        \frac{\alpha+n}{\beta+t_{n}} 
        ,
        \frac{\alpha+n}{(\beta+t_{n})^{2}} 
        \right)
        .
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{ex.7.6.11-1.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 7.6.13}
    
    If in the previous example, we only take $n=3$, then the distributions are not
    close to normal anymore.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{7.8.png}
    \end{figure}
\end{frame}

\section{8.1 The Sampling Distribution of a Statistic}

\subsection{Statistics and Estimators}

\begin{frame}
    \frametitle{Example 8.1.1, 8.1.5}
    
    Let $X_1,\ldots, X_n$ be \acs{iid} $\Ber(\theta)$ with $n=40$.
    The \acs{mlee} of $\theta$,
    \begin{equation*}
    T = \frac{1}{n}\sum_{i=1}^{n} X_i
    \end{equation*}
    can be seen as \acs{rv}, 
    and we can ask what is $\p{\abs{T-\theta} > 0.1 | \theta}$?

    \cake{} What is the distribution of $n T$?

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{8.2.png}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Sampling distribution}
    
    Let $X_{1}, \ldots, X_{n}$ be \acs{iid} \acsp{rv} whose distribution 
    has unknown parameter $\theta$.

    Let $r:\dsR^{n}\times\Omega\mapsto \dsR$ be a function.

    Then $T = r(X_1,\ldots, X_n, \theta)$ can be seen as a \acs{rv} 
    before we make observations.

    The distribution of $T$ is called the \emph{sampling distribution} of $T$.
\end{frame}

\begin{frame}
    \frametitle{Example 8.1.2}
    
    Let $X_1,\ldots, X_n$ be \acs{iid} with a $\scN(\mu, \sigma^{2})$ distribution.

    Then by Corollary 5.6.2, 
    $\overline{X}_{n}$, the \acs{mlee} of $\theta$,
    has a \emph{sampling distribution} $\scN(\mu, \sigma^{2}/n)$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{ex.8.1.2.png}
    \end{figure}
\end{frame}

\subsection{Purpose of the Sampling Distribution}

\begin{frame}
    \frametitle{Example 8.1.3}
    
    Let $X_1,\ldots, X_n$ be \acs{iid} $\Exp(\theta)$ with $n=3$.
    Assume that $\theta = \Gam(1,2)$.
    Let $\hat{\theta}$ be the mean posterior distribution.

    We want to compute
    \begin{equation*}
        \p{\abs{\hat{\theta}-\theta} < 0.1}
    \end{equation*}
    before making observation.

    \pause{}
    \cake{} What is the posterior distribution in this example?

    \begin{block}{Theorem 7.3.4}
        Let $X_1,\ldots, X_n$ be \ac{iid} $\Exp(\theta)$, 
        where $\theta \sim \Gam(\alpha, \beta)$. 
        Then $(\theta | \bfx) \sim \Gam(\alpha + n, \beta + \sum_{i=1}^{n}x_{i})$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 8.1.4}
    
    How can we compute (with help of \emoji{laptop})
    \begin{equation*}
        \p{\abs{\hat{\theta}-\theta} < 0.1}
    \end{equation*}
    in the previous example?

    \hint{} $T = X_{1} + X_2 + X_3 \sim \Gam(3, 1)$. (Theorem 5.7.7)

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{8.1.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 8.1.5}
    
    Suppose instead we want to use the \acs{mlee} $\hat{\theta} = 3/T$ to estimate
    $\theta$. How to compute
    \begin{equation*}
        \p{\abs{\frac{\hat{\theta}}{\theta}-1}< 0.1\cond\theta}
    \end{equation*}
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-06.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 8.2, Exercise 1, 3, 5, 7, 9, 11.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
