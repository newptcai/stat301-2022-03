\input{../meta.tex}

\title{Lecture 25}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{11.3 Statistical Inference in Simple Linear Regression}

\subsection{Joint Distribution of the Estimators}

\begin{frame}[c]
    \frametitle{Example 11.3.1}
    
    In the Boiling Point of \emoji{droplet} example, we might want to test
    \begin{equation*}
        H_{0} : \beta_{0} + 201.5 \beta_{1} = 24.5.
    \end{equation*}

    Or we may want to find an interval estimate of
    \begin{equation*}
        \beta_{0} + 201.5 \beta_{1}.
    \end{equation*}

    This will be possible if we can find the joint distribution of the estimators of
    $(\beta_{0}, \beta_{1}, \sigma^{2})$.
\end{frame}

\begin{frame}
    \frametitle{Theorem 11.3.1}
    
    Suppose that the \acp{rv} $Y_1,\ldots, Y_n$ are independent, 
    and each has a normal distribution with the \emph{same variance} 
    $σ^2$. 

    If $A$ is an orthogonal $n × n$ matrix and $\bfZ = A \bfY$, 
    then $Z_1, \ldots, Z_n$ also are independent, 
    and each has a normal distribution with variance $σ^2$.

    \bomb{} The theorem says nothing about the means.
\end{frame}

\begin{frame}
    \frametitle{Theorem 11.3.2}
    
    In the linear regression model,
    $(\hbeta_{0}, \hbeta_{1})$ has a bivariate normal distribution.

    Also, if $n \ge 3$, then $\hat{\sigma}^{2}$ is independent of
    $(\hbeta_{0}, \hbeta_{1})$ and
    \begin{equation*}
        \frac{n \hat{\sigma}^{2}}{\sigma^{2}}
        \sim
        \chi^{2}(n-2)
        .
    \end{equation*}
\end{frame}

\subsection{Tests of Hypotheses about the Regression Coefficients}

\begin{frame}
    \frametitle{Tests of Hypotheses}
    We will let
    \begin{equation*}
        \sigma' = 
        \left(
        \frac{S^{2}}{n-2}
        \right)^{1/2}
        .
    \end{equation*}

    Consider
    \begin{equation*}
        \begin{aligned}
            H_{0} & : c_{0} \beta_{0} + c_{1} \beta_{1} = c_{\ast} \\
            H_{1} & : c_{0} \beta_{0} + c_{1} \beta_{1} \ne c_{\ast}
        \end{aligned}
        \tag*{(11.3.13)}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Theorem 11.3.3}
    
    Let $T_{n-2}^{-1}$ be the quantile function of $t(n-2)$.

    The test which rejects $H_{0}$ if 
    $\abs{U_{01}} \ge T_{n-2}^{-1}(1-\alpha_{0}/2)$, where
    \begin{equation*}
        U_{01}
        =
        \left(
            \frac{c_{0}^{2}}{n}
            +
            \frac{(c_{0}\overline{x}-c_{1})^{2}}{s_{x}^{2}}
        \right)^{-1/2}
        \left(
            \frac{c_{0} \hbeta_{0} + c_{1} \hbeta_{1} - c_{\ast}}{\sigma'}
        \right)
        ,
    \end{equation*}
    has significance level $\alpha_{0}$.
\end{frame}

\begin{frame}
    \frametitle{Theorem 11.3.4}
    
    Consider
    \begin{equation*}
        \begin{aligned}
            H_{0} & : c_{0} \beta_{0} + c_{1} \beta_{1} \le c_{\ast} \\
            H_{1} & : c_{0} \beta_{0} + c_{1} \beta_{1} > c_{\ast}
        \end{aligned}
        \tag*{(11.3.16)}
    \end{equation*}

    A level $\alpha_{0}$ test of (11.3.16) is to reject $H_{0}$ if
    $U_{01} \ge T_{n-2}^{-1}(1-\alpha_{0})$.
\end{frame}

\begin{frame}
    \frametitle{Hypotheses about \texorpdfstring{$\beta_{0}$}{beta 0}}
    
    Consider
    \begin{equation*}
        \begin{aligned}
            H_{0} & : \beta_{0} = \beta_{0}^{\ast} \\
            H_{1} & : \beta_{0} \ne \beta_{0}^{\ast}.
        \end{aligned}
        \tag*{(11.3.18)}
    \end{equation*}

    This is the same as $c_{0} = 1, c_{1} = 0$ and $c_{\ast} = \beta_{0}^{\ast}$ in
    \begin{equation*}
        \begin{aligned}
            H_{0} & : c_{0} \beta_{0} + c_{1} \beta_{1} = c_{\ast} \\
            H_{1} & : c_{0} \beta_{0} + c_{1} \beta_{1} \ne c_{\ast}
        \end{aligned}
        \tag*{(11.3.13)}
    \end{equation*}

    The same test applies.
\end{frame}

\begin{frame}
    \frametitle{Hypotheses about \texorpdfstring{$\beta_{1}$}{beta 1}}
    
    Consider
    \begin{equation*}
        \begin{aligned}
            H_{0} & : \beta_{1} = \beta_{1}^{\ast} \\
            H_{1} & : \beta_{1} \ne \beta_{1}^{\ast}.
        \end{aligned}
        \tag*{(11.3.18)}
    \end{equation*}

    This is the same as $c_{0} = 0, c_{1} = 1$ and $c_{\ast} = \beta_{1}^{\ast}$ in
    \begin{equation*}
        \begin{aligned}
            H_{0} & : c_{0} \beta_{0} + c_{1} \beta_{1} = c_{\ast} \\
            H_{1} & : c_{0} \beta_{0} + c_{1} \beta_{1} \ne c_{\ast}
        \end{aligned}
        \tag*{(11.3.13)}
    \end{equation*}

    The same test applies.
\end{frame}

\subsection{Confidence Intervals}

\begin{frame}
    \frametitle{Theorem 11.3.5}
    
    The open interval between
    \begin{equation*}
        c_{0} \hbeta_{0}
        +
        c_{1} \hbeta_{1}
        \pm
        \sigma'
        \left(
            \frac{c_{0}^{2}}{n}
            +
            \frac{(c_{0}\overline{x}-c_{1})^{2}}{s_{x}^{2}}
        \right)^{1/2}
        T_{n-2}^{-1}
        \left(
            1-\frac{\alpha_{0}}{2}
        \right)
    \end{equation*}
    is a coefficient $1-\alpha_{0}$ confidence interval for 
    $c_{0} \beta_{0} + c_{1} \beta_{1}$.

    \begin{block}{Proof}
        Use that
        \begin{equation*}
            U_{01}
            =
            \left(
                \frac{c_{0}^{2}}{n}
                +
                \frac{(c_{0}\overline{x}-c_{1})^{2}}{s_{x}^{2}}
            \right)^{1/2}
            \left(
                \frac{c_{0} \hbeta_{0} + c_{1} \hbeta_{1} - c_{\ast}}{\sigma'}
            \right)
            \sim t(n-2)
            ,
        \end{equation*}
    where $c_{\ast} = c_{0} \beta_{0} + c_{1} \beta_{1}$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Theorem 11.3.6}
    
    Let $Y$ be a new observation with predictor $x$ such that
    $Y$ is independent of $Y_{1}, \dots, Y_{n}$.

    Let $\hat{Y} = \hbeta_{0} + \hbeta_{1} x$.

    The with probability $1-\alpha_{0}$, $Y$ is between
    \begin{equation*}
        \hat{Y}
        \pm
        \sigma'
        \left(
            \frac{c_{0}^{2}}{n}
            +
            \frac{(c_{0}\overline{x}-c_{1})^{2}}{s_{x}^{2}}
        \right)^{1/2}
        T_{n-2}^{-1}
        \left(
            1-\frac{\alpha_{0}}{2}
        \right)
    \end{equation*}

    This is called a coefficient $1-\alpha_{0}$ \alert{prediction interval} of $Y$.
\end{frame}

\subsection{Residue Analysis}

\begin{frame}
    \frametitle{Example 11.3.6}

    The quantities $e_{i} = y_{i} - \hat{y}_{i}$ are called residues.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.75\textwidth]{fig-11.9.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 11.3.7}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{fig-11.11.png}
    \end{figure}
\end{frame}

\appendix


\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-25.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 11.3, Exercise 1, 3, 5, 7, 9, 11, 13,
                    15, 17.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
