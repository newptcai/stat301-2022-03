\input{../meta.tex}

\title{Lecture 19}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\begin{frame}
    \frametitle{Quick review of 9.5}
    
    Let $X_1,\ldots, X_n$ be \ac{iid} $\scN(\mu,\sigma)$ and
    \begin{equation*}
        \overline{X}_n
        =
        \frac{\sum_{i=1}^n X_{i}}{n}
        ,
        \qquad
        \sigma'
        =
        \left(
        \frac{\sum_{i=1}^n (X_{i}-\overline{X}_n)^{2}}{n-1}
        \right)^{1/2}
        .
    \end{equation*}
    Let
    \begin{equation*}
        U = \sqrt{n} \frac{\xmean{} - \mu_{0}}{\sigma'}.
    \end{equation*}
    Then $U \sim t(n-1)$ when $\mu = \mu_{0}$.

    The $t$ test \reject{} if $U \ge c$ and \accept{} if $U < c$, 
    where $c$ satisfies
    \begin{equation*}
        \p{U \ge c| \mu = \mu_{0}} = \alpha_{0},
    \end{equation*}
    i.e., $c = T_{n-1}^{-1}(1-\alpha_{0})$.

    \cake{} What is the distribution when $\mu \ne \mu_{0}$?
\end{frame}

\section{9.6 Comparing the Means of Two Normal Distributions}

\subsection{The Two-Sample $t$ Test}

\begin{frame}[c]
    \frametitle{Example 9.6.1 \emoji{cloud-with-rain}}
    Let 
    $X_1,\ldots, X_m$ be
    and 
    $Y_1,\ldots, Y_n$ be
    be the log-rainfall of $m$ seeded and $n$ natural clouds.

    \think{} How can we know if seeding the cloud worth the \emoji{coin}?
    \pause{}

    We can assume that
    \begin{itemize}
        \item $X_1,\ldots, X_m$ are \acs{iid} $\scN(\mu_{1}, \sigma^{2})$ 
        \item and $Y_1,\ldots, Y_n$ are \acs{iid} $\scN(\mu_{2}, \sigma^{2})$,
    \end{itemize}
    and consider
    \begin{equation*}
        H_{0}: \mu_{1} \le \mu_{2}
        ,
        \qquad
        H_{1}: \mu_{1} > \mu_{2}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Sum of two independent normal random variables}
    
    \begin{block}{Theorem 5.6.7}
        If the \acp{rv} $X1,\ldots, X_k$ are independent and if 
        $X_{i} \sim \scN(\mu_{i}, \sigma_{i}^{2})$,
        then
        \begin{equation*}
            X_{1} + \dots + X_{k}
            \sim
            \scN(
            \mu_{1} + \dots + \mu_{k}
            ,
            \sigma_{1}^{2} + \dots + \sigma_{k}^{2}
            )
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Theorem 9.6.1}
    Let
    \begin{equation*}
        \overline{X}_m = \frac{1}{m} \sum_{i=1}^{m} X_{i}
        ,
        \qquad
        \overline{Y}_{n} = \frac{1}{n} \sum_{i=1}^{n} Y_{i}
        ,
    \end{equation*}
    \begin{equation}
        S_{X}^{2}
        =
        \sum_{i=1}^{m} (X_{i} - \overline{X}_{m})^{2}
        ,
        \qquad
        S_{Y}^{2}
        =
        \sum_{i=1}^{n} (Y_{i} - \overline{Y}_{n})^{2}
        .
    \end{equation}
    \pause{}

    Define
    \begin{equation*}
        U = 
        \frac{(m+n-2)^{1/2}(\overline{X}_m - \overline{Y}_n)}
        {\displaystyle{}
        \left(\frac{1}{m}+\frac{1}{n}\right)^{1/2} (S_{X}^{2} + S_{Y}^{2})^{1/2}}
    \end{equation*}
    When $\mu_{1} = \mu_{2}$, we have $U \sim t(m+n-2)$.

\end{frame}

\begin{frame}
    \frametitle{Theorem 9.6.2}
    
    Let $\delta$ be the test which \reject{} if $U \ge T_{m+n-2}^{-1}(1-\alpha_{0})$.
    Then the following properties holds ---
    \begin{enumerate}
        \item $π(\mu_{1}, \mu_{2}, σ^2|δ) = α_0$ when $\mu_{1} = \mu_2$,
        \item $π(\mu_{1}, \mu_{2}, σ^2|δ) < α_0$ when $\mu_{1} < \mu_2$,
        \item $π(\mu_{1}, \mu_{2}, σ^2|δ) > α_0$ when $\mu_{1} > \mu_2$,
        \item $π(\mu_{1}, \mu_{2}, σ^2|δ) \to 0$ as $\mu_1 - \mu_{2} \to −∞$,
        \item $π(\mu_{1}, \mu_{2}, σ^2|δ) \to 1$ as $\mu_{1} - \mu_{2} \to ∞$.
    \end{enumerate}
    Moreover $\alpha(\delta) \le \alpha_{0}$.
\end{frame}

\begin{frame}
    \frametitle{Theorem 9.6.3}
    
    If we observe that $U = u$, then the $p$-value of the two-sample $t$ test
    is $1 - T_{m+n-2}(u)$.
\end{frame}

\begin{frame}
    \frametitle{Example 9.6.2 \emoji{cloud-with-rain}}
    
    In the \emoji{cloud-with-rain} example, we have observed
    \begin{equation*}
        \overline{X}_m = 5.13
        ,
        \qquad
        \overline{Y}_{n} = 3.99
        ,
    \end{equation*}
    \begin{equation}
        S_{X}^{2}
        =
        \sum_{i=1}^{m} 63.96
        ,
        \qquad
        S_{Y}^{2}
        =
        \sum_{i=1}^{n} 67.39
        ,
    \end{equation}
    with $m = n = 26$.

    \think{} What is the test result if $\alpha = 0.01$. (We really don't want to
    waste \emoji{coin}.)
    What is the $p$-value?

    \cake{} How would you explain this $p$-value to your \grandma{}?
\end{frame}

\subsection{Power of the Test}

\begin{frame}
    \frametitle{Theorem 9.6.4}

    The \ac{rv} $U$ has the noncentral $t$ distribution
    with $m+n-2$ degrees of freedom and noncentrality parameter
    \begin{equation*}
        \psi
        =
        \frac{\mu_{1} - \mu_{2}}{
            \displaystyle
            \sigma
            \left(\frac{1}{m}+\frac{1}{n}\right)^{1/2}
        }
    \end{equation*}
\end{frame}

\subsection{Two-Sided Alternatives}

\begin{frame}
    \frametitle{Two-Sided Alternatives}
    
    Given that
    \begin{equation*}
        H_{0}: \mu_{1} = \mu_{2}
        ,
        \qquad
        H_{1}: \mu_{1} \ne \mu_{2}
        .
    \end{equation*}
    We can adapt the two-sample $t$ to \reject{} $H_{0}$ if 
    $\abs{U} \ge c$ where 
    \begin{equation*}
        c = \txtq{}
    \end{equation*}
    such that test has level $\alpha_{0}$.
\end{frame}

\section{A review}

\begin{frame}
    \frametitle{Arachnophobes}
    
    Let $X_1,\ldots, X_{12}$ be the anxiety of 12 arachnophobes 
    when they showed pictures of
    $\temoji{spider}$, on the scale of $0-100$.

    On average, normal people's anxiety is $5$ when they see a $\temoji{spider}$.

    How should we know that arachnophobes are indeed more fearful of \emoji{spider}?
\end{frame}

\begin{frame}
    \frametitle{Arachnophobes}
    
    Let $X_1,\ldots, X_{12}$ be the anxiety of 12 arachnophobes 
    when they showed pictures of
    $\temoji{spider}$.

    Let $Y_1,\ldots, Y_{12}$ be the anxiety of 
    \emph{the same} 12 arachnophobes when they showed real
    $\temoji{spider}$.

    How should we know what arachnophobes are more fearful of, pictures 
    or real \emoji{spider}?
\end{frame}

\begin{frame}
    \frametitle{Arachnophobes}
    
    Let $X_1,\ldots, X_{12}$ be the anxiety of 12 arachnophobes 
    when they showed pictures of
    $\temoji{spider}$.

    Let $Y_1,\ldots, Y_{12}$ be the anxiety of 
    \emph{another} 12 arachnophobes when they showed real
    $\temoji{spider}$.

    How should we know what arachnophobes are more fearful of, pictures 
    or real \emoji{spider}?
\end{frame}

\begin{frame}
    \frametitle{$1$ or $2$?}
    
    Suppose $X_{1},\ldots, X_{n}$ are \ac{iid} $\Unif[0,\theta]$.

    Consider
    \begin{equation*}
        H_{0}: \theta = 1,
        \qquad
        H_{1}: \theta = 2.
    \end{equation*}

    \bonus{} Can you find a $\delta$ such that $\alpha(\delta) = 0$ and
    $\beta(\delta) = 1$.
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-19.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 9.6, 
                    Exercise 1, 3, 5, 7, 9.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
