\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 03}

\begin{document}

\maketitle

\section{7.3 Conjugate Prior Distributions}

\begin{frame}[c]
    \frametitle{Bernoulli and beta distributions}
    
    \begin{block}{Review -- Theorem 7.3.1}
        Let $X_1,\ldots, X_n$ be \ac{iid} $\Ber(\theta)$, where $\theta \sim
        \Be(\alpha, \beta)$. Then
        \begin{equation*}
            (\theta | \bfx)
            \sim
            \Be\left(\alpha + \sum_{i=1}^{n} x_{i}, \beta + n - \sum_{i=1}^{n} x_{i}\right).
        \end{equation*}
    \end{block}
\end{frame}

\subsection*{Improper Prior Distributions}

\begin{frame}
    \frametitle{Example 7.3.13 -- A clinical trial \emoji{syringe}}
    $40$ patients took part in a clinical trial. $22$ of them recovered
    \emoji{laughing}, $18$ did not \emoji{skull}.

    Assume that a patient recovers with probability $\theta$, where $\theta \sim \Be(\alpha,
    \beta)$. What is the posterior $\xi(\theta|\bfx)$?
\end{frame}

\begin{frame}[c]
    \frametitle{Improper prior distributions}
    
    An \emph{improper prior distributions} is a function $\xi:\Omega\mapsto\dsR$ 
    with
    \begin{equation*}
        \int_{\Omega} \xi(\theta) \dd \theta = \infty.
    \end{equation*}

    \bomb{} We cannot scale $\xi$ by a constant to make it a \ac{pdf}. But we may
    still use it as a prior to compute posterior.
\end{frame}

\section{7.4 Bayes Estimators}

\begin{frame}
    \frametitle{Example 7.4.1}
    
    $20$ types of junk food are tested to for the difference between measured 
    and advertised calories count. 

    Assume that these difference are \ac{iid} with parameter $\theta$.

    Can we report a single number as an estimate of $\theta$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{chips.jpg}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Estimator and estimate}
    
    Let $X_1,\ldots, X_n$ be \acp{rv} with parameter $\theta$.

    \emoji{straight-ruler} An \alert{estimator} is a function $\delta:\dsR^{n} \mapsto \dsR$.

    \emoji{nine} If $X_{1} = x_{1}, \ldots, X_{n} = x_{n}$, 
    the value $\delta(x_{1}, \ldots, x_{n})$ is called an \emph{estimate} of
    $\theta$.

    \think{} How do we measure if an estimate is good?
    
    \pause{}

    A \alert{loss function} is function $L: \Omega \times \dsR \mapsto \dsR$.

    $L(\theta, a)$ measures how bad the estimate $a$ is.

    In general, we want to minimize
    \begin{equation*}
        \E{L(\theta, a)|\bfx} = \int_{\Omega} L(\theta, a) \xi(\theta) \dd \theta.
    \end{equation*}
\end{frame}

\begin{frame}[c]
    \frametitle{Bayes Estimator}
    
    Let $L$ be a loss function. 

    For each possible value $\bfx$ of $X_1,\ldots, X_n$,
    let $\delta^{\ast}(\bfx)$ be the value $a$ which minimizes
    \begin{equation*}
        \E{L(\theta, a)|\bfx} = \int_{\Omega} L(\theta, a) \xi(\theta) \dd \theta.
    \end{equation*}

    Then $\delta^{\ast}$ is a called \alert{Bayes estimator} of $\theta$.
\end{frame}

\subsection{Different Loss Functions}

\begin{frame}[c]
    \frametitle{Different Loss Functions}
    
    The loss function
    \begin{equation*}
        L(\theta, a) = \abs{\theta-a}^2
    \end{equation*}
    is called the \alert{squared error loss function}.

    \begin{corollary}[Corollary 7.4.1]
        If $\E{\theta|\bfX} < \infty$, then $\delta^{\ast}(\bfX) = \E{\theta|\bfX}$.
    \end{corollary}

    \pause{}

    The loss function
    \begin{equation*}
        L(\theta, a) = \abs{\theta-a}
    \end{equation*}
    is called the \alert{absolute error loss function}.

    \begin{corollary}[Corollary 7.4.2]
        If $\E{\theta|\bfX} < \infty$, then $\delta^{\ast}(\bfX)$ is the median of
        $\xi(\theta|\bfX)$.
    \end{corollary}
\end{frame}

\begin{frame}
    \frametitle{Example 7.4.3}
    
    $40$ patients took part in a clinical trial and got \emoji{pill}.

    Among them, $22$ recovered \emoji{laughing}, $18$ did not \emoji{skull}.

    Let $X_{i}$ be the indicator \ac{rv} that patient $i$ recovered.

    Assume that $X_1,\ldots, X_n$ are \ac{iid} $\Ber(\theta)$
    and $\theta \sim \Be(1, 1)$.

    What is $\delta^{\ast}(\bfX)$ for the \emph{squared error loss function}?

    \cake{} What is another name for $\Be(1,1)$?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    $50$ patients took part in a clinical trial and got \emoji{syringe}.

    Among them, $28$ recovered \emoji{laughing}, $22$ did not \emoji{skull}.

    Let $X_{i}$ be the indicator \ac{rv} that patient $i$ recovered.

    Assume that $X_1,\ldots, X_n$ are \ac{iid} $\Ber(\theta)$
    and $\theta \sim \Be(2, 2)$.

    \question{} 
    What is $\delta^{\ast}(\bfX)$ for the \emph{squared error loss function}?

    \question{} 
    Is \emoji{syringe} better than \emoji{pill}?
\end{frame}

\section{The Bayes Estimate for Large Samples}

\begin{frame}
    \frametitle{Example 7.4.7 --- Scottish soldiers}

    \begin{columns}
        \begin{column}{0.6\textwidth}
            Suppose that the chest measurements of $n=5732$ Scottish soldiers are
            \ac{iid} $\scN(\theta, 4)$ and $\theta \sim \scN(\mu_{0}, \nu_{0}^{2})$.

            If $\bar{x}_{n} = 39.85$, then
            \begin{equation*}
                \delta^{\ast}(\theta) 
                = 
                \E{\theta|\bfx}
                =
                \txtq{}
            \end{equation*}
            with the \emph{squared error loss function}.
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{Scottish.jpg}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Consistent estimator}
    
    A sequence of estimators that converges in probability to the unknown value been
    estimated is called a \alert{consistent sequences of estimators}.

    In general, the Bayes estimators form a consistent sequence of estimators.

    \cake{} Why is this true in the previous example?
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-03.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 7.3, Exercise 9, 21, 22.
                \item[\emoji{pencil}] Section 7.4, Exercise 1, 2, 3, 5, 7, 9, 11.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
