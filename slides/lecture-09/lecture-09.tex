\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 09}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{8.5 Confidence Intervals}

\subsection{Confidence Intervals for the Mean of a Normal Distribution}

\begin{frame}
    \frametitle{How confident can we be? \emoji{triumph}}
    
    Let $X_1,\ldots, X_n$ be \acs{iid} $\scN(\mu, \sigma^{2})$. 
    Let
    \begin{equation*}
        \sigma' = 
        \left(
        \frac{\sum_{i=1}^{n} (X_{i}-\overline{X}_n)^{2}}{n-1}
        \right)^{\frac{1}{2}}
        ,
        \qquad
        U
        =
        \frac{n^{\frac{1}{2}}}{\sigma'}(\overline{X}_n-\mu)
        \sim t(n-1).
    \end{equation*}
    Given $\gamma \in (0,1)$, how to find $c$ such that
    \begin{equation*}
        \p{-c < U < c} = \gamma
    \end{equation*}
    and what does it tell us?
\end{frame}

\begin{frame}
    \frametitle{Example 8.5.2}
    
    Let $X_1,\ldots, X_n$ be \acs{iid} $\scN(\mu, \sigma^{2})$ with $n = 26$.

    Given $\gamma = 0.95$, how to find $c$ such that
    \begin{equation*}
        \p{-c < U < c} = \gamma
    \end{equation*}
    and what does it tell us?
\end{frame}

\begin{frame}
    \frametitle{Definition 8.5.1}
    
    Let $X_1,\ldots, X_n$ be \acs{iid} with a distribution that depends on $\theta$.
    If two statistics $A$ and $B$ satisfies
    \begin{equation*}
        \p{A < g(\theta) < B} \ge \gamma,
    \end{equation*}
    then $(A, B)$ is called a coefficient $\gamma$ \alert{confidence interval} of
    $g(\theta)$.
\end{frame}

\begin{frame}
    \frametitle{Theorem 8.5.1}
    
    Let $X_1,\ldots, X_n$ be \acs{iid} $\scN(\mu, \sigma^{2})$.
    The interval $(A, B)$ with
    \begin{equation*}
        \begin{aligned}
            A 
            &
            = 
            \overline{X}_n 
            - T^{-1}_{n-1}\left(\frac{1+\gamma}{2}\right)
            \frac{\sigma'}{n^{1/2}}
            ,
            \\
            B 
            &
            = 
            \overline{X}_n 
            + T^{-1}_{n-1}\left(\frac{1+\gamma}{2}\right)
            \frac{\sigma'}{n^{1/2}}
            ,
        \end{aligned}
    \end{equation*}
    is called an coefficient $\gamma$ \alert{confidence interval} of
    \begin{equation*}
        g(\mu, \sigma^{2}) = \mu.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 8.5.3}

    \think{} What does it mean to say that $(A, B)$ is the $90\%$ confidence interval of
    $\mu$ when $\overline{X}_n = 0.288743$ and $\sigma = 0.763311$?

    \pause{}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{Ex.8.5.2.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\emoji{clown-face} How sure are you?}
    \begin{table}[]
        %\begin{tabular}{|l|l|}
        \begin{tabular}{|p{0.5\textwidth}|p{0.4\textwidth}|}
            \hline
            True or false? 
            & How sure are you? \\ \hline \hline
            1. The elephant is the world’s largest mammal. 
            & 55\% 65\% 75\% 85\% 95\% \\ \hline
            2. Sea otters sometimes hold hands while they sleep.   
            & 55\% 65\% 75\% 85\% 95\% \\ \hline
            3. Centipedes have more legs than any other animal.  
            & 55\% 65\% 75\% 85\% 95\% \\ \hline
        \end{tabular}
    \end{table}   

    Source: \emph{The Scout Mindset} by Julia Galef.

    Similar exercise at
    \href{https://programs.clearerthinking.org/calibrate_your_judgment.html}{\texttt{https://clearerthinking.org}}.
\end{frame}

\subsection{One-Sided Confidence Intervals}

\begin{frame}
    \frametitle{Definition 8.5.2}
    
    Let $X_1,\ldots, X_n$ be \acs{iid} with a distribution that depends on $\theta$.

    If $A$ satisfies
    \begin{equation*}
        \p{A < g(\theta)} \ge \gamma,
    \end{equation*}
    then $(A, \infty)$ is called a \alert{one-sided} 
    coefficient $\gamma$ \alert{confidence interval} of $g(\theta)$.
    $A$ is called coefficient $\gamma$ \alert{lower confidence limit}.

    \pause{}

    If $B$ satisfies
    \begin{equation*}
        \p{g(\theta) < B} \ge \gamma,
    \end{equation*}
    then $(-\infty, B)$ is called a \alert{one-sided} 
    coefficient $\gamma$ \alert{confidence interval} of $g(\theta)$.
    $B$ is called coefficient $\gamma$ \alert{upper confidence limit}.
\end{frame}

\begin{frame}
    \frametitle{Theorem 8.5.2}
    
    Let $X_1,\ldots, X_n$ be \acs{iid} $\scN(\mu, \sigma^{2})$.
    The coefficient $\gamma$ lower and upper confidence limits for $\mu$ are
    \begin{equation*}
        \begin{aligned}
            A 
            &
            = 
            \overline{X}_n 
            - T^{-1}_{n-1}\left(\gamma\right)
            \frac{\sigma'}{n^{1/2}}
            ,
            \\
            B 
            &
            = 
            \overline{X}_n 
            + T^{-1}_{n-1}\left(\gamma\right)
            \frac{\sigma'}{n^{1/2}}
            ,
        \end{aligned}
    \end{equation*}

    \cake{} Which one is larger? $T_{n-1}^{-1}(\gamma)$ or
    $T_{n-1}^{-1}\left(\frac{\gamma+1}{2}\right)$?
\end{frame}

\begin{frame}
    \frametitle{Example 8.5.6}
    
    Let $X_1,\ldots, X_n$ be \acs{iid} $\scN(\mu, \sigma^{2})$ with $n = 26$.

    How to find a $90\%$ lower and upper confidence limit of $\mu$ 
    when $\overline{X}_n = 0.288743$ and $\sigma = 0.763311$?

    \begin{figure}[htpb]
        \centering
        \includegraphics<2>[width=0.8\textwidth]{Ex.8.5.6-a.png}%
        \includegraphics<3>[width=0.8\textwidth]{Ex.8.5.6-b.png}
    \end{figure}
\end{frame}

\subsection{Confidence Intervals for Other Parameters}

\begin{frame}
    \frametitle{Example 8.5.7}
    
    Let $X_1, X_2, X_2$ be \acs{iid} $\Exp(\theta)$. Let $T = X_1 + X_2 + X_3$.
    Then $\theta T \sim \Gam(3,1)$.

    What is s an exact coefficient $\gamma$ upper confidence limit for $\theta$?
\end{frame}

\begin{frame}
    \frametitle{Definition 8.5.3}
    
    Let $\bfX = X_1,\ldots, X_n$ be \acs{iid} with a distribution that depends on $\theta$.
    If $V(\bfX, \theta)$ has the same distribution for all $\theta$,
    then it is called \alert{pivotal quantity}.

    \pause{}

    To use the pivotal for finding confident interval, 
    one needs a function $r(v, \bfx)$ such that
    \begin{equation*}
        r (V(\bfX, θ ), \bfX) = g(θ).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Theorem 8.5.3}
    
    Let $\bfX = X_1,\ldots, X_n$ be \acs{iid} with a distribution that depends on $\theta$.

    Let $V$ be a pivot.

    Let $G$ be the \acs{cdf} of $V$ and assume that $G$ is \alert{continuous}.

    Assume that $r(v, \bfx)$ is \alert{strictly increasing} in $v$.

    Let $\gamma=\gamma_{2}-\gamma_{1}$.

    \pause{}
    Then the following are endpoints of an exact 
    $\gamma$ coefficient confidence
    interval for $g(\theta)$ ---
    \begin{equation*}
        \begin{aligned}
            A 
            &
            = 
            r(G^{-1}(\gamma_{1}), \bfX)
            ,
            \\
            B 
            &
            = 
            r(G^{-1}(\gamma_{2}), \bfX)
            .
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 8.5.8}
    
    Let $X_1,\ldots, X_n$ be \acs{iid} $\scN(\mu, \sigma^{2})$. 
    Consider
    \begin{equation*}
        V(\bfX, \theta)
        =
        \frac{\sum_{i=1}^{n} (X_i - \overline{X}_n)^{2}}{\sigma^{2}}.
    \end{equation*}

    What is the confidence interval for $\sigma^{2}$?
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-09.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 8.5, Exercise 1, 5, 7, 9, 11.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
