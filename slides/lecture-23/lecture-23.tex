\input{../meta.tex}

\title{Lecture 23}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{10.5 Simpson's Paradox}

\subsection{An Example of the Paradox}

\begin{frame}
    \frametitle{Example 10.5.1-2 \emoji{open-mouth}}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{table-10.29.png}
    \end{figure}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{table-10.30.png}
    \end{figure}
\end{frame}

\subsection{The Paradox Explained}

\begin{frame}
    \frametitle{The Paradox Explained}
    
    The ``paradox'' is caused by several reasons ---
    \begin{itemize}
        \item \emoji{woman} are more likely to improve than \emoji{man}
        \item Most \emoji{woman} receives the standard treatment.
        \item Most \emoji{man} receives the new treatment.
    \end{itemize}

    \pause{}

    \cake{} How to prevent such a problem?
\end{frame}

\begin{frame}
    \frametitle{In Mathematical Equations}
    
    Let $A$ be the event of a experiment subject is \emoji{man}.

    Let $B$ be the event that a subject receives the new treatment.

    It is possible that
    \begin{equation*}
        \begin{aligned}
            \p{I|A \cap B}
            &
            >
            \p{I|A \cap B^{c}}
            \\
            \p{I|A^{c} \cap B}
            &
            >
            \p{I|A^{c} \cap B^{c}}
            \\
            \p{I|B}
            &
            <
            \p{I|B^{c}}
        \end{aligned}
    \end{equation*}

    But this is not possible if $\p{A|B} = \p{A|B^{c}}$.
\end{frame}

\section{11.1 The Method of Least Squares}

\subsection{Fitting a Straight Line}

\begin{frame}[c]
    \frametitle{Example 11.1.1}

    Suppose that $10$ patients are treated by two drugs $A$ and $B$
    to reduce their blood pressure.

    Let $x_{1},\ldots, x_{10}$ and $y_{1},\ldots, y_{n}$ be the amount reduction of
    blood pressure by each drug respectively.

    \think{} How can we predict a patient's reaction to drug $B$ given his/her/their
    reaction to $A$?
\end{frame}
    
\begin{frame}[c]
    \frametitle{Example 11.1.1}
    \begin{columns}[c]
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{table-11.1.png}
            \end{figure}
        \end{column}
        \begin{column}{0.6\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{fig-11.1.png}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{How About a Straight Line?}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{fig-11.2.png}
    \end{figure}
\end{frame}

\subsection{The Least-Squares Line}

\begin{frame}
    \frametitle{Example 11.1.2}

    \think{} How can we find a line so that it is as \emph{close} as possible 
    to all the points?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{fig-11.3.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 11.1.1}
    
    The line that minimizes the sum of the squares of the vertical deviations of all
    the points from the line has the following slope and intercept:
    \begin{equation*}
        \begin{aligned}
            \hat{\beta}_1
            &
            =
            \frac
            {\sum_{i=1}^{n}(y_i - \overline{y})(x_i - \overline{x})}
            {\sum_{i=1}^{n}(x_i - \overline{x})^2}
            ,
            \\
            \hat{\beta}_2
            &
            =
            \overline{y} - \hat{\beta}_{1} \overline{x}
            ,
        \end{aligned}
    \end{equation*}
    where 
    $\overline{x} = \frac{1}{n} \sum_{i=1}^{n} x_{i}$
    and
    $\overline{y} = \frac{1}{n} \sum_{i=1}^{n} y_{i}$
    .
\end{frame}

\begin{frame}
    \frametitle{Definition 11.1.1}

    The line $y = \hat{\beta}_{0} + \hat{\beta}_{1} x$ is called
    the \alert{least squares line}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{fig-11.4.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A Digression to Linear Algebra \dizzy{}}

    Let
    \begin{equation*}
        \bfX =
        \begin{bmatrix} 
            1 & x_{1} \\
            1 & x_{2} \\
            \vdots & \vdots \\
            1 & x_{n} \\
        \end{bmatrix} 
        ,
        \qquad
        \bfy =
        \begin{bmatrix} 
            y_{1} \\
            y_{2} \\
            \vdots \\
            y_{n} \\
        \end{bmatrix} 
    \end{equation*}
    Then
    \begin{equation*}
        \norm{
            \bfX 
            \begin{bmatrix}
                \hat{\beta}_{0} \\
                \hat{\beta}_{1}
            \end{bmatrix}
            -
            \bfy
        }
        =
        \inf_{\beta_{0}, \beta_{1} \in \dsR}
        \norm{
            \bfX 
            \begin{bmatrix}
                \beta_{0} \\
                \beta_{1}
            \end{bmatrix}
            -
            \bfy
        }
    \end{equation*}
    In other words,
    $ 
    \begin{bmatrix}
        \hat{\beta}_{0} \\
        \hat{\beta}_{1}
    \end{bmatrix}
    $
    is the \alert{least squares solution} of
    \begin{equation*}
        \bfX 
        \begin{bmatrix}
            \beta_{0} \\
            \beta_{1}
        \end{bmatrix}
        =
        \bfy
        .
    \end{equation*}
\end{frame}

\subsection{Fitting a Polynomial by the Method of Least Squares}

\begin{frame}
    \frametitle{What if we want a curve?}
    
    Suppose instead of straight line, we want to fit a polynomial.
    \begin{equation*}
        y = β_0 + β_1 x + β_2 x^2 + \dots + β_k x^k.
    \end{equation*}
    In other words, we want to minimize
    \begin{equation*}
        Q = \sum_{i=1}^{n}
        (y_{i} - (β_0 + β_1 x + β_2 x^2 + \dots + β_k x^k))^{2}
    \end{equation*}
    \cake{} What should we do?
\end{frame}

\begin{frame}
    \frametitle{Example 11.1.3}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{fig-11.5.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 11.1.4}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{fig-11.6.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Another Digression to Linear Algebra \dizzy{}}

    Let
    \begin{equation*}
        \bfX =
        \begin{bmatrix} 
            1 & x_{1} & x_{1}^{2} & \dots & x_{1}^k \\
            1 & x_{2} & x_{2}^{2} & \dots & x_{2}^k \\
            \vdots & \vdots & \vdots & \dots & \vdots \\
            1 & x_{n} & x_{n}^{2} & \dots & x_{n}^k \\
        \end{bmatrix} 
        ,
        \qquad
        \bfy =
        \begin{bmatrix} 
            y_{1} \\
            y_{2} \\
            \vdots \\
            y_{n} \\
        \end{bmatrix} 
    \end{equation*}
    Then
    $ 
            \begin{bmatrix}
                \hat{\beta}_{0} \\
                \hat{\beta}_{1} \\
                \vdots \\
                \hat{\beta}_{k} \\
            \end{bmatrix}
    $
    is the \alert{least squares solution} of
    \begin{equation*}
        \bfX 
            \begin{bmatrix}
                \beta_{0} \\
                \beta_{1} \\
                \vdots \\
                \beta_{k} \\
            \end{bmatrix}
        =
        \bfy
        .
    \end{equation*}
\end{frame}

\subsection{Fitting a Linear Function of Several Variables}

\begin{frame}
    \frametitle{What if we have more than one variables?}
    
    Suppose instead of straight line, we want to fit a polynomial.
    \begin{equation*}
        y = β_0 + β_1 x_1 + β_2 x_2 + \dots + β_k x_k.
    \end{equation*}
    In other words, we want to minimize
    \begin{equation*}
        Q = \sum_{i=1}^{n}
        (y_{i} - (β_0 + β_1 x_{i1} + β_2 x_{i2} + \dots + β_k x_{ik})^{2}
    \end{equation*}
    \cake{} What should we do?
\end{frame}


\begin{frame}
    \frametitle{Example 11.1.5}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{table-11.2.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Final Digression to Linear Algebra \dizzy{}}

    Let
    \begin{equation*}
        \bfX =
        \begin{bmatrix} 
            1 & x_{11} & x_{12} & \dots & x_{1k} \\
            1 & x_{21} & x_{22} & \dots & x_{2k}^k \\
            \vdots & \vdots & \vdots & \dots & \vdots \\
            1 & x_{n1} & x_{n2} & \dots & x_{nk} \\
        \end{bmatrix} 
        ,
        \qquad
        \bfy =
        \begin{bmatrix} 
            y_{1} \\
            y_{2} \\
            \vdots \\
            y_{n} \\
        \end{bmatrix} 
    \end{equation*}
    Then
    $ 
            \begin{bmatrix}
                \hat{\beta}_{0} \\
                \hat{\beta}_{1} \\
                \vdots \\
                \hat{\beta}_{k} \\
            \end{bmatrix}
    $
    is the \alert{least squares solution} of
    \begin{equation*}
        \bfX 
            \begin{bmatrix}
                \beta_{0} \\
                \beta_{1} \\
                \vdots \\
                \beta_{k} \\
            \end{bmatrix}
        =
        \bfy
        .
    \end{equation*}
\end{frame}
\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-23.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 10.5, Exercise 1, 3, 5, 7.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
