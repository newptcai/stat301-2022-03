\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 02}

\begin{document}

\maketitle

\section{7.2 Prior and Posterior Distributions}

\subsection{The Posterior Distribution}

\begin{frame}[c]
    \frametitle{Posterior distributions}
    
    Suppose that a statistical model has a \ac{rv} parameter $\theta$ and
    \ac{rv}s $X_{1}, \dots, X_{m}$ observed.

    The conditional distribution of $\theta$ given $X_{1}, \dots, X_{m}$
    is called its \alert{posterior distribution}. 

    We denote the \ac{pdf} of $\theta$ given $X_{1} = x_1, \dots, X_n = x_n$ by
    \begin{equation*}
        \xi(\theta | \bfx)
        =
        \xi(\theta | x_1,\dots, x_n).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Computing the posterior distribution}
    
    \begin{block}{Theorem 7.2.1}
        Suppose that $X_{1}, \ldots, X_{n}$ are \acs{iid} \acp{rv} with \acs{pdf}
        $f(x|\theta)$. 

        Suppose also that $\theta$ is a \ac{rv} with prior \ac{pdf} $\xi(\theta)$.

        \pause{}

        Then the posterior \ac{pdf} of $\theta$ is
        \begin{equation*}
            \xi(\theta | \bfx)
            =
            \frac{f_n(\bfx | \theta)\xi(\theta)}{g_n(\bfx)}
            =
            \frac{f(x_1|\theta)\dots f(x_n|\theta) \xi(\theta)}{g_n(\bfx)}
        \end{equation*}
        where
        \begin{equation*}
            g_n(\bfx) 
            = 
            \int_{\Omega} f_n(\bfx | \theta) \xi(\theta)  \dd{\theta}
            .
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 7.2.6}
    
    Assume $X_{1}, \dots, X_{n}$ are \ac{iid} $\Exp(\theta)$, 
    and $\theta = \Gam(\alpha, \beta)$,
    compute $\xi(\theta | \bfx)$ using Theorem 7.2.1, where
    \begin{equation*}
        \bfx =  (2911, 3403, 3237, 3509, 3118).
    \end{equation*}

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{7.1.png}
        \caption*{The case $\alpha = 4, \beta = 20,000$ and $\alpha = 1, \beta = 1,000$.}
    \end{figure}
\end{frame}


\subsection{The Likelihood Function}

\begin{frame}
    \frametitle{Summary}
    
    \tableofcontents{}
\end{frame}

\begin{frame}
    \frametitle{Theorem 7.2.1 revisited}

    We have
    \begin{equation*}
        \xi(\theta | \bfx)
        =
        \frac{f_n(\bfx | \theta)\xi(\theta)}{g_n(\bfx)}
        \propto
        f_n(\bfx | \theta)\xi(\theta)
        ,
    \end{equation*}
    since $g_{n}(\bfx)$ is a constant.
    The function $f_n(\bfx | \theta)$ is called the \alert{likelihood function}.

    \pause{}

    To make the \ac{lhs} a \ac{pdf}, we must have
    \begin{equation*}
        g_n(\bfx) 
        = 
        \int_{\Omega} f_n(\bfx | \theta) \xi(\theta)  \dd{\theta}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 7.2.7}
    
    Let $X_1,\ldots, X_n$ be \ac{iid} $\Ber(\theta)$ where $\theta \sim \Unif[0,1]$.
    Then
    \begin{equation*}
        \xi(\theta|\bfx)
        \propto
        f_{n}(\bfx|\theta) \xi(\theta) 
        = 
        \begin{cases}
            \theta^{y} (1-\theta)^{n-y}
            & 
            \text{for } 0 < \theta < 1, \\
            0
            & \text{otherwise}.
        \end{cases}
    \end{equation*}
    where
    $y = x_1 + \dots + x_n$.

    \pause{}

    The $\Be(\alpha, \beta)$ distribution has \ac{pdf}
    \begin{equation*}
        f_{\alpha, \beta}
        (x)
        =
        \begin{cases}
            \displaystyle \frac{x^{\alpha -1} (1-x)^{\beta -1}}{\Beta(\alpha ,\beta )} 
            & \text{for } 0 < x < 1, \\
            0 & \text{otherwise}.
        \end{cases}
    \end{equation*}

    Thus, we have $\xi(\theta|\bfx) = f_{\txtq{}, \txtq{}} (\theta)$.
\end{frame}

\subsection{Sequential Observations and Prediction}

\begin{frame}
    \frametitle{Sequential observations and prediction}
    
    Often $X_1,\ldots, X_n$ can only be observed one-by-one.
    After observing $X_1 = x_1$,
    \begin{equation*}
        \xi(\theta|x_{1}) = f(x_{1}|\theta) \xi(\theta).
    \end{equation*}
    \pause{}
    After observing $X_2 = x_2$,
    \begin{equation*}
        \xi(\theta|x_{1}, x_{2}) 
        =
        f(x_{2}|\theta) \xi(\theta|x_1).
    \end{equation*}
    \pause{}
    After $n$ observations,
    \begin{equation*}
        \xi(\theta|\bfx) 
        =
        f(x_{n}|\theta) \xi(\theta|x_1,\ldots,x_{n-1}),
    \end{equation*}
    \pause{}
    and
    \begin{equation*}
        f(x_{n}|x_{1},\ldots, x_{n-1})
        =
        \int_{\Omega} 
        f(x_{n}|\theta) \xi(\theta|x_1,\ldots,x_{n-1})
        \dd \theta
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 7.2.8}
    
    Continuing the Example 7.2.6, we can compute
    \begin{equation*}
        f(x_{6}|x_{1}, \ldots, x_{5})
        =
        \int_{\Omega} 
        f(x_{6}|\theta) \xi(\theta|x_1,\ldots,x_{5})
        \dd \theta
        ,
    \end{equation*}
    and $\p{x_{6} > 3000 \cond \bfx}$.

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{7.2.png}
        \caption*{The original prior is $\Gam(4, 20000)$ and the alternative is $\Gam(1, 1000)$.}
    \end{figure}
\end{frame}

\section{7.3 Conjugate Prior Distributions}

\begin{frame}[c]
    \frametitle{Conjugate Family/Hyperparameters}
    
    Let $X_{1}, \ldots, X_{n}$ be conditionally \ac{iid} given $\theta$ with \ac{pdf}
    $f(x|\theta)$.

    Let $\Psi$ be a family of possible distributions of $\theta$.

    If the posterior $\xi(\theta|\bfx)$ is always in $\Psi$, then $\Psi$ is called a
    \alert{conjugate family} of prior distributions for samples from $f(x|\theta)$.

    The parameters for distributions in $\Psi$ are called (prior/posterior)
    \alert{hyperparameters}.
\end{frame}

\begin{frame}
    \frametitle{Review beta distribution}

    A random variable $X \sim \Be(\alpha, \beta)$ has a \ac{pdf}
    \begin{equation*}
        f(x)
        =
        \begin{cases}
            \displaystyle
            \frac{x^{\alpha -1} (1-x)^{\beta -1}}{B(\alpha ,\beta )}
                & 
                \text{for } 0 < x < 1, \\
                0
                & \text{otherwise}.
        \end{cases}
    \end{equation*}
    \cake{} What is the another name of $\Be(1,1)$?

    \pause{}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.55\textwidth]{Beta_distribution.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Bernoulli and beta distributions}
    
    \begin{block}{Theorem 7.3.1}
        Let $X_1,\ldots, X_n$ be \ac{iid} $\Ber(\theta)$, where $\theta \sim
        \Be(\alpha, \beta)$. Then
        \begin{equation*}
            (\theta | \bfx)
            \sim
            \Be\left(\alpha + \sum_{i=1}^{n} x_{i}, \beta + n - \sum_{i=1}^{n} x_{i}\right).
        \end{equation*}
    \end{block}

    \pause{}

    \begin{exampleblock}{Example 7.3.2}
        Let $X_{1}, \ldots, X_{n}$ be \ac{iid} $\Ber(\theta)$ where $\theta \sim
        \Unif[0,1]$.  Show that the variance of $(\theta|\bfx)$ is
        \begin{equation*}
            V = \frac{(y+1)(z+1)}{(y+z+2)^{2} (y+z+3)}
        \end{equation*}
        where $y = \sum_{i=1}^{n} x_{i}$ and $z = n- y$.
    \end{exampleblock}
\end{frame}

%\begin{frame}
%    \frametitle{Poisson and gamma distributions}
%    
%    \begin{block}{Theorem 7.3.2}
%        Let $X_1,\ldots, X_n$ be \ac{iid} $\Poi(\theta)$, where $\theta \sim
%        \Gam(\alpha, \beta)$. Then
%        \begin{equation*}
%            (\theta | \bfx)
%            \sim
%            \Gam\left(\alpha + \sum_{i=1}^{n} x_{i}, \beta + n\right).
%        \end{equation*}
%    \end{block}
%\end{frame}
%
%\begin{frame}
%    \frametitle{Normal distributions}
%    
%    \begin{block}{Theorem 7.3.3}
%        Let $X_1,\ldots, X_n$ be \ac{iid} $\Gaussian(\theta, \sigma^{2})$, 
%        where $\theta \sim \Gaussian(\mu_0, \nu_{0}^{2})$. 
%        Then $(\theta | \bfx) \sim \Gaussian(\mu_1, \nu_1^{2})$
%        where
%        \begin{equation*}
%            \mu_{1}
%            =
%            \frac{\sigma^{2} \mu_{0} + n \nu_{0}^{2} \overline{x}_{n}}{\sigma^{2} + n
%            \nu_{0}^{2}}
%            ,
%            \qquad
%            \nu_{1}^{2}
%            =
%            \frac{\sigma^{2} \nu_{0}^{2}}{\sigma^{2} + n \nu_{0}^{2}}
%            .
%        \end{equation*}
%    \end{block}
%\end{frame}

%\begin{frame}
%    \frametitle{Example 7.3.10}
%    
%    Let $X_1,\ldots, X_n$ be \ac{iid} $\Gaussian(\theta, 100)$, 
%    where $\theta \sim \Gaussian(0, 60)$. 
%
%    If $\overline{x}_n = 0.125$ and $n =20$,
%    then what is $\p{\theta > 1|\bfx}$?
%\end{frame}

\begin{frame}
    \frametitle{Exponential distributions}
    
    \begin{block}{Theorem 7.3.4}
        Let $X_1,\ldots, X_n$ be \ac{iid} $\Exp(\theta)$, 
        where $\theta \sim \Gam(\alpha, \beta)$. 
        Then $(\theta | \bfx) \sim \Gam(\alpha + n, \beta + \sum_{i=1}^{n}x_{i})$.
    \end{block}

    Proof is essentially Example 7.2.6.
\end{frame}

\begin{frame}
    \frametitle{\tps}
    
    Recall that in Example 7.3.2, the variance of posterior is
    \begin{equation*}
        V = \frac{(y+1)(z+1)}{(y+z+2)^{2} (y+z+3)}
    \end{equation*}
    where $y = \sum_{i=1}^{n} x_{i}$ and $z = n- y$.

    Show that if $n > 22$ then $V < 0.01$.

    \hint{} How to find minimum of a function in calculus?
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-02.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 7.3, Exercise 1, 3, 5, 7, 11, 17, 19.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
