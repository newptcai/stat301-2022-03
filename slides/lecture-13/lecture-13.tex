\input{../meta.tex}

\title{Lecture 13}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{9.1 Problems of Testing Hypotheses}

\subsection{Making a Test Have a Specific Significance Level}

\begin{frame}\label{ex:9:1:8}
    \frametitle{Example 9.1.8 --- Continuous \ac{rv}}
    
    Let $X_1,\ldots, X_{n}$ be \ac{iid} $\scN(\mu, \sigma^{2})$.
    Let
    \begin{equation*}
        H_{0}: \mu = \mu_{0}
        ,
        \qquad
        H_{1}: \mu \ne \mu_{0}
        .
    \end{equation*}
    Let $T = \abs{\overline{X}_n - \mu_{0}}$.
    We reject $H_{0}$ when $T \ge c$.

    \think{} Given $\alpha_{0}$, how to find $c$ such that
    \begin{equation*}
        \sup_{\mu \in \Omega_{0}}
        \pi(\mu|\delta)
        =
        \sup_{\mu \in \Omega_{0}}
        \p{T \ge c|\mu}
        =
        \alpha_{0}
        .
    \end{equation*}
    \cake{} 
    What is the \ac{lhs} of the above called?
    What is $\alpha_{0}$ called?
\end{frame}

\begin{frame}
    \frametitle{Example 9.1.9 --- Discrete \ac{rv}}
    
    Let $X_1,\ldots, X_{n}$ be \ac{iid} $\Ber(p)$.
    Let
    \begin{equation*}
        H_{0}: p \le p_{0}
        ,
        \qquad
        H_{1}: p > p_{0}
        .
    \end{equation*}

    Let $Y = \sum_{i=1}^{n} X_{i}$.
    We reject $H_{0}$ when $Y \ge c$.

    \think{} Given $\alpha_{0}$, how to find the \alert{smallest} $c$ such that
    \begin{equation*}
        \sup_{p \in \Omega_{0}}
        \pi(p|\delta)
        =
        \sup_{p \in \Omega_{0}}
        \p{Y \ge c|p}
        \le
        \alpha_{0}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Good power functions}

    A good power function should be small when $\theta \in \Omega_0$ and increases to
    $1$ when $\theta$ moves away.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{9.4.png}
    \end{figure}
\end{frame}

\subsection{Equivalence of Tests and Confidence Sets}

\begin{frame}
    \frametitle{Theorem 9.1.1}
    
    Let $X_1,\ldots, X_n$ be \ac{iid} \acp{rv} with parameter $\theta$.

    Let $g(\theta)$ be a function.

    Assume that for each $g_{0}$, 
    there is test $\delta_{g_{0}}$ with $\alpha(\delta_{g_{0}}) = \alpha_{0}$
    with 
    \begin{equation*}
        H_{0, g_{0}}: g(\theta) = g_{0}
        ,
        \qquad
        H_{1, g_{0}}: g(\theta) \ne g_{0}
        .
    \end{equation*}

    Let
    \begin{equation*}
        \omega(\bfx) = 
        \{g_{0} : \delta_{g_{0}} \text{ does not reject } H_{0, g_{0}} 
            \text{ if }
        \bfX = \bfx \}
    \end{equation*}

    The probability
    \begin{equation*}
        \p{g(\theta_{0}) \in \omega(\bfX)|\theta = \theta_{0}} \ge 1 - \alpha_{0}.
    \end{equation*}

    We call $\omega(\bfX)$ the coefficient $γ$ \alert{confidence set} for $g(θ)$.
\end{frame}

\subsection{The $p$-value}

\begin{frame}
    \frametitle{Example 9.1.10-9.1.11}

    Suppose that in \hyperlink{ex:9:1:8}{Example 9.1.8}, we observed
    \begin{equation*}
        Z 
        = 
        \frac{\overline{X}_n - \mu_{0}}{\sigma/\sqrt{n}}
        =
        2.83
        .
    \end{equation*}
    This implies that we \emph{reject} $H_{0}$ whenever
    \begin{equation*}
        2.83 
        \ge
        \Psi^{-1}
        (1- \alpha_{0}/2)
    \end{equation*}
    i.e., when
    \begin{equation*}
        \alpha_{0} \ge \txtq{}
    \end{equation*}
    
    \bonus{} Can you compute \question{}?
\end{frame}

\begin{frame}
    \frametitle{Definition 9.1.9}

    The \alert{$p$-value} is the smallest level $α_0$ such that we would
    reject the null-hypothesis at level $α_0$ with the observed data.

    \emoji{star-struck} If we report $p$-value, we do not need to select an arbitrary
    \emph{level of significance} $\alpha_{0}$ before the test.

    \cake{} If $p$-value is $0.0054$, 
    then $H_{0}$ will be rejected if $\alpha_{0} \txtq{} 0.0054$ 
    and accepted otherwise.
\end{frame}

\begin{frame}
    \frametitle{Calculating $p$-values}
    
    Let $\delta_{c}$ be the test which rejects $H_{0}$ when $T \ge c$. 
    Recall that
    \begin{equation*}
        \alpha(\delta_{c})
        =
        \sup_{\theta \in \Omega_{0}} \pi(\theta|\delta_{c})
        =
        \sup_{\theta \in \Omega_{0}} \p{T \ge c | \theta}
        .
    \end{equation*}
    \emoji{star-struck} Then the $p$-value when $T = t$ is just $\alpha(\delta_{t}).$

    \cake{} How does $\alpha(\delta_{c})$ changes when $c$ increases?
\end{frame}

\begin{frame}
    \frametitle{Example 9.1.12}
    
    Let $X_1,\ldots, X_{n}$ be \ac{iid} $\Ber(p)$.
    Let
    \begin{equation*}
        H_{0}: p \le p_{0}
        ,
        \qquad
        H_{1}: p > p_{0}
        .
    \end{equation*}

    Let $Y = \sum_{i=1}^{n} X_{i}$.
    We reject $H_{0}$ when $Y \ge c$.

    \bonus{} If $Y = 7$ is observed, and $n=12$, $p_{0} = 0.5$, what is the $p$-value?
\end{frame}

\subsection{Likelihood Ratio Tests}

\begin{frame}
    \frametitle{Definition 9.1.11}

    Let
    \begin{equation*}
        H_{0}: \theta \in \Omega_{0}
        ,
        \qquad
        H_{1}: \theta \in \Omega_{1}
        .
    \end{equation*}
    
    The statistics
    \begin{equation*}
        \Lambda(\bfx)  
        = 
        \frac
        {\sup_{\theta \in \Omega_{0}} f_{n}(\bfx|\theta)}
        {\sup_{\theta \in \Omega_{1}} f_{n}(\bfx|\theta)}
    \end{equation*}
    is called the \alert{likelihood ratio statistics}.

    A \alert{likelihood test} is to reject $H_{0}$ if $\Lambda(\bfx) \le k$ for some
    constant $k$.
\end{frame}

\begin{frame}
    \frametitle{Example 9.1.18}
    
    Let $X_1,\ldots, X_n$ be \ac{iid} $\Ber(\theta)$.
    Let
    \begin{equation*}
        H_{0}: \theta = \theta_{0}
        ,
        \qquad
        H_{1}: \theta \ne \theta_{0}
        .
    \end{equation*}

    \think{} 
    If we observe $Y = \sum_{i=1}^{n} X_{i} = y$,
    what is $\Lambda(y)$?

    \pause{}

    \think{}
    Which $k$ should we take when $n = 10$ and $\theta_{0} = 0.3$
    so that the significance level is at most $\alpha_{0} = 0.05$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{table.9.1.png}
    \end{figure}
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-13.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 9.1, 
                    Exercise 17, 19, 21.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
