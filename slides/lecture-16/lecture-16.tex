\input{../meta.tex}

\title{Lecture 16}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\begin{frame}[c]
    \frametitle{Quick Review of 9.2}
    \vspace{1em}
    \begin{columns}[c]
        \begin{column}{0.55\textwidth}
            Let
            \begin{equation*}
                H_{0}: \theta = \theta_{0}
                ,
                \qquad
                H_{1}: \theta = \theta_{1}
                .
            \end{equation*}
            Recall that
            \begin{equation*}
                \begin{aligned}
                    α(δ) & = \p{ \temoji{cross-mark}\, H_0|θ = θ_0 }, \\
                    β(δ) & = \p{ \temoji{check-mark}\, H_0|θ = θ_1 }.
                \end{aligned}
            \end{equation*}
        \end{column}
        \begin{column}{0.45\textwidth}
            \begin{figure}[htpb]
                \centering
                \includesvg[width=\textwidth]{Ex.9.2.1.svg}
                \caption*{Example 9.2.1 \testtube{}}
            \end{figure}
        \end{column}
    \end{columns}
    \pause{}
    We can either find $\delta^{\ast}$ where
    $$
    a \alpha(\delta^{\ast}) + b \beta(\delta^{\ast})
    =
    \inf_{\delta}
    a \alpha(\delta) + b \beta(\delta)
    $$
    or we can find $\delta^{\ast}$ s.t.\ $\alpha(\delta^{\ast}) \le \alpha_{0}$
    and
    $$
    \beta(\delta^{\ast})
    =
    \inf_{\delta:\alpha(\delta) \le \alpha_{0}}
    \beta(\delta)
    $$
\end{frame}

\section{9.3 Uniformly Most Powerful \emoji{muscle} Tests}

\subsection{Definition of a Uniformly Most Powerful Test}

\begin{frame}[c]
    \frametitle{Example 9.3.1 \testtube{}}

    Let $X_1,\ldots, X_n$ be the time $n$ people 
    have to wait in a queue to get a
    COVID test \emoji{test-tube}.

    Assume that $X_1,\ldots, X_n$ are \ac{iid} $\Exp(\theta)$.
    The hypotheses we consider are
    \begin{equation*}
        H_{0}: \theta \le \frac{1}{2}
        ,
        \qquad
        H_{1}: \theta > \frac{1}{2}
        .
    \end{equation*}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.3\textwidth]{long-queue.jpg}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Definition 9.3.1}
    We will consider
    \begin{equation*}
        H_{0}: \theta \in \Omega_{0}
        ,
        \qquad
        H_{1}: \theta \in \Omega_{1}
        \tag*{(9.3.1)}
        .
    \end{equation*}
    where $\Omega_{0}$ and $\Omega_{1}$ are disjoint subsets of $\dsR$.

    \pause

    A test $δ^{\ast}$ is a \alert{\acf{ump}}
    test of (9.3.1) at the \alert{level of significance} $α_0$ 
    if $α(δ^{\ast}) ≤ α_0$ and,
    \begin{equation*}
        \beta(\delta^{\ast})
        =
        \inf_{\delta:\alpha(\delta) \le \alpha_{0}}
        \beta(\delta)
        ,
        \qquad
        \text{for all } \theta \in \Omega_{1}
        .
    \end{equation*}
    \pause{}
    In other words
    \begin{equation*}
        1- π(θ|δ^{\ast})
        ≤
        \inf_{\delta:\alpha(\delta) \le \alpha_{0}}
        1 - π(θ |δ)
        \qquad
        ,
        \text{for all } \theta \in \Omega_{1}
        .
    \end{equation*}
\end{frame}

\subsection{Monotone Likelihood Ratio}

\begin{frame}
    \frametitle{Example 9.3.2}
    
    Assume that $X_1,\ldots, X_n$ are \ac{iid} $\Exp(\theta)$.
    Consider
    \begin{equation*}
        H_{0}': \theta \alert{=} \frac{1}{2}
        ,
        \qquad
        H_{1}': \theta = \theta'
        ,
    \end{equation*}
    where $\theta' > 1/2$

    \think{} 
    Can we find a $\delta^{\ast}$ 
    with $\alpha(\delta^{\ast}) \le \alpha_{0}$
    which minimizes $\beta(\delta^{\ast})$?

    \pause{}

    \emoji{open-mouth} Why is this test also a \ac{ump} for
    \begin{equation*}
        H_{0}': \theta \alert{=} \frac{1}{2}
        ,
        \qquad
        H_{1}: \theta > \frac{1}{2}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Definition 9.3.2}
    
    Let $\bfX = (X_{1}, \ldots, X_{n})$ be \ac{iid} \acp{rv} with \ac{pdf}
    $f_{n}(\bfx|\theta)$.

    Let $T = r(\bfX)$ be a statistics.

    Then $X$ has a \alert{\ac{mlr}} in $T$ if for $\theta_{1} < \theta_{2}$,
    the likelihood ratio
    $$
    \frac{f_{n}(\bfx|\theta=\theta_{2})}{f_{n}(\bfx|\theta=\theta_{1})}
    $$
    is a monotone function of $r(\bfx)$.
\end{frame}

\begin{frame}
    \frametitle{Example 9.3.3}
    
    Let $\bfX = (X_1,\ldots, X_n)$ be \ac{iid} $\Ber(p)$. 

    \think{} Can we show that $\bfX$ has \ac{mlr} in $Y= \sum_{i=1}^{n} X_{i}$?
\end{frame}

\begin{frame}
    \frametitle{Example 9.3.4}
    
    Let $\bfX = (X_1,\ldots, X_n)$ be \ac{iid} $\Exp(p)$. 

    \think{} Can we show that $\bfX$ has \ac{mlr} in $T= \sum_{i=1}^{n} X_{i}$?

    \cake{} Can we think of some other statistics in which $\bfX$ has a \ac{mlr}?
\end{frame}

\begin{frame}
    \frametitle{Example 9.3.5}
    
    Let $\bfX = (X_1,\ldots, X_n)$ be \ac{iid} $\scN(\mu, \sigma^{2})$,
    where $\mu$ is unknown and $\sigma$ is known.

    \think{} Can we show that $\bfX$ has \ac{mlr} in $\overline{X}_n$?

    \pause{}

    \bonus{} Can you find a statistics for which $\bfX$ has a \ac{mlr}
    where $\mu$ is known and $\sigma$ is unknown.
\end{frame}

\subsection{One-Sided Alternatives}

\begin{frame}
    \frametitle{Theorem 9.3.1}
    
    Consider
    \begin{equation*}
        H_{0}: \theta \le \theta_{0}
        ,
        \qquad
        H_{1}: \theta > \theta_{0}
        .
    \end{equation*}

    Suppose that $\bfX$ has \emph{increasing} \ac{mlr} in $T$.

    Let $c$ and $\alpha$ be constants such that
    \begin{equation*}
        \p{T \ge c | \theta = \theta_{0}}
        =
        \alpha_{0}
        .
    \end{equation*}

    Let $\delta^{\ast}$ be the test which 
    \accept{} if $T \ge c$,
    \reject{} if $T < c$.

    Then $\delta^{\ast}$ is \ac{ump}.
\end{frame}

\begin{frame}
    \frametitle{Theorem 9.3.1 --- Some intuition}

    \hint{} The theorem says instead of
    \begin{equation*}
        H_{0}: \theta \le \theta_{0}
        ,
        \qquad
        H_{1}: \theta > \theta_{0}
        ,
    \end{equation*}
    we can consider
    \begin{equation*}
        H_{0}': \theta \alert{=} \theta_{0}
        ,
        \qquad
        H_{1}': \theta = \theta_{1}
        ,
    \end{equation*}
    for any $\theta_{1}' > \theta_{0}$.

    \pause{}

    So the \emph{best} test is the one which \reject{} if
    \begin{equation*}
        \frac{f(\bfX|\theta = \theta_{1})}{f(\bfX|\theta = \theta_{0})}
        \ge k
        \iff
        T \ge c
        .
    \end{equation*}
    Thus, we pick $c$ such that
    \begin{equation*}
        \p{T \ge c | \theta = \theta_{0}} = \alpha_{0}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.3.6}
    
    Assume that $X_1,\ldots, X_n$ are \ac{iid} $\Exp(\theta)$.
    Consider
    \begin{equation*}
        H_{0}: \theta \alert{\le} \frac{1}{2}
        ,
        \qquad
        H_{1}: \theta \alert{>} \frac{1}{2}
        .
    \end{equation*}

    \think{} 
    Can we find a \ac{ump}?
\end{frame}

\begin{frame}
    \frametitle{Example 9.3.7}
    
    Assume that $X_1,\ldots, X_n$ are \ac{iid} $\Ber(p)$.
    Consider
    \begin{equation*}
        H_{0}: \theta \alert{\le} 0.1
        ,
        \qquad
        H_{1}: \theta \alert{>} 0.1
        .
    \end{equation*}

    \think{} 
    Can we find a \ac{ump}?
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-16.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 9.3, 
                    Exercise 1, 3, 5, 7, 9, 11, 13, 15.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
