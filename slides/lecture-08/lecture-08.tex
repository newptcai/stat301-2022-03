\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 08}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{8.3 Joint Distribution of the Sample Mean and Sample Variance}

\subsection{Independence of the Sample Mean and Sample Variance}

\begin{frame}[c]
    \frametitle{Theorem 8.3.1}
    
    Let $X_1,\ldots, X_n$ be \acs{iid} $\scN(\mu, \sigma^{2})$.
    Recall that \acsp{mlee} of $\mu$ and $\sigma^{2}$ are
    \begin{equation*}
        \hat{\mu} = 
        \overline{X}_n
        =
        \frac{1}{n}
        \sum^{n}_{i=1} X_i
        ,
        \qquad
        \hat{\sigma}^{2}
        =
        \frac{1}{n}
        \sum^{n}_{i=1} (X_i-\overline{X}_n)^2
        .
    \end{equation*}
    These two \acp{rv} are independent and
    \begin{equation*}
        \hat{\mu}
        \sim
        \scN\left(\mu, \frac{\sigma^{2}}{n}\right)
        ,
        \qquad
        \frac{n}{\sigma^{2}}
        \hat{\sigma}^{2}
        \sim
        \chi^{2}(n-1)
        .
    \end{equation*}

    \emoji{smile} We will just sketch the proof. 
\end{frame}

\begin{frame}
    \frametitle{An experiment}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=1\textwidth]{thm.8.3.1.png}
        \includegraphics<2>[width=1\textwidth]{thm.8.3.1-1.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Proof sketch}
    
    \begin{block}{Theorem 8.3.4}
        Let $\bfX = (X_1,\ldots, X_n)$ be \acs{iid} standard normal.
        Let $A$ be an orthogonal matrix. Then $\bfY = A \bfX$ is also a vector of
        \acs{iid} standard normal.

        Moreover $\sum_{i=1}^{n} X_{i}^{2} = \sum_{i=1}^{n}Y_{i}^{2}$.
    \end{block} 
\end{frame}

\begin{frame}
    \frametitle{Example 8.3.2}
    
    Let $n = 26$ and 
    $X_1,\ldots, X_n$ be \acs{iid} $\scN(\mu, \sigma^{2})$.
    What is
    \begin{equation*}
        \p{\hat{\sigma}^{2} \le 0.77 \sigma^{2}}.
    \end{equation*}
\end{frame}

\subsection{Estimation of the Mean and Standard Deviation}

\begin{frame}
    \frametitle{How many experiments do we need?}
    
    Let $X_1,\ldots, X_n$ be \acs{iid} $\scN(\mu, \sigma)$.
    What is the minimum $n$ we need such that
    \begin{equation*}
        \p{
            \left[\abs{\hat{\mu}-\mu} \le \frac{1}{5} \sigma\right]
            \cap
            \left[\abs{\hat{\sigma}-\sigma} \le \frac{1}{5} \sigma\right]
        }
        \ge
        \frac{1}{2}
    \end{equation*}
\end{frame}

\section{8.4 The $t$ Distributions}

\subsection{Definition of the Distributions}

\begin{frame}[c]
    \frametitle{The $t$ Distributions}
    
    Consider two independent \acp{rv} $Y \sim \chi^{2}(m)$ and $Z \sim \scN(0, 1)$.
    The distribution of the random variable
    \begin{equation*}
        X
        =\frac{Z}{\displaystyle \left(\frac{Y}{m}\right)^{\frac{1}{2}}}
    \end{equation*}
    is called the \alert{$t$ distribution with freedom $m$}.
\end{frame}

\begin{frame}
    \frametitle{Theorem 8.4.1}

    The \acs{rv} $X$ has \acs{pdf}
    \begin{equation*}
        \frac{\Gamma(\frac{m+1}{2})}{(m \pi)^{1/2} \Gamma(\frac{m}{2})}
        \left(1+\frac{x^{2}}{m}\right)^{-(m+1)/2}
    \end{equation*}
    \only<1>{
    \cake{} Can you show
    \begin{equation*}
        \left(1+\frac{x^{2}}{m}\right)^{-(m+1)/2}
        \to e^{-\frac{x^2}{2}},
        \qquad
        \text{as }
        m \to \infty?
    \end{equation*}
    }
    \only<2>{
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{8.4.png}
    \end{figure}
    }
\end{frame}

\begin{frame}[c]
    \frametitle{Moments of the $t$ Distributions}
    
    When $m \le 1$, $E{X} = \infty$.

    When $m > 1$,
    \begin{equation*}
        \E{\abs{X}^{k}}
        \begin{cases}
            < \infty & \text{if } k < m, \\
            = \infty & \text{otherwise}.
        \end{cases}
    \end{equation*}
    
    \cake{} What is $\E{X}$ when $\E{X} < \infty$?
\end{frame}

\subsection{Relation to Random Samples from a Normal Distribution}

\begin{frame}
    \frametitle{Theorem 8.4.2}
    
    Let $X_1,\ldots, X_n$ be \acs{iid} $\scN(\mu, \sigma^{2})$.
    Let
    \begin{equation*}
        \sigma' = 
        \left(
        \frac{\sum_{i=1}^{n} (X_{i}-\overline{X}_n)^{2}}{n-1}
        \right)^{\frac{1}{2}}
    \end{equation*}
    Then
    \begin{equation*}
        \frac{n^{\frac{1}{2}}}{\sigma'}(\overline{X}_n-\mu)
        \sim t(n-1).
    \end{equation*}

    \cool{} The quantity $\sigma'$ and its distribution does not depend on
    $\sigma$.
\end{frame}

\begin{frame}
    \frametitle{Example 8.4.3}
    
    Let $n = 26$ and $X_1,\ldots, X_n$ be \acs{iid} $\scN(\mu, \sigma)$.
    Then
    \begin{equation*}
        \p{\overline{X}_n \le \mu + 0.2581 \sigma'} = 0.9.
    \end{equation*}
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-08.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 8.3, Exercise 1, 5, 7, 9.
                \item[\emoji{pencil}] Section 8.4, Exercise 1, 3, 5, 7.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
