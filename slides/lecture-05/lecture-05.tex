\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 05}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{7.6 Properties of Maximum Likelihood Estimators}

\subsection{Invariance}

\begin{frame}
    \frametitle{Invariance Property}
    
    \begin{block}{Theorem 7.6.1}
        If $\hat{\theta}$ is the \ac{mlee} of $\theta$ and $g$ is a one-to-one
        function,
        then $g(\hat{\theta})$ is the \ac{mlee} of $g(\theta)$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 7.6.2}
    
    In Example 7.5.2, 
    the \ac{mlee} of $\theta$ is $\hat{\theta} = 0.455$.

    \cake{} What is the \ac{mlee} of $\frac{1}{\theta}$?
\end{frame}

\begin{frame}
    \frametitle{\ac{mlee} of a Function}
    
    Let $g : \Omega \mapsto G$ be an \emph{arbitrary} function.

    For $t \in G$, let
    \begin{equation*}
        G_{t} = \{\theta : g(\theta) = t\}.
    \end{equation*}

    \pause{}

    Let
    \begin{equation*}
        L^{\ast}(t) = \max_{\theta \in G_{t}} \log f_{n}(\bfx|\theta).
    \end{equation*}

    \pause{}

    The \alert{\ac{mlee}} of $g(\theta)$ is the value $\hat{t}$ where
    \begin{equation*}
        L^{\ast}(\hat{t}) = \max_{t \in G} L^{\ast}(t).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 7.6.3}
    
    Let $X_1,\ldots, X_n$ be \acs{iid} copies of $X \sim \scN(\mu, \sigma^{2})$ with 
    unknown parameter $\theta = (\mu, \sigma^{2}) \in \Omega = \dsR \times (0, \infty)$.

    Let $(\hat{\mu}, \hat{\sigma}^{2})$ be the \ac{mlee} of $\theta$.

    \cake{} What is the \acp{mlee} of $\sigma$ and $\E{X^{2}} = \mu^{2} +
    \sigma^{2}$?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Let $X_{1},\ldots, X_{n}$ be \ac{iid} copies of $X \sim \Poi(\theta)$.
    In other words, $X$ all have \ac{pf}
    \begin{equation*}
        f(i|\theta) 
        =
        \p{X = i|\theta}
        =
        \begin{cases}
            \displaystyle
            \frac{e^{-\theta } \theta ^i}{i!}, & i \in \{0, 1, 2, \dots\} \\
            0, & \text{ otherwise}.
        \end{cases}
    \end{equation*}

    Find the \acs{mle} of $\theta$ and $\sqrt{\V{X}} = \sqrt{\theta}$.

    \hint{} Maximize $L(\theta) = \log f_{n}(\bfx|\theta)$.
\end{frame}

\subsection*{Consistency}

\begin{frame}[c]
    \frametitle{Consistency}

    In all the cases we have seen, the \ac{mle} is \alert{consistent}.

    In other words, as the number of observations $n \to \infty$,
    \begin{equation*}
        \hat{\theta} \inprob \theta.
    \end{equation*}

    Since Bayes estimators are also \alert{consistent}, the two are close 
    when $n$ is \emph{large}.
\end{frame}

\begin{frame}
    \frametitle{Example}
    Let $X_1,\ldots, X_n$ be \ac{iid} $\Ber(\theta)$.

    If we assume the prior distribution $\theta = \Be(\alpha, \beta)$,
    then by Example~7.4.3, 
    \begin{equation*}
        \delta^{\ast}(\bfX) = 
        \frac{\alpha + \sum_{i=1}^{n} X_{i}}{\alpha + \beta + n}
        \inprob
        \frac{\sum_{i=1}^{n} X_{i}}{n}
        =
        \overline{X}_n.
    \end{equation*}
    \pause{}
    On the other hand, we have the \ac{mle} of $\theta$
    \begin{equation*}
        \delta(\bfX) 
        = 
        \frac{\sum_{i=1}^{n} X_{i}}{n}
        =
        \overline{X}_n.
    \end{equation*}
    \pause{}
    Thus
    \begin{equation*}
        \delta(\bfX)  \inprob \delta^{\ast}(\bfX)
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Limitation of \ac{mle}}
    Let $X_1,\ldots, X_5$ be \acs{iid} $\Ber(\theta)$ \acsp{rv} representing a \emoji{coin}
    toss getting head.
    Assume that we have observed $\bfx = (1, 1, 1, 0, 1)$.

    If we take the prior distribution $\theta = \Be(100, 100)$,
    then
    \begin{equation*}
        \delta^{\ast}(\bfx) = 
        \frac{\alpha + \sum_{i=1}^{n} x_{i}}{\alpha + \beta + n}
        =
        \frac{30 + 4}{30 + 30 + 5}
        \approx
        0.5230
        .
    \end{equation*}
    \only<1>{%
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.6\textwidth]{beta-30-30.png}
        \end{figure}
    }
    \only<2>{On the other hand, we have the \ac{mle} of $\theta$
    \begin{equation*}
        \delta(\bfx) 
        = 
        \frac{\sum_{i=1}^{n} x_{i}}{n}
        =
        \frac{4}{5}
        =
        0.8
    \end{equation*}
    \bomb{} When there are very few observations, Bayes estimator might make more
    sense.
    }
\end{frame}

\subsection{Numerical Computation}

\begin{frame}
    \frametitle{Example 7.6.4}\label{ex:7:6:4}
    
    Let $X_{1},\ldots, X_{n}$ be \ac{iid} $\Gam(\alpha, 1)$.

    Show that $\hat{\alpha}$, the \ac{mlee} of $\alpha$, satisfies
    \begin{equation*}
        \Psi(\alpha)
        \eqd
        \frac{\Gamma(\alpha)'}{\Gamma(\alpha)}
        =
        \frac{1}{n}\log \left(\prod _{i=1}^n x_{i}\right)
        ,
    \end{equation*}
    where $\Psi(\alpha)$ is called the
    \href{https://dlmf.nist.gov/search/search?q=digamma\%20function}{digamma} function.
\end{frame}

\begin{frame}
    \frametitle{Example 7.6.5}
    
    Let $X_{1}, \ldots, X_{n}$ be \ac{iid} Cauchy \acp{rv} with parameters $(\theta,
    1)$. Thus $X_{i}$ has \acs{pdf}
    \begin{equation*}
        f(x|\theta)
        =
        \frac{1}{\pi  \left((x-\theta )^2+1\right)}
    \end{equation*}

    Find $\hat{\theta}$, the \ac{mlee} of $\theta$.

    \pause{}

    \cake{} What is $\E{X_{1}|\theta}$?
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{cauchy-3-1.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Newton's method}
    
    Let $\theta_{0}$ be a guess for the solution of $f(\theta) = 0$.

    Newton's method replace $\theta_{0}$ with an updated guess
    \begin{equation*}
        \theta_{1} =\theta_{0} - \frac{f(\theta_0)}{f'(\theta_{0})}
    \end{equation*}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{7.7.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 7.6.6}
    
    Continuing \hyperlink{ex:7:6:4}{Example~7.6.4},
    assume that we observed
    \begin{equation*}
        \frac{1}{20} \log\left(\prod_{i=1}^{20} x_{i}\right) = 1.220,
        \qquad
        \frac{1}{20} \sum_{i=1}^{20} x_{i} = 3.679.
    \end{equation*}

    Find $\hat{\alpha}$, which satisfies $\Psi(\hat{\alpha}) = 1.22$ using Newton's
    Method.

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{eg.7.6.6.png}
    \end{figure}
\end{frame}

\subsection{Method of Moments}

\begin{frame}
    \frametitle{Method of Moments}
    
    Let $X_1,\ldots, X_n$ be \ac{iid} with unknown parameter $\theta$.

    Let 
    \begin{equation*}
    \mu_{i}(\theta) = \E{X_{1}^{i} \cond \theta}
    ,
    \qquad
    m_{i} 
    = 
    \frac{1}{n}
    \sum_{j=1}^n X_{j}^{i}
    .
    \end{equation*}

    Suppose that $\mu(\theta) = (\mu_{1}(\theta), \dots, \mu_{k}(\theta))$ is
    one-to-one and $M$ is its inverse function.

    \pause{}

    Then the \alert{method of moments} estimator of $\theta$ is
    \begin{equation*}
        M(m_{1}, \ldots, m_{k}).
    \end{equation*}

    \zany{} It is usually easy to compute!
\end{frame}

\begin{frame}
    \frametitle{Example 7.6.8}
    
    In \hyperlink{ex:7:6:4}{Example~7.6.4}, 
    $X_{1},\ldots, X_{n}$ are \ac{iid} $\Gam(\alpha, 1)$.

    Find $\hat{\alpha}$, the method of moment estimator of $\alpha$.
\end{frame}

\begin{frame}
    \frametitle{Example 7.6.9}
    
    Let $X_1,\ldots, X_n$ be \ac{iid} $\Be(\alpha, \beta)$.

    Find $\hat{\alpha}, \hat{\beta}$, the method of moment estimators of $\alpha$ and
    $\beta$.
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-05.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 7.6, Exercise 1, 3, 5, 7, 9, 11, 13,
                    15, 19, 21.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
