\input{../meta.tex}

\title{Lecture 24}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{11.2 Regression}

\begin{frame}[c]
    \frametitle{A Bit Random Knowledge}
    
    \alert{regression} ---

    \begin{enumerate}
        \item an abnormal state in which development has stopped 
            prematurely.
       \item (psychiatry) a defense mechanism in which you flee from 
           reality by assuming a more infantile state.
    \end{enumerate}

    \pause{}

    In statistics, \alert{regression toward the mean} refers to the fact that if one sample of a
    random variable is extreme, the next sampling of the same random variable is likely
    to be closer to its mean.
\end{frame}

\subsection{Regression Functions}

\begin{frame}[c]
    \frametitle{Example 11.2.1}
    
    \begin{columns}[c]
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{table-11.5.png}
            \end{figure}
        \end{column}
        \begin{column}{0.6\textwidth}
            We can predicate the pressure $y$ from the boiling point
            $x$ by
            \begin{equation*}
                y = \hbeta_{0} + \hbeta_{1} x.
            \end{equation*}

            \think{}
            But if we consider the pressure $Y$ to be \ac{rv}, 
            can we find a model so we can say something about it?
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{Definition 11.2.1}
    
    We are interested in the conditional distribution of a \ac{rv} $Y$, given the
    values of some \emph{variables} $X_1,\ldots, X_n$, which are
    \begin{itemize}
        \item either random variables,
        \item or \alert{control variables} decided by \emoji{scientist}.
    \end{itemize}

    \pause{}

    The $X_1,\ldots, X_n$ are called \alert{predictors}.

    The \ac{rv} $Y$ is called the \alert{response}.

    The conditional expression
    \begin{equation*}
        \E{Y|X_{1}, \dots, X_{k}}
    \end{equation*}
    is the \emph{regression function of $Y$ on $X_1,\ldots, X_n$}.
\end{frame}

\begin{frame}
    \frametitle{The General Setup}
    
    We will assume that
    \begin{equation*}
        \E{Y|X_{1}, \dots, X_{k}}
        =
        \beta_{0}
        +
        \beta_{1} X_{1}
        +
        \dots
        +
        \beta_{k} X_{k}
        ,
    \end{equation*}
    where $\beta_1,\ldots, \beta_k$ are \emph{unknown} parameters called 
    \emph{regression coefficients}.

    Assume also that we have $n$ observations of $(X_1,\ldots, X_k, Y)$, denoted
    by $(x_{i1},\ldots, x_{ik}, y_{i})$.

    The estimates of $\hbeta_1,\ldots, \hbeta_k$ are called
    \alert{least-squares estimators} of $\beta_1,\ldots, \beta_k$.
\end{frame}

\subsection{Simple Linear Regression}

\begin{frame}[c]
    \frametitle{Assumptions}
    
    We will assume that $Y = \beta_{0} + \beta_{1} X + \epsilon$, 
    where $\epsilon \sim \scN(0, \sigma^{2})$.

    This type of problems are called \alert{simple linear regression}.

    \begin{block}{\bomb{} A Change of Notation}
        We will use 
        $(X_{1}, Y_{1}), \dots, (X_{n}, Y_{n})$
        to denote $n$ \ac{iid} copies (observations) of $(X, Y)$.
    \end{block}
\end{frame}

\begin{frame}{Assumptions 11.2.1-5}
    We will further assume
    \begin{itemize}
        \item Predictor is known -- We will condition on the event $X_{1} = x_{1}, \dots, X_{n} = x_{n}$.
        \item Normality -- The conditional distribution of $Y_{i}$ are all normal.
        \item Linear Mean -- The conditional mean of $Y_{i}$ is $\beta_{0} +
            \beta_{1} x_{i}$.
        \item Common Variance -- The conditional variance of $Y_{i}$, $\sigma^{2}$ is
            the same.
        \item Independence -- $Y_1,\ldots, Y_n$ are independent conditioned on $X_{1}
            = x_{1}, \dots, X_{n} = x_{n}$.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Theorem 11.2.1}
    
    Given that $ Y_1 = y_1, \dots, Y_{n} = y_{n}$,
    the \acp{mle} of $\beta_{0}$ and $\beta_{1}$ are $\hbeta_{0}$ and
    $\hbeta_{1}$, and the \ac{mle} of $\sigma^{2}$ is
    \begin{equation*}
        \hat{\sigma}^{2}
        =
        \frac{1}{n}
        \sum^{n}_{i=1} (y_{i} - \hbeta_{0} - \hbeta_{1})^{2}.
    \end{equation*}
\end{frame}

\subsection{The Distribution of the Least-Squares Estimators}

\begin{frame}
    \frametitle{Estimators as Random Variables}

    Recall that
    \begin{equation*}
        \begin{aligned}
            \hbeta_1
            &
            =
            \frac
            {\sum_{i=1}^{n}(Y_i - \overline{Y})(x_i - \overline{x})}
            {\sum_{i=1}^{n}(x_i - \overline{x})^2}
            ,
            \\
            \hbeta_2
            &
            =
            \overline{Y} - \hbeta_{1} \overline{x}
            ,
        \end{aligned}
    \end{equation*}
    where 
    $\overline{x} = \frac{1}{n} \sum_{i=1}^{n} x_{i}$
    and
    $\overline{Y} = \frac{1}{n} \sum_{i=1}^{n} Y_{i}$
    .

    So in the current setup, $\hbeta_{0}$ and $\hbeta_{1}$ are \acp{rv}.

    \think{} What are the distributions of them?
\end{frame}

\begin{frame}
    \frametitle{Theorem 11.2.2}
    
    Let
    \begin{equation*}
        s_{x} = \left(\sum^{n}_{i=1} (x_{i}- \overline{x})^{2}\right)^{1/2}.
    \end{equation*}
    Then, we have
    \begin{equation*}
        \hbeta_{1}
        \sim
        \scN
        \left(
        \beta_{1},
        \frac{\sigma^{2}}{s_{x}^{2}}
        \right)
        ,
        \qquad
        \hbeta_{0} 
        \sim 
        \scN
        \left(\beta_{0}, \sigma^{2}\left(\frac{1}{n} +
        \frac{\overline{x}}{s_{x}^{2}}\right)\right).
    \end{equation*}
    Moreover
    \begin{equation*}
        \mathrm{Cov}(\hbeta_{0}, \hbeta_{1})
        =
        -
        \frac{\overline{x} \sigma^{2}}{s_{2}^{2}}
        .
    \end{equation*}

    So $\hbeta_0$ and $\hbeta_1$ are unbiased.
\end{frame}

\begin{frame}
    \frametitle{Example 11.2.2}
    
    What are the variances of $\hbeta_0$ and $\hbeta_1$ and the \emoji{droplet}
    example?
\end{frame}

\begin{frame}
    \frametitle{Example 11.2.3}
    
    Let $T = c_{0} \hbeta_{0} + c_{1} \hbeta_{1} + c_{\ast}$.
    What is $\V{T}$?
\end{frame}

\section{Prediction}

\begin{frame}
    \frametitle{Theorem 11.2.3}
    
    The \ac{mse} of $\hat{Y} = \hbeta_{0} + \hbeta_{1} X$ 
    conditioned on $X = x$ is
    \begin{equation*}
        \E{(\hat{Y}-Y)^{2} | X = x}
        =
        \sigma^{2}
        \left(
        1 
        + 
        \frac{1}{n}
        +
        \frac{(x- \overline{x})^{2}}{s_{x}^{2}}
        \right)
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 11.2.5}
    
    What is the \ac{mse} when we predicate the pressure when boiling point of water
    is $201.5$ using the least-squares line?
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-23.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 11.1, Exercise 1, 3, 5, 7, 9. (This is
                    for lecture 23.)
                \item[\emoji{pencil}] Section 11.2, Exercise 1, 3, 5, 7, 9, 11, 13,
                    15, 17.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
