\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 12}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{9.1 Problems of Testing Hypotheses}

\subsection{The Null and Alternative Hypotheses}

\begin{frame}
    \frametitle{Example 9.1.1}

    Let $X_1,\ldots, X_{26}$ be log-rainfall from $26$ seeded clouds.

    Assume that they are \ac{iid} $\scN(\mu, \sigma^{2})$.

    \think{} How to tell if $\mu > 4$?
    
    \only<1>{
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{cloud-seeding.png}
        \caption*{From \href{https://en.wikipedia.org/wiki/Cloud\_seeding}{Wikipedia}}
        \label{fig:}
    \end{figure}
    }


    \only<2>{
        Let
        \begin{equation*}
            \Omega_{0} = (4, \infty) \times (0, \infty)
            ,
            \qquad
            \Omega_{1} = [0, 4] \times (0, \infty)
            .
        \end{equation*}
        We want to know which hypotheses is true
        \begin{equation*}
            H_{0} = (\mu, \sigma) \in \Omega_{0}
            ,
            \qquad
            H_{1} = (\mu, \sigma) \in \Omega_{1}.
        \end{equation*}
    }
\end{frame}

\begin{frame}[c]
    \frametitle{Definition 9.1.1}

    The hypothesis $H_0$ is called the \alert{null hypothesis}.

    The hypothesis $H_1$ is called the \alert{alternative hypothesis}. 

    If we decide that $θ \in \Omega_{1}$ , we are said to \alert{reject} $H_0$. 

    If we decide that $θ \in \Omega_{0}$ , we are said to \alert{reject} $H_1$. 
\end{frame}

\begin{frame}
    \frametitle{Example 9.1.2}
    
    Let $X_1,\ldots, X_{n}$ be breadth of human \emoji{skull} in ancient Egypt.

    Assume that they are \ac{iid} $\scN(\mu, \sigma^{2})$.

    \think{} How to tell if $\mu \ge 140$?

    \cake{} What are $\Omega_{0}$ and $\Omega_{1}$?  What are $H_{0}$ and $H_{1}$?
\end{frame}

\subsection{Simple and Composite Hypotheses}

\begin{frame}[c]
    \frametitle{Definition 9.1.2, 9.1.3}
    
    If $\Omega_i$ contains just a single value of $θ$, then
    it is a \alert{simple hypothesis}. 
    Otherwise it is a \alert{composite hypothesis}.

    If $H_{0}$ is of the form $\theta \le \theta_{0}$ or $\theta \ge \theta_{0}$,
    then it is a \alert{one-sided hypothesis}.
    Otherwise it is a \alert{two-sided hypothesis}.

    \cake{} What types of hypotheses are in the \emoji{skull} example?
\end{frame}

\subsection{The Critical Region and Test Statistics}

\begin{frame}
    \frametitle{Example 9.1.3}
    
    Let $X_1,\ldots, X_{n}$ be \ac{iid} $\scN(\mu, \sigma^{2})$.
    Let
    \begin{equation*}
        H_{0}: \mu = \mu_{0}
        ,
        \qquad
        H_{1}: \mu \ne \mu_{0}
        .
    \end{equation*}
    Let
    \begin{equation*}
        S_{0} = \{\bfx: \abs{\overline{X}_n - \mu_{0}} < c\}
        ,
        \qquad
        S_{1} = S_{0}^{c}
        .
    \end{equation*}

    \cake{} Should we reject $H_{0}$ if $\bfX \in S_{0}$?
    What about $\bfX \in S_{1}$?
\end{frame}

\begin{frame}
    \frametitle{Definition 9.1.4, 9.1.5}

    The set $S_1$ in the previous example is called the \alert{critical region} of the test.

    Let $T = r(\bfX)$ be a statistic, and let $R$ be a subset of the real line. 

    If a test procedure reject $H_{0}$ if $T \in R$,
    then we call $T$ a \alert{test statistic}, and $R$ 
    the \alert{rejection region} of the test.

    \cake{} 
    What is $R$ in the previous example?
\end{frame}

\subsection{The Power Function and Types of Error}

\begin{frame}[c]
    \frametitle{Definition 9.1.6}
    
    Let $\delta$ be a test.
    The \alert{power function} $\pi(\theta|\delta)$ is defined by
    \begin{equation*}
        \pi(\theta|\delta) 
        = 
        \p{\bfX \in S_{1}|\theta}
        =
        \p{T \in R|\theta}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.1.5}
    
    Let $X_1,\ldots, X_{n}$ be \ac{iid} $\scN(\mu, \sigma^{2})$.
    Let
    \begin{equation*}
        H_{0}: \mu = \mu_{0}
        ,
        \qquad
        H_{1}: \mu \ne \mu_{0}
        .
    \end{equation*}
    We reject $H_{0}$ if $T = \abs{\overline{X}_n - \mu_{0}} \ge c$.

    What is $\pi(\theta|\delta)$?

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{9.1.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Definition 9.1.7}
    
    An erroneous decision to reject a true null hypothesis is a \alert{type I error},
    or an \alert{error of the first kind}. 

    An erroneous decision not to reject a false null hypothesis
    is called a \alert{type II error}, or an \alert{error of the second kind}.

    \cake{} Can you fill the table in terms of power functions?

    \begin{table}[]
        \begin{tabular}{|l|l|l|}
            \hline
            Probability & $\theta \in \Omega_{0}$  & $\theta \in \Omega_{1}$ \\ \hline
            Type I error &   &  \\ \hline
            Type II error &   &  \\ \hline
        \end{tabular}
    \end{table}
\end{frame}

\begin{frame}
    \frametitle{How to minimize the errors?}
    
     The most popular method for striking a balance between the two types of error 
     is to choose a number $α_{0}$ require that
     \begin{equation*}
         π(θ |δ) \le α_0, \qquad \text{for all } θ \in \Omega_0 \tag*{(9.1.6)}.
     \end{equation*}

    Then, among all tests that satisfy this, 
    we 
    seeks a test which (\think{} in some sense) maximizes $\pi(\theta|\delta)$ for $\theta \in
    \Omega_{1}$.
\end{frame}

\begin{frame}
    \frametitle{Definition 9.1.8}
    
    A test that satisfies (9.1.6) is called \alert{a level $α_0$ test}, 

    The \alert{size} $α(δ)$ of a test $δ$ is defined by
    \begin{equation*}
        \alpha(\delta) = \sup_{\theta \in \Omega_{0}} \pi(\theta|\delta).
    \end{equation*}

    \cake{} A test $δ$ is a level $α_0$ test if and only if its size is at most
    \question{}.
\end{frame}

\begin{frame}
    \frametitle{Example 9.1.7}
    
    Let $X_1,\ldots, X_n$ be \ac{iid} $\Unif[0,\theta]$.
    Let
    \begin{equation*}
    H_0: 3 \le θ \le 4,
    \qquad
    H_1: θ < 3 \text{ or } θ > 4.
    \end{equation*}

    Let $Y_{n} = \max\{X_{1},\dots, X_{n}\}$ be the test statistics.

    Let $R = (-\infty, 2.9] \cup [4, \infty)$ be the rejection region.

    \cake{} What is $\pi(\theta|\delta)$?
    What is the size of $\delta$?
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-12.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 9.1, Exercise 1, 3, 5, 7, 9,
                    15.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
