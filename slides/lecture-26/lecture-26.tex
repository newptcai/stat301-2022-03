\input{../meta.tex}

\title{Lecture 26}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{Story Time --- The Odds, Continually Updated}

\begin{frame}[c]
    \frametitle{How Bayes Statistics Saved a Life}
    
    Statistics may not sound like the most heroic of pursuits. 
    But if not for statisticians, 
    a Long Island fisherman might have died 
    in the Atlantic Ocean after falling off his boat early one morning last summer.

    The man owes his life to a once obscure field known as \emph{Bayesian statistics} \dots

    \hfill ---
    \href{https://www.nytimes.com/2014/09/30/science/the-odds-continually-updated.html}{New
    York Times} Sept.~29, 2014
\end{frame}

\begin{frame}
    \frametitle{How Did It Work?}

    At first, all the Coast Guard knew about the fisherman was 
    that he fell off his \emoji{sailboat} \emph{sometime from 9 p.m.\ on July 24 to 6 the next morning}. 

    The sparse information went into a \emoji{desktop-computer} \dots    

    Over the next few hours, searchers added new information --- 
    \begin{itemize}
        \item prevailing currents, 
        \item places the search \emoji{helicopter} had already flown 
        \item and some additional clues found by the boat's captain.
    \end{itemize}

    The system could not deduce exactly where Mr.~Aldridge was drifting, 
    but with more information, 
    it continued to narrow down \emph{the most promising places to search}.
\end{frame}

\begin{frame}[c]
    \frametitle{Happy Ending \emoji{smile}}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            Just before turning back to refuel, 
            a searcher in a \emoji{helicopter} spotted 
            a man clinging to two buoys he had tied together. 

            He had been in the water for \emph{12 hours} \dots

            Even in the jaded 21st century, it was considered something of a miracle.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{fisherman.jpg}
                \caption*{John Aldridge}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\section{11.4 Bayesian Inference in Simple Linear Regression}

\subsection{Improper Priors for Regression Parameters}

\begin{frame}
    \frametitle{The Setup for a Bayes Analysis}
    
    Consider the pair of \acp{rv} $(X, Y)$ such that
    \begin{equation*}
       Y|X \sim \scN(\beta_{0} + \beta_{1} X, \sigma^{2}).  
    \end{equation*}

    Let
    \begin{equation*}
        \tau = \frac{1}{\sigma^{2}}
    \end{equation*}
    be the \alert{precision}.

    Assume that $(\beta_{0}, \beta_{1}, \tau)$ are \acp{rv} with an
    \emph{improper} \ac{pdf}
    \begin{equation*}
        \xi(\beta_{0}, \beta_{1}, \tau) = \frac{1}{\tau}.
    \end{equation*}

    \cake{} Why is this \ac{pdf} \emph{improper}?
\end{frame}

\begin{frame}
    \frametitle{Theorem 11.4.1}
    
    Given observations 
    $(X_{1}, Y_{1}) = (x_{1}, y_{1}), \dots, (X_{n}, Y_{n}) = (x_{n}, y_{n})$
    the posterior distributions are ---

    Conditioning on $\tau$,  $(\beta_{0}, \beta_{1})$ 
    is a bivariate normal distribution with correlation
    \begin{equation*}
        \frac
        {- n \overline{x}}
        {(n \sum_{i=1}^{n} x_{i}^{2})^{1/2}}
    \end{equation*}
    and
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{table-11.10.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Theorem 11.4.1}
    
    Moreover
    \begin{equation*}
        \tau \sim \Gam\left(\frac{n-2}{2}, \frac{S^{2}}{2}\right),
    \end{equation*}
    and
    \begin{equation*}
        \left(
            \frac{c_{0}^{2}}{n}
            +
            \frac{(c_{0}\overline{x}-c_{1})^{2}}{s_{x}^{2}}
        \right)^{-1/2}
        \left(
            \frac
            {
                c_{0} \beta_{0} + c_{1} \beta_{1} 
                - (c_{0} \hat{\beta}_{0}+ c_{1} \hbeta_{1})
            }{\sigma'}
        \right)
        \sim
        t(n-2)
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Compare with Theorem 11.3.3}

    Recall that in Theorem 11.3.3
    \begin{equation*}
        U_{01}
        =
        \left(
            \frac{c_{0}^{2}}{n}
            +
            \frac{(c_{0}\overline{x}-c_{1})^{2}}{s_{x}^{2}}
        \right)^{-1/2}
        \left(
            \frac{c_{0} \hbeta_{0} + c_{1} \hbeta_{1} - c_{\ast}}{\sigma'}
        \right)
        \sim
        t(n-2)
        .
    \end{equation*}
    Compare this with
    \begin{equation*}
        \left(
            \frac{c_{0}^{2}}{n}
            +
            \frac{(c_{0}\overline{x}-c_{1})^{2}}{s_{x}^{2}}
        \right)^{-1/2}
        \left(
            \frac
            {
                c_{0} \beta_{0} + c_{1} \beta_{1} 
                - (c_{0} \hat{\beta}_{0}+ c_{1} \hbeta_{1})
            }{\sigma'}
        \right)
        \sim
        t(n-2)
        .
    \end{equation*}
    \emoji{expressionless} So in practice the results in this section 
    are the same as in 11.3.
\end{frame}

\begin{frame}
    \frametitle{Example 11.4.2}
    
    \think{} 
    Given the data in Example 11.3.6 \emoji{droplet}, 
    how to use Bayes analysis to find $A$ such that
    \begin{equation*}
        \p{-A < \beta_{1} < A | \bfx}
        =
        1 - \alpha_{0}
    \end{equation*}
    using that
    \begin{equation*}
        \left(
            \frac{c_{0}^{2}}{n}
            +
            \frac{(c_{0}\overline{x}-c_{1})^{2}}{s_{x}^{2}}
        \right)^{-1/2}
        \left(
            \frac
            {
                c_{0} \beta_{0} + c_{1} \beta_{1} 
                - (c_{0} \hat{\beta}_{0}+ c_{1} \hbeta_{1})
            }{\sigma'}
        \right)
        \sim
        t(n-2)
    \end{equation*}
\end{frame}

\subsection{Prediction Intervals}

\begin{frame}
    \frametitle{The Setup}
    
    Let $\hat{Y} = \hbeta_{0} + \hbeta_{1} x$.

    Using the posterior distribution, we have
    \begin{equation*}
        U_{x}
        =
        \frac{Y-\hat{Y}}
        {
            \sigma' 
            \left(
                1+\frac{1}{n} + 
                \frac{(x- \overline{x})^2}{s_{2}^{2}}
            \right)
        }
        \sim
        t(n-2)
        .
    \end{equation*}
    So with probability $1-\alpha_{0}$
    \begin{equation*}
        \hat{Y}
        \pm
        \sigma'
        \left(
            \frac{1}{n}
            +
            \frac{(\overline{x}-x)^{2}}{s_{x}^{2}}
        \right)^{1/2}
        T_{n-2}^{-1}
        \left(
            1-\frac{\alpha_{0}}{2}
        \right)
    \end{equation*}

    \emoji{expressionless} The same as in Theorem 11.3.6.
\end{frame}

\begin{frame}
    \frametitle{Example 11.4.3}
    
    \think{} 
    Given the data in Example 11.4.2 \emoji{droplet}, 
    how to use
    \begin{equation*}
        \hat{Y}
        \pm
        \sigma'
        \left(
            \frac{1}{n}
            +
            \frac{(\overline{x}-x)^{2}}{s_{x}^{2}}
        \right)^{1/2}
        T_{n-2}^{-1}
        \left(
            1-\frac{\alpha_{0}}{2}
        \right)
    \end{equation*}
    when $x = 208$ and $\alpha_{0} = 0.1$?
\end{frame}

\subsection{Tests of Hypotheses}

\begin{frame}
    \frametitle{A Pointless Hypothesis}
    
    Consider
    \begin{equation*}
        \begin{aligned}
            H_{0} & : c_{0} \beta_{0} + c_{1} \beta_{1} = c_{\ast} \\
            H_{1} & : c_{0} \beta_{0} + c_{1} \beta_{1} \ne c_{\ast}
        \end{aligned}
    \end{equation*}

    \cake{} Why do we \emph{always} reject $H_{0}$ when we use the improper prior?
\end{frame}

\begin{frame}
    \frametitle{A Better Hypothesis}
    
    Thus it is better to consider
    \begin{equation*}
        \begin{aligned}
            H_{0} & : c_{0} \beta_{0} + c_{1} \beta_{1} \le c_{\ast} \\
            H_{1} & : c_{0} \beta_{0} + c_{1} \beta_{1} > c_{\ast}
        \end{aligned}
    \end{equation*}
    
    \think{} How to use
    \begin{equation*}
        \left(
            \frac{c_{0}^{2}}{n}
            +
            \frac{(c_{0}\overline{x}-c_{1})^{2}}{s_{x}^{2}}
        \right)^{-1/2}
        \left(
            \frac
            {
                c_{0} \beta_{0} + c_{1} \beta_{1} 
                - (c_{0} \hat{\beta}_{0}+ c_{1} \hbeta_{1})
            }{\sigma'}
        \right)
        \sim
        t(n-2)
    \end{equation*}
    to show that
    \begin{equation*}
        \p{H_{0}} = T_{n-2}(-U_{01})
    \end{equation*}

    \emoji{expressionless} The same as in Theorem 11.3.4.
\end{frame}

\begin{frame}
    \frametitle{Example 11.4.4}
    
    \think{} Given the data in Example 11.3.2 \emoji{car}, how to compute
    $\p{\abs{\beta_{1}} \le c}$?

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{../images/fig-11.14.png}
    \end{figure}
\end{frame}

\appendix


\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-26.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 11.4, Exercise 1, 2, 3, 4, 5, 6, 7.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
