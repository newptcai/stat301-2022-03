\input{../meta.tex}

\title{Lecture 21}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{10.1 Tests for Goodness-of-Fit}

\subsection{Description of Nonparametric Problems}

\begin{frame}[c]
    \frametitle{Example 10.2.1}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            In Example 10.1.6, we tested if the logarithms of ball bearing lifetimes
            are $\scN(\log(50), 0.25)$.

            \bigskip
            
            \think{} But how do we even know that a normal distribution is a good
            model? 

            \bigskip
            
            \bomb{} Not everything are \emph{normal}!
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{ball-bearing.png}
                \caption*{Image by deel\_de from \href{https://pixabay.com/photos/ball-bearing-roller-bearing-industry-1236203/}{Pixabay}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{A Bit Genetics \emoji{dna}}
    
    \alert{gene} --
    a segment of \emoji{dna} that is involved in producing a
    polypeptide chain; \ldots it is considered a unit of heredity.

    \bigskip

    \alert{allele} -- 
    either of a pair (or series) of alternative
    forms of a gene that can occupy the same locus on a
    particular chromosome and that control the same character.
\end{frame}

\begin{frame}[c]
    \frametitle{Example 10.2.2 --- Two Alleles}
    
    Consider a gene that has two types of alleles.

    A \emoji{scientist} assumes that 
    each parent has probability $\theta$ to pass the first allele.

    Thus, the probability of a child to get each of the three combinations are
    \begin{equation*}
        p_{1} = \theta^{2}
        ,
        \qquad
        p_{2} = 2 \theta (1-\theta)
        ,
        \qquad
        p_{3} = (1-\theta)^{2}
        .
    \end{equation*}

    \think{} How do we know if this is a good model?
\end{frame}

\begin{frame}[c]
    \frametitle{Example 10.2.2 --- Two Alleles}
    
    Consider a gene that has three types of alleles.

    A \emoji{scientist} assumes that 
    each parent has probability $\theta_{1}$ and $\theta_{2}$ 
    to pass the first allele and second allele respectively.

    Thus, the probability of a child to get each of the six combinations are
    \begin{equation*}
        \begin{aligned}
            &
            p_{1} = \theta_{1}^{2}
            ,
            \qquad
            p_{2} = \theta_{2}^{2}
            ,
            \qquad
            p_{3} = (1-\theta_{1} - \theta_{2})^{2}
            \\
            &
            p_{4} = 2 \theta_{1} \theta_{2}
            ,
            \qquad
            p_{5} = 2 \theta_{1} (1-\theta_{1} - \theta_{2})
            ,
            \qquad
            p_{6} = 2 \theta_{2} (1-\theta_{1} - \theta_{2})
            .
        \end{aligned}
    \end{equation*}

    \think{} How do we know if this is a good model?
\end{frame}

\begin{frame}
    \frametitle{The general problem}
    
    We will consider
    \begin{equation*}
        \begin{aligned}
        & H_{0}: \exists \theta \in \Omega, 
        \forall i \in \{1,2,\ldots, k\}, p_{i} = \pi_i(\theta).
        \\
        & H_{1}: \text{$H_{0}$ is not true.}
        \end{aligned}
    \end{equation*}

    We will assume that 
    \begin{itemize}
        \item $\sum_{i=1}^{k} \pi_{i}(\theta) = 1$;
        \item $\theta = (\theta_{1}, \theta_{2},\ldots, \theta_{s})$;
        \item $s < k-1$.
    \end{itemize}

    \hint{} Different from 10.1 --- We are not testing for a fixed $\theta$.
\end{frame}

\subsection{The \texorpdfstring{$\chi^2$}{Chi Square} Test for Composite Null Hypotheses}

\begin{frame}
    \frametitle{Theorem 10.2.1}
    
    Let $N_1,\ldots, N_k$ be the number of observations of each type in a sample.

    Let $\hat{\theta}$ be the \ac{mle} of $\theta$ \emph{given $N_1,\ldots, N_k$}.

    Then conditioning on $H_{0}$ is true
    \begin{equation*}
        Q
        =
        \sum_{i=1}^{k}
        \frac{(N_{i} - n \pi_{i}(\hat{\theta}))^{2}}{n \pi_{i}(\hat{\theta})}
        \inlaw
        \chi^{2}(\alert{k-1-s})
        ,
    \end{equation*}
    as $n \to \infty$, if certain conditions \dizzy{} are satisfied.
\end{frame}

\begin{frame}
    \frametitle{Example 10.2.3}
    
    Continuing the Example 10.2.2 \emoji{dna}.

    When there are two alleles, $k = 3$, $s = 1$, $Q \inlaw \chi^{2}(1)$.

    \cake{} What about when there are three alleles?
\end{frame}

\subsection{Determining the Maximum Likelihood Estimates}

\begin{frame}
    \frametitle{Example 10.2.4}
    
    Continuing the Example 10.2.2 \emoji{dna}.

    When there are two alleles, how to compute $\hat{\theta}$?
\end{frame}

\subsection{Testing Whether a Distribution Is Normal}

\begin{frame}
    \frametitle{The Test}
    
    Let $X_1,\ldots, X_n$ be a sample take from an unknown distribution.

    To test if they are $\scN(\mu, \sigma^{2})$, we divide the real line in to $k$
    disjoint intervals $(a_{1}, b_{1}), \ldots, (a_{k}, b_{k})$.

    Let $N_{i}$ be the number of observations in $(a_{i}, b_{i})$.

    When $H_{0}$ is true,
    \begin{equation*}
        \pi_{i}(\mu, \sigma^{2})
        =
        \Phi\left(\frac{b_{i}-\mu}{\sigma}\right)
        -
        \Phi\left(\frac{a_{i}-\mu}{\sigma}\right)
        .
    \end{equation*}

    Then we can compute $Q$ and carry out the $\chi^{2}$ test.
\end{frame}

\begin{frame}[c]
    \frametitle{How to find the \ac{mle}}
    
    To find the \ac{mle} of $(\mu, \sigma^2)$ \emph{given $N_1,\ldots, N_k$}, 
    we need to maximize
    \begin{equation*}
        (\pi_{i}(\mu, \sigma^{2}))^{N_{1}}
        \cdots
        (\pi_{i}(\mu, \sigma^{2}))^{N_{k}}
        .
    \end{equation*}

    Can we use the $\xmean{}$ and $S_{n}^{2}/n$,
    the \ac{mle} \emph{given $X_1,\ldots, X_n$},
    instead?
\end{frame}

\begin{frame}
    \frametitle{Theorem 10.2.2}
    
    Let $\hat{\theta}_{n}$ be the \ac{mle} of $\theta$ \emph{given $X_1,\ldots, X_n$}.

    Let $\pi_{i}(\theta) = \p{X_{1} \in (a_{i}, b_{i})|\theta}$.

    Let
    \begin{equation*}
        Q'
        =
        \sum_{i=1}^{k}
        \frac{(N_{i} - n \pi_{i}(\hat{\theta}_{n}))^{2}}{n \pi_{i}(\hat{\theta}_{n})}
        .
    \end{equation*}

    Then conditioning on $H_{0}$ is true,
    as $n \to \infty$, 
    the \ac{cdf} is bounded between the \acp{cdf} of
    $\chi^{2}(k-3)$
    and
    $\chi^{2}(k-1)$
    ,
    if certain conditions \dizzy{} are satisfied.
\end{frame}

\begin{frame}
    \frametitle{Example 10.2.5}
    
    \think{} How to use Theorem 10.2.2 and data from Example 10.1.6 to decide
    if the logarithms of ball bearing lifetimes have normal distribution?

    We use the same intervals as before and compute $Q'$.
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-21.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 10.1, Exercise 1, 3, 5, 7.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
