\input{../meta.tex}

\title{Lecture 17}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\begin{frame}[c]
    \frametitle{Quick Review of 9.3}
    Let
    \begin{equation*}
        H_{0}: \theta \le \theta_{0}
        ,
        \qquad
        H_{1}: \theta > \theta_{0}
        .
    \end{equation*}

    \pause{}

    The \ac{ump} test $\delta^{\ast}$ satisfies that
    $\alpha(\delta^{\ast}) \le \alpha_{0}$ and
    \begin{equation*}
        \beta(\delta^{\ast})
        \le
        \inf_{\delta: \alpha(\delta) \le \alpha_{0}}
        \beta(\delta)
        ,
        \qquad
        \text{for all }
        \theta > \theta_{0}
        .
    \end{equation*}

    \pause{}

    When the samples has a increasing \ac{mlr} in statistics $T$,
    the test $\delta$ which \reject{} when $T > c$ 
    and \accept{} when $T \le c$ where $c$ satisfies
    \begin{equation*}
        \p{T > c | \theta = \theta_{0}} = \alpha_{0},
    \end{equation*}
    is a \ac{ump} test.
\end{frame}

\section{9.4  Two-Sided Alternatives}

\subsection{General Form of the Procedure}

\begin{frame}[c]
    \frametitle{Example 9.4.1}
    \begin{columns}[c]
        \begin{column}{0.7\textwidth}

            Let $X_1,\ldots, X_{n}$ be breadth of human \emoji{skull} in ancient Egypt,
            which we assume to be \ac{iid} $\scN(\mu, 26)$.

            \think{} How to tell if $\mu = 140$, where $140$mm is the average of modern-day
            \emoji{skull}?

            Consider
            \begin{equation*}
                H_{0}: \theta \alert{=} 140
                ,
                \qquad
                H_{1}: \theta \alert{\ne} 140
                .
            \end{equation*}
        \end{column}
        \begin{column}{0.3\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{death.png}
            \end{figure} 
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{One-sided case}
    
    Let $X_1,\ldots, X_{n}$ be \ac{iid} $\scN(\mu, \sigma^{2})$ where
    $\sigma$ is known.

    Consider
    \begin{equation*}
        H_{0}': \mu \alert{\le} \mu_{0}
        ,
        \qquad
        H_{1}': \mu \alert{>} \mu_{0}
        .
    \end{equation*}

    The \ac{pdf} of $X_1,\ldots, X_n$ has a increasing \ac{mlr} in
    $\overline{X}_{n}$. 

    So a \ac{ump} test is $\delta_{1}$ which \reject{} if $\overline{X}_n \ge
    c_{2}$ such that
    \begin{equation*}
        \p{\overline{X_{n}} \ge c_{2}|\mu=\mu_{0}} = \alpha_{0}.
    \end{equation*}

    \cake{} 
    How to compute $c_{2}$? 

    \cake{} 
    What is the problem of using this test for $H_{0}: \mu = \mu_{0}$?
\end{frame}

\begin{frame}
    \frametitle{The other side}
    
    Let $X_1,\ldots, X_{n}$ be \ac{iid} $\scN(\mu, \sigma^{2})$ where
    $\sigma$ is known.

    Consider
    \begin{equation*}
        H_{0}': \mu \alert{\ge} \mu_{0}
        ,
        \qquad
        H_{1}': \mu \alert{<} \mu_{0}
        .
    \end{equation*}

    The \ac{pdf} of $X_1,\ldots, X_n$ has a increasing \ac{mlr} in
    $-\overline{X}_{n}$. 

    \cake{} 
    What condition should $c_{1}$ satisfy to make
    the test $\delta_{2}$ which \reject{} if $\overline{X}_n \le
    c_{1}$ a \ac{ump}?

    \cake{} 
    How to compute $c_{1}$?
\end{frame}

\begin{frame}
    \frametitle{Two-sided case}
    A natural test for
    \begin{equation*}
        H_{0}: \mu \alert{=} \mu_{0}
        ,
        \qquad
        H_{1}: \mu \alert{\ne} \mu_{0}
        ,
    \end{equation*}
    is to \reject{} if 
    $\overline{X}_{n} \le c_{1}$ or $\overline{X}_{n} \ge c_{2}$, such that
    \begin{equation*}
        \p{\overline{X}_n \le c_{1} | \mu = \mu_{0}}
        +
        \p{\overline{X}_n \ge c_{2} | \mu = \mu_{0}}
        =
        \alpha_{0}
        .
    \end{equation*}
    \pause{}
    In other words,
    \begin{equation*}
        \Phi\left(
        \frac{n^{1/2}(c_1-\mu_{0})}{\sigma}
        \right)
        +
        1
        -
        \Phi\left(
        \frac{n^{1/2}(c_2-\mu_{0})}{\sigma}
        \right)
        =
        \alpha_{1}
        +
        \alpha_{2}
        =
        \alpha_{0}
        .
    \end{equation*}
\end{frame}

\subsection{Selection of the Test Procedure}

\begin{frame}
    \frametitle{Select $\alpha_{1}$ and $\alpha_{2}$}
    Choosing different $\alpha_{1}, \alpha_{2}$ gives different test.
    
    What to choose depends on what kind of mistakes is more \emoji{fearful}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{9.8.png}
        \caption*{
            $\delta_{3}$ has $\alpha_{1}=\alpha_{2}=0.025$,
            and
            $\delta_{4}$ has $\alpha_{1}=0.01, \alpha_{2}=0.04$.
        }
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 9.4.2}

    Let $X_1,\ldots, X_{n}$ be breadth of human \emoji{skull} in ancient Egypt,
    which we assume to be \ac{iid} $\scN(\mu, 26)$.

    Consider
    \begin{equation*}
        H_{0}: \mu \alert{=} 140
        ,
        \qquad
        H_{1}: \mu \alert{\ne} 140
        .
    \end{equation*}

    \pause{}

    In this problem, there is no reason not to simply choose $\alpha_{1} =
    \alpha_{2}$.

    \bonus{} 
    Can you compute $c_{1}$ and $c_{2}$  when $n = 100$ and $\alpha_{0} = 0.1$?
\end{frame}

\subsection{Other distributions}

\begin{frame}
    \frametitle{Example 9.4.3}
    
    Let $X_1,\ldots, X_{n}$ be \ac{iid} $\Exp(\theta)$.

    To test
    \begin{equation*}
        H_{0}: \theta \alert{=} \frac{1}{2}
        ,
        \qquad
        H_{1}: \theta \alert{\ne} \frac{1}{2}
        ,
    \end{equation*}
    we combine the \ac{ump} tests for
    \begin{equation*}
        H_{0}': \theta \alert{\le} \frac{1}{2}
        ,
        \qquad
        H_{1}': \theta \alert{>} \frac{1}{2}
        ,
    \end{equation*}
    and
    \begin{equation*}
        H_{0}'': \theta \alert{\ge} \frac{1}{2}
        ,
        \qquad
        H_{1}'': \theta \alert{<} \frac{1}{2}
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.4.4 --- Alternative test}
    
    Alternatively, we can use the test which \reject{}
    if the likelihood ration $ \Lambda(\bfx) \le c$.

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{9.9.png}
    \end{figure}
\end{frame}

\subsection*{Composite Null Hypothesis}

\begin{frame}
    \frametitle{Example 9.4.5}
    
    Let $X_1,\ldots, X_{n}$ be \ac{iid} $\scN(\mu, 1)$.
    Consider
    \begin{equation*}
        H_{0}: 9.9 \le \mu \le 10.1
        ,
        \qquad
        H_{1}: \mu < 9.9 \text{ or } \mu > 10.1
        .
    \end{equation*}

    Let $\delta$ be the test \rejct{} if 
    $\overline{X}_n \le c_{1}$ or
    $\overline{X}_n \ge c_{2}$.

    \think{} How to find $c_{1}$ and $c_{2}$ such that
    \begin{equation*}
        \pi(9.9|\delta) = \pi(10.1|\delta) = 0.05
    \end{equation*}

    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{9.10}
    \end{figure}
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-17.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 9.4, 
                    Exercise 1, 3, 5, 7, 9, 11, 13.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
