\input{../meta.tex}

\title{Lecture 22}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{10.3 Contingency Tables}

\subsection{Independence in Contingency Tables}

\begin{frame}[c]
    \frametitle{Example 10.2.1}

    \think{} How can we tell if \emph{curriculum} 
    and \emph{candidate proffered} are independent?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{table-10.12.png}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Definition 10.3.1}
    
    A table in which each observation is classified in 
    two or more ways is called a \alert{contingency table}.

    Table~10.12 is called a \alert{two-way contingency table}.
    
    We will consider a two-way contingency table of $R$ rows and $C$ columns.
\end{frame}

\begin{frame}
    \frametitle{Some Notations}

    Let $p_{ij}$ be the probability that an individual (observation)
    belongs to row $i$ and column $j$.
    Let
    \begin{equation*}
        p_{i+}
        =
        \sum_{j=1}^{C}
        p_{ij}
        ,
        \qquad
        p_{+j}
        =
        \sum_{i=1}^{R}
        p_{ij}
        .
    \end{equation*}
    So
    \begin{equation*}
        \sum_{i=1}^{R}
        p_{i+}
        =
        \sum_{j=1}^{C}
        p_{j+}
        =
        1
        .
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{More Notations}
    Let $N_{ij}$ be the number of individuals (observations) in a sample
    belongs to row $i$ and column $j$.
    Let
    \begin{equation*}
        N_{i+}
        =
        \sum_{j=1}^{C}
        N_{ij}
        ,
        \qquad
        N_{+j}
        =
        \sum_{i=1}^{R}
        N_{ij}
        .
    \end{equation*}
    So
    \begin{equation*}
        \sum_{i=1}^{R}
        N_{i+}
        =
        \sum_{j=1}^{C}
        N_{j+}
        =
        n
        .
    \end{equation*}
\end{frame}

\begin{frame}[c]
    \frametitle{The Hypotheses}
    
    We want to test the hypotheses
    \begin{equation*}
        \begin{aligned}
            H_0 :~&p_{ij} = p_{i+} p_{+j}, \quad \text{for all } i \in \{1, \dots, R\}, j \in
            \{1, \dots, C\}, \\
            H_1 :~& H_0 \text{~is not true}.
        \end{aligned}
    \end{equation*}
\end{frame}

\subsection{The \texorpdfstring{$\chi^2$}{Chi-Square} Test of Independence}

\begin{frame}
    \frametitle{The Test}
    
    We can applied the $\chi^2$ test from 10.2 and compute
    \begin{equation*}
        Q
        =
        \sum_{i=1}^{R}
        \sum_{j=1}^{C}
        \frac{(N_{i} - \hat{E}_{ij})^{2}}{\hat{E}_{ij}}
        \inlaw
        \chi^{2}(\alert{k-1-s})
        ,
    \end{equation*}
    where $\hat{E}_{ij}$ is the \acs{mle} of $N_{ij}$ when $H_{0}$ is true.

    \cake{} What are $k$ and $s$ here?

    \pause{}
    
    When $H_{0}$ is true
    \begin{equation*}
        \hat{E}_{ij}
        =
        n
        \hat{p}_{ij}
        =
        n
        \hat{p}_{i+}
        \hat{p}_{j+}
        =
        n
        \frac{N_{i+}}{n}
        \frac{N_{+j}}{n}
        =
        \frac{N_{i+}N_{+j}}{n}
        .
    \end{equation*}
\end{frame}

\begin{frame}[c]
    \frametitle{Example 10.3.2}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{table-10.13.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 10.3.3}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{table-10.14.png}
        \includegraphics[width=0.6\textwidth]{table-10.15.png}
    \end{figure}
\end{frame}

\section{Story Time: Florence Nightingale}

\begin{frame}[c]
    \frametitle{Florence Nightingale}

    \begin{columns}[c]
        \begin{column}{0.6\textwidth}

            Nightingale was famous for being
            \begin{itemize}
                \item the founder of modern nursing,
                \item a social reformer,      
                \item the only non-royal woman on \emoji{pound} until 2002, etc.
            \end{itemize}

            But she was also a statistician and 
            the first female fellow of the \emph{Royal Statistical Society}.
            
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Florence_Nightingale-1.jpg}
                \caption*{Florence Nightingale (1820-1910)}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Nightingale As a Statistician}

    Nightingale pioneered using data visualization to improve public health.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{Florence_Nightingale.jpg}
    \end{figure}

    See more about Nightingale on \href{https://en.wikipedia.org/wiki/Florence\_Nightingale}{Wikipedia}
\end{frame}


\section{10.4 Tests of Homogeneity}

\subsection{Samples from Several Populations}

\begin{frame}
    \frametitle{Example 10.4.1}

    How can we tell if the distributions of 
    \emph{proffered candidates}
    in different populations
    (students taking different \emph{curriculum})
    is the same?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{table-10.12.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Some Notations}

    Let $p_{ij}$ be the probability that an individual (observation)
    from population $j$ has type $i$.
    So
    \begin{equation*}
        \sum_{j=1}^{C}
        p_{ij}
        =
        1
        \quad \text{for all } i \in \{1, \dots, R\}
        .
    \end{equation*}

    \pause{}
    
    We want to test the hypotheses
    \begin{equation*}
        \begin{aligned}
            H_0 :~& p_{1j} = p_{2j} = \dots = p_{Rj}, \quad \text{for all } j \in \{1, \dots, C\}, \\
            H_1 :~& H_0 \text{~is not true}.
        \end{aligned}
    \end{equation*}
\end{frame}

\subsection{The \texorpdfstring{$\chi^2$}{Chi-Square} Test of Homogeneity}

\begin{frame}
    \frametitle{The Test}
    
    If $H_{0}$ is true, then
    \begin{equation*}
        \sum_{j=1}^{C}
        \frac{(N_{i+} - N_{i+} p_{ij})^{2}}{N_{i+}p_{ij}}
        \inlaw
        \chi^{2}(C-1)
        ,
    \end{equation*}
    and
    \begin{equation*}
        \sum_{i=1}^{R}
        \sum_{j=1}^{C}
        \frac{(N_{i+} - N_{i+} p_{ij})^{2}}{N_{i+}p_{ij}}
        \inlaw
        \chi^{2}(R(C-1))
        .
    \end{equation*}
    \pause{}
    Therefore, we can compute
    \begin{equation*}
        Q
        =
        \sum_{i=1}^{R}
        \sum_{j=1}^{C}
        \frac{(N_{i} - \hat{E}_{ij})^{2}}{\hat{E}_{ij}}
        \inlaw
        \chi^{2}(R(C-1)-s)
        .
    \end{equation*}
    \cake{} What is $s$ here?
\end{frame}

\subsection{Comparing Two or More Proportions}

\begin{frame}[c]
    \frametitle{Example 10.4.2}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{table-10.21.png}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Example 10.4.3}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{table-2.1.png}
    \end{figure}

    \bonus{} What are $R$ and $C$ here?
\end{frame}

\subsection{Correlated \texorpdfstring{$2 \time 2$}{2 by 2} Tables}

\begin{frame}
    \frametitle{After a \emoji{fire}}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{table-10.22.png}
    \end{figure}
    \vspace{2em}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{table-10.23.png}
    \end{figure}
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-22.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 10.3, Exercise 1, 3, 5, 7, 9.
                \item[\emoji{pencil}] Section 10.4, Exercise 1, 3, 5, 7.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
