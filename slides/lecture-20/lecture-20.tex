\input{../meta.tex}

\title{Lecture 20}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{10.1 Tests for Goodness-of-Fit}

\subsection{Description of Nonparametric Problems}

\begin{frame}[c]
    \frametitle{Example 10.1.1}

    \begin{columns}[c]
        \begin{column}{0.6\textwidth}
            Let $X_1,\ldots, X_n$ be the logarithms of failure times
            of $n$ \href{https://en.wikipedia.org/wiki/Ball\_bearing}{ball bearings}.

            \think{} How can we know ---
            \begin{itemize}
                \item Is the normal distribution is a good model for $X_1,\ldots, X_n$?
                \item If not, how to estimate the mean and variance of?
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=\textwidth]{ball-bearing.png}
                \caption*{Image by deel\_de from \href{https://pixabay.com/photos/ball-bearing-roller-bearing-industry-1236203/}{Pixabay}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{Nonparametric Problems}
    
    We will \emph{not} assume $X_1,\ldots, X_n$ are from a
    particular distribution 
    and try to answer questions like
    \begin{itemize}
        \item Are $X_1,\ldots, X_n$ from a normal distribution?
        \item What is the median?
        \item Are two samples from the same distribution?
    \end{itemize}

    Such problems are called \alert{nonparametric problems} 
    and the methods we will use are called \alert{nonparametric methods}.
\end{frame}

\subsection{Categorical Data}

\begin{frame}
    \frametitle{Example 10.1.2 \emoji{drop-of-blood}}

    \think{} How can we know Table 10.2 is the correct distribution?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{table-10.1.png}
        \includegraphics[width=0.5\textwidth]{table-10.2.png}
    \end{figure}

    Data in which each observation belongs to one of finitely many categories
    is called \alert{categorical data}.
\end{frame}

\subsection{The \texorpdfstring{$\chi^2$}{Chi-Square} Test}

\begin{frame}
    \frametitle{The $\chi^2$ Test}
    
    Let $p_{i}$ be the probability that an observation is of type $i$ with
    $\sum_{i=1}^{k} p_{i} = 1$.

    We consider
    \begin{equation*}
        \begin{aligned}
            H_{0}: & p_{i} = p_{i}^{0} 
             \quad \text{for } i \in \{1, 2, \dots, k\} \\
            H_{1}: & p_{i} \ne p_{i}^{0} 
                \quad \text{for at least one } i \in \{1, 2, \dots, k\}.
        \end{aligned}
    \end{equation*}

    \pause{}
    
    Let $N_{i}$ be the number of observations of type $i$.
    Then $(N_{1},\ldots, N_{k})$ has a multinomial distribution with parameters
    $n$ and $\bfp = (p_{1},\ldots, p_{k})$.
\end{frame}

\begin{frame}
    \frametitle{Theorem 10.1.1}

    If $H_{0}$ is true, then as $n \to \infty$
    \begin{equation*}
        Q =
        \sum_{i=1}^{k}
        \frac{(N_{i} - n p_{i}^{0})^{2}}{n p_{i}^{0}}
        \inlaw
        \chi^{2}(k-1)
        .
    \end{equation*}

    \think{} 
    For a proof, see
    \href{https://ocw.mit.edu/courses/18-443-statistics-for-applications-fall-2003/resources/lec23/}{here}
    and also
    \href{https://math.stackexchange.com/a/2374894/1618}{here}.

    \pause{}

    \testtube{}
    This means when $n$ is large, we can 
    \begin{itemize}
        \item \reject{} $H_{0}$ when $Q \ge c$,
        \item \accept{} $H_{0}$ otherwise.
    \end{itemize}
    And the $c$ should be the $1-\alpha_{0}$ quantile of $\chi^{2}(k-1)$.

    This is the \alert{$\chi^{2}$ test of goodness-of-fit}.
\end{frame}

\begin{frame}
    \frametitle{Example 10.1.3 \emoji{drop-of-blood}}

    In the Example 10.1.2 \emoji{drop-of-blood}, we have $Q = 20.37$.

    \think{} What is the $p$-value for the $\chi^{2}$ test of goodness-of-fit?
\end{frame}

\begin{frame}
    \frametitle{Example 10.1.4}
    
    \think{} What is the $p$-value when we test the hypothesis that the three
    responses are equally likely?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\textwidth]{table-10.3.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 10.1.5}
    
    We bought $100$ \emoji{robot}, $16$ of them are broken.

    \think{} How can we test that
    \begin{equation*}
        \begin{aligned}
            H_{0}: & \, \text{A \emoji{robot} is broken with probability $0.1$}
            ,
            \\
            H_{1}: & \, \text{$H_{0}$ is not true}
            .
        \end{aligned}
    \end{equation*}
\end{frame}

\subsection{Testing Hypotheses about a Continuous Distribution}

\begin{frame}
    \frametitle{Is $X$ a uniform random?}
    
    Consider $X$ which takes its value on $[0,1]$.

    \think{} How can we test if $X \sim \Unif[0,1]$?
\end{frame}

\begin{frame}[c]
    \frametitle{The recipe \emoji{ramen}}
    
    To generalize the previous example to any continuous distribution,
    we can
    \begin{itemize}
        \item Partition the real line into $k$ intervals.
        \item Compute $p_{i}^{0}$ for each interval.
        \item Count $N_{i}$.
        \item Compute $Q$ and the $p$-value.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Example 10.1.6}
    
    \think{} Continuing Example 10.1.1, how to test if the logarithms of the failure
    times are \ac{iid} $\scN(\log(50), 0.25)$?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    \bonus{} Show that if $p_{i}^{0} = 1/k$ for all $i$, then
    \begin{equation*}
        Q 
        =
        \sum_{i=1}^{k}
        \frac{(N_{i} - n p_{i}^{0})^{2}}{n p_{i}^{0}}
        =
        \left(\frac{k}{n} \sum_{i=1}^{k} N_{i}^{2}\right)
        -
        n
        .
    \end{equation*}
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-20.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 10.1, Exercise 1, 3, 5, 7, 9.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
