\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 01}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    
    \tableofcontents{}
\end{frame}

\section{7.1 Statistical Inference}

\subsection{Probability and Statistical Models}

\begin{frame}[c]
    \frametitle{What is statistical inference}
    
    Statistical inference tries to answer questions like

    \begin{itemize}[<+->]
        \item What would we say is the probability that a future patient will respond
            successfully to \emoji{pill} after we observe the results from a collection of other patients?
        \item What can we say about a student's grade in this course after we
            observing some his/her grades?
        \item What can we say about whether there was discrimination after observing how different 
             \emoji{girl} \emoji{girl-light-skin-tone} \emoji{girl-medium-light-skin-tone} \emoji{girl-medium-skin-tone} \emoji{girl-medium-dark-skin-tone} \emoji{girl-dark-skin-tone}
            were treated?
        \item Can we claim the \emoji{crab} in Yangcheng Lake is larger those in Tai Lake
            after observing crabs from both lakes?
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Review --- Probability Distributions}
    
    The \ac{pdf} of $X \sim \Exp(\theta)$ is
    \begin{equation*}
        f(x) = 
        \begin{cases}
            \theta  e^{- \theta  x} & x > 0, \\
            0 & \text{otherwise}
        \end{cases}
    \end{equation*}

    \pause{}

    The \ac{pdf} of $X \sim \Gam(\alpha, \beta)$ is
    \begin{equation*}
        f(x) = 
        \begin{cases}
            \displaystyle \frac{\beta ^{\alpha } x^{\alpha -1} e^{- \beta x}}{\Gamma (\alpha )} & x > 0, \\
            0 & \text{otherwise}
        \end{cases}
    \end{equation*}

    \cake{} What is the \ac{pdf} of $X \sim \Unif[0, 1]$?
\end{frame}

\begin{frame}[c]
    \frametitle{Example 7.1.1}
    A mobile phone company assumes the lifetimes of their \emoji{iphone} are \acf{iid}
    $\Exp(\theta)$.

    \pause{}

    Let $X_{1}, X_{2}, \dots, X_{m}$ be the lifetimes of $m$ \emoji{iphone}.
    Then as $m \to \infty$
    \begin{equation*}
        \frac{\sum_{i=1}^{m} X_{i}}{m}
        \inprob
        \text{\question}
    \end{equation*}
    These \acp{rv} are \alert{observable}.

    \pause{}

    If we assume that $\theta \sim \Gam(1,2)$, then it is an \alert{hypothetically
    observable} \ac{rv}.

    \cake{} What does it mean to say $Y_{n} \inprob Y$?
\end{frame}

\begin{frame}[c]
    \frametitle{Statistical model}

    A statistical model consists of

    \begin{itemize}
        \item[\emoji{hearts}] \acp{rv} of interest;
        \item[\think{}] the family of possible the distributions of these
            \acp{rv};
        \item[\question{}] unknown parameters;
        \item[\dizzy{}] the distributions of these parameters.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Statistical inference}
    
    A \alert{statistical inference} is a procedure that produces a probabilistic statement about a model.

    \begin{exampleblock}{In Example 7.1.1}
        \begin{itemize}
            \item Produce a \ac{rv} $Y$  such that $\p{Y \ge \theta \cond \theta } = 0.9$.
            \item Produce a \ac{rv} $Y \approx \theta$.
            \item What is
                \begin{equation*}
                    \p{\frac{1}{10} \sum_{i=m+1}^{m+10} X_{i} \ge 2}
                \end{equation*}
            \item How \emph{confident} we are that $\theta \ge 0.4$?
        \end{itemize}
    \end{exampleblock}
\end{frame}

\begin{frame}[c]
    \frametitle{Parameters}
    
    Characteristics that determine the joint distribution for the \acp{rv} of
    interest is called a \alert{parameter} of the distribution. 

    The set $\Omega$ of all possible values of $\theta$ or $(\theta_1, . . . , \theta_k )$ is
    called the \alert{parameter space}.

    \begin{example}
        $\Bin(n, p)$, $\Gaussian(\mu, \sigma^{2})$, $\Exp(\theta)$, etc.
    \end{example}

    \cake{} What is $\Omega$ for $\Exp(\theta)$ in Example 7.1.1?
\end{frame}

\subsection{Examples of Statistical Inference}

\begin{frame}
    \frametitle{Example 7.1.3 --- A Clinical Trial \emoji{syringe}}

    \begin{exampleblock}{The model}
        Assume that $X_{1}, X_{2}, \dots, X_{40}$ be \ac{iid} $\Ber(P)$ 
        and $P \sim \Unif[0,1]$.    
    \end{exampleblock}

    \pause{}

    \begin{exampleblock}{The statistical inference}
        Given that $X = \sum_{i=1}^{40} X_{i} = x$, 
        the predict $p$ of $P$ which minimizes the \acf{mse}
        \begin{equation*}
            \E{(P-p)^{2}}
            =
            \frac{301}{75,852}
        \end{equation*}
        is
        \begin{equation*}
            p 
            =
            \E{P \cond X = x}
            = 
            \frac{x+1}{42}
            .
        \end{equation*}
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Example 7.1.4 -- Radioactive Particles \emoji{radioactive}}
    
    \begin{exampleblock}{The model}
        Assume that $X = \Poi(\beta t)$ be the number of radioactive particles which
        arrive in $t$ seconds and that $\beta \sim \Gam(\alpha, \gamma)$.    
    \end{exampleblock}

    \pause{}

    \begin{exampleblock}{The statistical inference}
        Conditioning on that $X = x$, we have
        \begin{equation*}
            \beta \sim \Gam(x+\alpha, t+\gamma).
        \end{equation*}
    \end{exampleblock}
\end{frame}

\begin{frame}[c]
    \frametitle{General Classes of Inference Problems}
    
    \begin{itemize}
        \item[\emoji{crystal-ball}] Predication/Estimation --- How many patients will recover?

        \item[\question] Statistical Decision Problems/Hypothesis testing --- Is the probability of recover $P > 0.5$?

        \item[\emoji{test-tube}] Experimental Design --- How many \emoji{pill} each
            patient should receive in a clinical trial?

        \item[\emoji{confetti-ball}] Other --- Many many more.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Statistics}
    
    Suppose that the observable \acp{rv} are $X_1, \dots, X_m$.

    Let $r: \dsR^{m} \mapsto \dsR$ be an arbitrary function. 
    Then the random variable $T = r(X_{1}, \dots, X_{m})$ is called a \alert{statistic}.

    \pause{}

    \begin{example}
        The mean, 
        \begin{equation*}
            \overline{X}_m = \frac{1}{m} \sum_{i=1}^{m} X_{i},
        \end{equation*}
        the maximum
        \begin{equation*}
            Y_m = \max \{X_{1}, \dots, X_{m}\},
        \end{equation*}
        and the constant function
        \begin{equation*}
            r(\{X_{1}, \dots, X_{m}\}) = 3.
        \end{equation*}
    \end{example}
\end{frame}

\begin{frame}
    \frametitle{Example 7.1.6}
    
    Suppose that the heights of men has distribution $\Gaussian(μ, 9)$. 

    Suppose for $n = 36$ let $\overline{X}_n$ stand for the average of $n$ men's heights. 
    Then 
    \begin{equation*}
        \p{ \mu \in [\overline{X}_n - 0.98, \overline{X}_n + 0.98]} > 0.95 .
    \end{equation*}

    \cake{} 
    What are the \acp{rv}, distributions of \acp{rv}, and parameters?
    What statistics do we compute?
    What is the inference?
\end{frame}

\subsection{Parameters as Random Variables}

\begin{frame}[c]
    \frametitle{Are parameters \acp{rv}?}
    
    In the clinical trial example, we can consider $P$ as an \acp{rv} and $X_{1},
    \ldots, X_{n}$ be \ac{iid} $\Ber(P)$.

    \alert{Bayesians} believe this is always right.

    \pause{}

    Or we can we can consider $P$ be an \acp{rv} and $X_{1},
    \ldots, X_{n}$ be \ac{iid} $\Ber(p)$ for some unknown $p$.

    \alert{Frequentist} believe sometimes we should do this.

    \pause{}

    This is an ongoing debate --
    \begin{itemize}
        \item \href{https://www.nytimes.com/2014/09/30/science/the-odds-continually-updated.html}{The Odds, Continually Updated - The New York Times}
        \item \href{https://80000hours.org/podcast/episodes/spencer-greenberg-bayesian-updating/}{How much should you change your beliefs based on new evidence?}
    \end{itemize}
\end{frame}

\section{7.2 Prior and Posterior Distributions}

\subsection{The Prior Distribution}

\begin{frame}[c]
    \frametitle{Prior distributions}

    Suppose that a statistical model has a \ac{rv} parameter $\theta$.

    The distribution that one assigns to $\theta$ before observing the other random
    variables is called its \alert{prior distribution}. 

    We denote its \ac{pdf} by $\xi(\theta)$.

    The prior comes from past experience and knowledge.
\end{frame}

\begin{frame}
    \frametitle{Example 7.2.1}
    Let $X_{1}, \dots X_{m}$ be \ac{iid} $\Exp(\theta)$ where $\theta \sim \Gam(1, 2)$.

    Then $\Gam(1,2)$ is the \emph{prior distribution} of $\theta$ and
    \begin{equation*}
        \xi(\theta) = 
        \left\{ 
            \begin{array}{cc}
                2 e^{-2 x} & x>0, \\
                0 & x\le 0 . \\
            \end{array}
        \right.
    \end{equation*}

    \pause{}
    
    The \ac{pdf} of $(X_{1}, \dots X_{m})$ conditioned on $\theta$ is
    \begin{equation*}
        f_{m}(\bfx | \theta)
        =
        \begin{cases}
            \theta^{m} \exp(-\theta y) 
            & \text{for all } x_{1}, \ldots x_{m} > 0, \\
            0 & \text{otherwise},
        \end{cases}
        ,
    \end{equation*}
    where $\bfx = x_{1}, \ldots, x_{m}$ and $y = \sum_{i=1}^{m} x_{i}$.
\end{frame}

\begin{frame}[c]
    \frametitle{Sensitivity analysis}
    
    If it often useful to compare posterior distributions from different prior.
    This is called \emph{sensitivity analysis}.

    It is very often the case that different prior distributions do not make much
    different if there is \emph{a lot of data} \emoji{expressionless}.
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-01.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignment problems will \emph{not} be graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 7.1, Exercise 1, 3, 5, 7.
                \item[\emoji{pencil}] Section 7.2, Exercise 1, 3, 5, 6, 7.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
