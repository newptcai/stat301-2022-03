\input{../meta.tex}

\title{Lecture 18}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\begin{frame}[c]
    \frametitle{Quick Review of 9.4}
    Let $X_1,\ldots, X_n$ be \ac{iid} $\scN(\mu, \sigma^{2})$, where $\sigma$ is
    \emph{known}.

    A natural test for
    \begin{equation*}
        H_{0}: \mu \alert{=} \mu_{0}
        ,
        \qquad
        H_{1}: \mu \alert{\ne} \mu_{0}
        ,
    \end{equation*}
    is to \reject{} if and only if
    $\overline{X}_{n} \le c_{1}$ or $\overline{X}_{n} \ge c_{2}$, such that
    \begin{equation*}
        \p{\overline{X}_n \le c_{1} | \mu = \mu_{0}}
        +
        \p{\overline{X}_n \ge c_{2} | \mu = \mu_{0}}
        =
        \alpha_{0}
        .
    \end{equation*}
    In other words,
    \begin{equation*}
        \Phi\left(
        \frac{n^{1/2}(c_1-\mu_{0})}{\sigma}
        \right)
        +
        1
        -
        \Phi\left(
        \frac{n^{1/2}(c_2-\mu_{0})}{\sigma}
        \right)
        =
        \alpha_{1}
        +
        \alpha_{2}
        =
        \alpha_{0}
        .
    \end{equation*}

    \emoji{confused} How is possible that we know what is $\sigma$?
\end{frame}

\section{9.5  The $t$ Test}

\subsection{Testing Hypotheses about the Mean of a Normal Distribution When the Variance Is Unknown}

\begin{frame}[c]
    \frametitle{Example 9.5.1}

    \begin{columns}[c]
        \begin{column}{0.7\textwidth}

            Let $X_1,\ldots, X_n$ be the number of days each $n$ \emoji{older-woman} stayed
            in a \emoji{hospital} in the past year.

            We assume that $X_1,\ldots, X_n$ are \ac{iid} $\scN(\mu, \sigma)$ where both
            $\mu$ and $\sigma$ are unknown.

            \think{} How should we test
            \begin{equation*}
                H_{0}: \mu \le 200
                ,
                \qquad
                H_{1}: \mu > 200
                ,
            \end{equation*}
        \end{column}
        \begin{column}{0.3\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{old-woman.png}
            \end{figure}
        \end{column}
    \end{columns}

    \pause{}

    \begin{block}{The \emoji{dart} of this section}
    We will consider how to test
    \begin{equation*}
        H_{0}: \mu \le \mu_{0}
        ,
        \qquad
        H_{1}: \mu > \mu_{0}
        .
        \tag*{(9.5.1)}
    \end{equation*}
    when $\mu$ and $\sigma$ are unknown
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{The $t$ test}
    
    Let
    \begin{equation*}
        \overline{X}_n
        =
        \frac{\sum_{i=1}^n X_{i}}{n}
        ,
        \qquad
        \sigma'
        =
        \left(
        \frac{\sum_{i=1}^n (X_{i}-\overline{X}_n)^{2}}{n}
        \right)
        .
    \end{equation*}
    Let
    \begin{equation*}
        U = \sqrt{n} \frac{\xmean{} - \mu_{0}}{\sigma'}.
    \end{equation*}
    Then $U \sim t(n-1)$.

    \pause{}

    The $t$ test \reject{} if $U \ge c$ and \accept{} if $U < c$, 
    where $c$ satisfies
    \begin{equation*}
        \p{U \ge c| \mu = \mu_{0}} = \alpha_{0},
    \end{equation*}
    i.e., $c = T_{n-1}^{-1}(1-\alpha_{0})$.
\end{frame}

\subsection{Properties of the $t$ Tests}

\begin{frame}
    \frametitle{Theorem 9.5.1}

    Let $\delta$ be the $t$ test. 
    Then the following properties holds ---
    \begin{enumerate}
        \item $π(μ, σ^2|δ) = α_0$ when $μ = μ_0$,
        \item $π(μ, σ^2|δ) < α_0$ when $μ < μ_0$,
        \item $π(μ, σ^2|δ) > α_0$ when $μ > μ_0$,
        \item $π(μ, σ^2|δ) \to 0$ as $μ \to −∞$,
        \item $π(μ, σ^2|δ) \to 1$ as $μ \to ∞$.
    \end{enumerate}
    Moreover $\alpha(\delta) \le \alpha_{0}$.
\end{frame}

\begin{frame}
    \frametitle{Theorem 9.5.2}

    If we observe that $U = u$, then the $p$-values of $t$ tests
    is $1 - T_{n-1}(u)$.

    \hint{} Low $p$-value means we are less likely to be wrong to \reject{}.
\end{frame}

\begin{frame}
    \frametitle{Example 9.5.4}

    Let $X_1,\ldots, X_n$ be \ac{iid} $\scN(\mu, \sigma^{2})$.
    \begin{equation*}
        H_{0}: \mu \le 5.2
        ,
        \qquad
        H_{1}: \mu > 5.2
        .
        \tag*{(9.5.1)}
    \end{equation*}
    
    Assume that for $n = 15$ we observe $\xmean{} = 5.4$ and $\sigma'=0.4226$.

    \think{} What is the $t$ test for $\alpha_{0} = 0.05$?

    \think{} What is the $p$-value?
\end{frame}

\begin{frame}
    \frametitle{Another way to write $U$}

    Note that
    \begin{equation*}
        U 
        = 
        \frac{\sqrt{n}(\xmean{} - \mu_{0})}{\sigma'}
        = 
        \frac{\sqrt{n}(\xmean{} - \mu_{0})/\sigma}{\sigma'/\sigma}.
    \end{equation*}
    \think{} What is the distributions of the numerator and denominator?
\end{frame}

\begin{frame}
    \frametitle{Definition 9.5.1}
    
    Let $Y$ and $W$ be independent \acp{rv} with 
    $W \sim \scN(ψ, 1)$ 
    and $Y \sim \chi^{2}(m)$. 
    Then the distribution of
    \begin{equation*}
        X
        =
        \frac{W}{\left(\frac{Y}{m}\right)^{1/2}}
    \end{equation*}
    is called \alert{the noncentral $t$ distribution} 
    with \alert{$m$ degrees of freedom} 
    and \alert{noncentrality parameter $ψ$}. 

    We shall let $T_m(t|ψ)$ denote the \ac{cdf} of this distribution.

    \cake{} What is $X$ when $\psi = 0$?
\end{frame}

\begin{frame}
    \frametitle{Theorem 9.5.3}
    
    The statistic $U$ has the noncentral $t$ distribution with $n-1$ degrees of
    freedom and noncentral parameter 
    \begin{equation*}
        \psi = \frac{n^{1/2}(\mu - \mu_{0})}{\sigma}.
    \end{equation*}

    The power function of the $t$ test is
    \begin{equation*}
        \pi(\mu, \sigma^{2} | \delta) = 1- T_{n-1}(c|\psi).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 9.5.5, 9.5.6}
    
    Let $X_1,\ldots, X_n$ be \ac{iid} $\scN(\mu, \sigma^{2})$.
    \begin{equation*}
        H_{0}: \mu \le 5.2
        ,
        \qquad
        H_{1}: \mu > 5.2
        .
        \tag*{(9.5.1)}
    \end{equation*}
    
    Assume that for $n = 15$ we observe $\xmean{} = 5.4$ and $\sigma'=0.4226$.
    Let $\delta$ be the $t$ test with $\alpha_{0} = 0.05$.

    \think{} 
    What it the probability of \reject{} when $\mu = 5.2 + \sigma/2$?

    \think{} 
    How large $n$ needs to be so this is be at least $0.8$?
    
    \pause{}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{9.2.png}
    \end{figure}
\end{frame}

\subsection{The Paired $t$ Test}

\begin{frame}
    \frametitle{Example 9.5.7}
    
    \think{} How can we tell if it is safer to sit on the driver's seat or the
    passenger's seat in a \emoji{car} crash?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\textwidth]{9.13.png}
    \end{figure}
\end{frame}

\subsection{Testing with a Two-Sided Alternative}

\begin{frame}
    \frametitle{Example 9.5.8}
    
    Let $X_1,\ldots, X_{n}$ be breadth of human \emoji{skull} in an ancient country,
    which we assume to be \ac{iid} $\scN(\mu, \sigma^{2})$.

    For the hypotheses
    \begin{equation*}
        H_{0}: \theta \alert{=} 140
        ,
        \qquad
        H_{1}: \theta \alert{\ne} 140
        ,
    \end{equation*}
    we can use the test which
    \begin{itemize}
        \item \reject{} if $\abs{U} \ge T^{-1}_{n-1}(1-\alpha_{0}/2)$
        \item \accept{} otherwise.
    \end{itemize}
\end{frame}

\appendix

\begin{frame}[c]
    \frametitle{Assignments}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-18.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignments are not graded but they will appear in quizzes!

            \begin{itemize}
                \item[\emoji{pencil}] Section 9.5, 
                    Exercise 1, 3, 5, 7, 9, 11, 13, 15.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
