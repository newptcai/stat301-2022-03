# STATS301 -- Statistics

This repository contains materials for the course STATS301 (Statistics)
taught at [Duke Kunshan University](https://dukekunshan.edu.cn/) 
by [Xing Shi Cai](https://newptcai.gitlab.io)
starting from March 2022.

You are free to use any material here. But this is *not* the official course website.
