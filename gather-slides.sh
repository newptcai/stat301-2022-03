#!/bin/bash

# The slides
find \
    slides \
    -type f \
    \( -name "*.pdf" ! -path "*/images/*" ! -name "*svg*" \) \
    -printf '%p\n' \
    -exec cp {} pdf \;
